<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 1:21 AM
 */

namespace App\Decorators\DirectorDecorator;


use App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie\GetDirectorPaginatedMovieHandler;
use Illuminate\Database\Eloquent\Model;

class GetDirectorMovieDecorator extends EloquentDirectorDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $director= parent::getModel($attributes, $id);
        $movieHandler = new GetDirectorPaginatedMovieHandler();
        $attributes['relatedId'] = $director['id'];
        $movieHandler->handle($attributes);

        if (isset($director['movies'])) {
            unset($director['movies']);
        }
        $director['movies'] = $attributes['paginatedMovies'];

        return $director;
    }
}