<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 1:20 AM
 */

namespace App\Decorators\DirectorDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\DirectorService;

class EloquentDirectorDecorator extends EloquentDecorator
{
    public function __construct(DirectorService $service)
    {
        parent::__construct($service);
    }
}