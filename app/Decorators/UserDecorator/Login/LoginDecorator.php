<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 1:33 AM
 */

namespace App\Decorators\UserDecorator\Login;


use App\Decorators\UserDecorator\EloquentUserDecorator;
use App\Handlers\UserHandlers\HashPasswordHandler;
use Illuminate\Database\Eloquent\Model;

class LoginDecorator extends EloquentUserDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $passwordHandler = new HashPasswordHandler();

        $passwordHandler->handle($attributes);

        $pairs = [
            [
                'needle' => 'username',
                'value'=> $attributes['username']
            ],
            [
                'needle' => 'password',
                'value' => $attributes['password']
            ]
        ];

        $user = $this->getBy($pairs);

        if ($user == null)
        {
            return null;
        }

        $id = $user['id'];
        return parent::getModel(['role'], $id);
    }
}