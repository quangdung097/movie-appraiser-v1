<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:21 PM
 */

namespace App\Decorators\UserDecorator\Login;


use App\Decorators\EloquentDecorator;
use App\Handlers\PostHandler\GetLastPostHandler;
use App\Handlers\PostHandler\GetNewPostHandler;
use App\Handlers\UserHandlers\GetNewUserHandler;
use App\Services\UserService;
use Illuminate\Database\Eloquent\Model;

class AdminDecorator extends EloquentDecorator
{
    public function __construct(UserService $service)
    {
        parent::__construct($service);
    }

    public function getModel(array $attributes, $id): ?Model
    {
        $loginUser = parent::getModel($attributes, $id);

        $lastPostHandler = new GetLastPostHandler();
        $countNewPostHandler = new GetNewPostHandler();
        $countNewUserHandler = new GetNewUserHandler();

        $lastPostHandler->setNextHandler($countNewPostHandler);
        $countNewPostHandler->setNextHandler($countNewUserHandler);

        $extendsAttributes = [];
        $lastPostHandler->handle($extendsAttributes);

        $loginUser['extends'] = $extendsAttributes;

        return $loginUser;
    }
}