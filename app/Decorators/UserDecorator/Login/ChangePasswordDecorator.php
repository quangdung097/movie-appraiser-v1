<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 1:33 AM
 */

namespace App\Decorators\UserDecorator\Login;


use App\Decorators\UserDecorator\EloquentUserDecorator;
use App\Handlers\UserHandlers\HashPasswordHandler;
use Illuminate\Database\Eloquent\Model;

class ChangePasswordDecorator extends EloquentUserDecorator
{
    private static $INVALID_OLD_PASSWORD_ERROR = 'Sai mật khẩu';
    private static $INVALID_USER_ERROR = 'Bạn chưa đăng nhập';
    public function updateModel(array $attributes, $id): bool
    {
        $passwordHandler = new HashPasswordHandler();

        $attributes['password'] = $attributes['oldPassword'];
        $passwordHandler->handle($attributes);

        $id = session('user_id');

        if ($id == null) {
            $this->setMessage(self::$INVALID_USER_ERROR);
            return false;
        }

        $pairs = [
            [
                'needle' => 'id',
                'value'=> $id
            ],
            [
                'needle' => 'password',
                'value' => $attributes['password']
            ]
        ];

        $user = $this->getBy($pairs);

        if ($user == null){
            $this->setMessage(self::$INVALID_OLD_PASSWORD_ERROR);
            return false;
        } else {
            $attributes['password'] = $attributes['newPassword'];
            $passwordHandler->handle($attributes);
            return parent::updateModel($attributes, $user['id']);
        }
    }
}