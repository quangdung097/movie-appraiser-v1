<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 5:21 PM
 */

namespace App\Decorators\UserDecorator;


use App\Handlers\ImageHandler\Eloquent\CreateImage\CreateUserImageHandler;
use App\Handlers\ImageHandler\File\UploadImage\UploadUserImageHandler;
use App\Handlers\UserHandlers\CheckUserRoleHandler;

class UpdateUserDecorator extends EloquentUserDecorator
{
    public function updateModel(array $attributes, $id): bool
    {
        $authorizedHandler = new CheckUserRoleHandler();

        if (array_key_exists('avatar', $attributes)) {
            $uploadHandler = new UploadUserImageHandler();
            $createHandler = new CreateUserImageHandler();
            $authorizedHandler->setNextHandler($uploadHandler);
            $uploadHandler->setNextHandler($createHandler);
        }
        $attributes['updated_user_id'] = $id;
        $response = $authorizedHandler->handle($attributes);

        if ($response->getStatus() == false) {
            $this->setMessage($response->getMessage());
            return false;
        }

        return parent::updateModel($attributes, $id);
    }
}