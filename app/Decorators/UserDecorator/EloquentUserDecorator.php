<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 10:40 PM
 */

namespace App\Decorators\UserDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\UserService;

class EloquentUserDecorator extends EloquentDecorator
{
    public function __construct(UserService $service)
    {
        parent::__construct($service);
    }
}