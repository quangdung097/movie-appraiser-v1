<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 10:43 PM
 */

namespace App\Decorators\UserDecorator\Register;


use App\Decorators\UserDecorator\EloquentUserDecorator;
use App\Handlers\ImageHandler\Eloquent\DefaultImageHandlers\SignInDefaultImageHandler;
use App\Handlers\UserHandlers\AddRoleHandler;
use App\Handlers\UserHandlers\HashPasswordHandler;
use Illuminate\Database\Eloquent\Model;

class RegisterDecorator extends EloquentUserDecorator
{
    public function createNewModel(array $attributes): ?Model
    {
        $passwordHandler = new HashPasswordHandler();
        $roleHandler = new AddRoleHandler();

        if (array_key_exists('avatar', $attributes)) {
            $avatarHandler = new SignInDefaultImageHandler();
        } else {
            $avatarHandler = new SignInDefaultImageHandler();
        }

        $passwordHandler->setNextHandler($roleHandler);
        $roleHandler->setNextHandler($avatarHandler);
        $passwordHandler->handle($attributes);

        return parent::createNewModel($attributes);
    }
}