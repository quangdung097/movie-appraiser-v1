<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 9:51 PM
 */

namespace App\Decorators;


use App\Services\Message;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class EloquentDecorator implements Decorator, Message
{
    private $service;
    private $message;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function createNewModel(array $attributes): ?Model
    {
        return $this->service->createNewModel($attributes);
    }

    public function updateModel(array $attributes, $id): bool
    {
        return $this->service->updateModel($attributes, $id);
    }

    public function deleteModel(array $attributes, $id): int
    {
        return $this->service->deleteModel($attributes, $id);
    }

    public function getModel(array $attributes, $id): ?Model
    {
        return $this->service->getModel($attributes, $id);
    }

    public function count(array $pairs = []): int
    {
        return $this->service->count($pairs);
    }

    public function getAll(array $attributes = [])
    {
        return $this->service->getAll($attributes);
    }

    public function getBetween($needle, $from, $to, array $relations = [])
    {
        return $this->service->getBetween($needle, $from, $to, $relations);
    }

    public function getBy(array $pairs, array $relations = [])
    {
        return $this->service->getBy($pairs, $relations);
    }

    public function getService(): Service
    {
        return $this->service;
    }

    public function paginate(array $attributes)
    {
        return $this->service->paginate($attributes);
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}