<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 1:11 AM
 */

namespace App\Decorators\ActorDecorator;


use App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie\GetActorPaginatedMovieHandler;
use Illuminate\Database\Eloquent\Model;

class GetActorMovieDecorator extends EloquentActorDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $actor = parent::getModel($attributes, $id);
        $movieHandler = new GetActorPaginatedMovieHandler();
        $attributes['relatedId'] = $actor['id'];
        $movieHandler->handle($attributes);

        if (isset($actor['movies'])) {
            unset($actor['movies']);
        }
        $actor['movies'] = $attributes['paginatedMovies'];

        return $actor;
    }
}