<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 1:10 AM
 */

namespace App\Decorators\ActorDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\ActorService;

class EloquentActorDecorator extends EloquentDecorator
{
    public function __construct(ActorService $service)
    {
        parent::__construct($service);
    }
}