<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/22/2019
 * Time: 12:16 AM
 */

namespace App\Decorators\NewsDecorator;


use App\Services\News\NewsService;

abstract class BaseNewsDecorator implements NewsService
{
    private $newService;

    public function __construct()
    {
        $this->newService = $this->createNewsService();
    }

    public function getNews(array $attributes): array
    {
        return $this->newService->getNews($attributes);
    }

    abstract public function createNewsService(): NewsService;
}