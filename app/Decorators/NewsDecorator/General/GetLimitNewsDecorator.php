<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/22/2019
 * Time: 12:18 AM
 */

namespace App\Decorators\NewsDecorator\General;


class GetLimitNewsDecorator extends GeneralNewsDecorator
{
    public function getNews(array $attributes): array
    {
        $articles = parent::getNews($attributes);

        if (count($articles) > 3) {
            $randomKeys = array_rand($articles, 3);
            $limitedArticles = [];
            foreach ($randomKeys as $key) {
                array_push($limitedArticles, $articles[$key]);
            }
            return $limitedArticles;
        }

        return $articles;
    }
}