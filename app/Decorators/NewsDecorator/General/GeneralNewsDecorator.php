<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/22/2019
 * Time: 12:18 AM
 */

namespace App\Decorators\NewsDecorator\General;


use App\Decorators\NewsDecorator\BaseNewsDecorator;
use App\Services\News\Implementations\GeneralNewsService;
use App\Services\News\NewsService;

class GeneralNewsDecorator extends BaseNewsDecorator
{

    public function createNewsService(): NewsService
    {
        return new GeneralNewsService();
    }
}