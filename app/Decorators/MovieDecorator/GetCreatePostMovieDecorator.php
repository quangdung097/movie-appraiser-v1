<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:03 AM
 */

namespace App\Decorators\MovieDecorator;


class GetCreatePostMovieDecorator extends EloquentMovieDecorator
{
    public function getAll(array $attributes = [])
    {
        $attributes['sort'] = 'title';
        $attributes['order'] = 'asc';
        return parent::getAll($attributes);
    }
}