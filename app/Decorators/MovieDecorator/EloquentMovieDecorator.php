<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 9:22 PM
 */

namespace App\Decorators\MovieDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\MovieService;

class EloquentMovieDecorator extends EloquentDecorator
{
    public function __construct(MovieService $service)
    {
        parent::__construct($service);
    }
}