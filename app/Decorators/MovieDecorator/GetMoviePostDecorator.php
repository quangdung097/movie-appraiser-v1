<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 9:31 PM
 */

namespace App\Decorators\MovieDecorator;


use App\Handlers\MovieHandler\GetRelatedMovieHandler;
use App\Handlers\PostHandler\GetPost\GetPaginatedPostsHandler;
use App\Handlers\UserRateHandler\CountAverageRateHandler;
use App\Handlers\UserRateHandler\CountRateTypeHandler;
use App\Handlers\UserRateHandler\GetUserMovieRateHandler;
use Illuminate\Database\Eloquent\Model;

class GetMoviePostDecorator extends EloquentMovieDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $movie = parent::getModel($attributes, $id);
        if ($movie != null) {
            $postHandleAttributes['movie_id'] = $movie['id'];
            $postHandleAttributes['limit'] = 3;
            $postHandleAttributes['order'] = 'desc';
            $postHandleAttributes['sort'] = 'created_at';

            $postHandler = new GetPaginatedPostsHandler();
            $individualRateHandler = new GetUserMovieRateHandler();
            $generalRateHandler = new CountAverageRateHandler();
            $rateTypeCountHandler = new CountRateTypeHandler();
            $relatedMovieHandler = new GetRelatedMovieHandler();

            $postHandler->setNextHandler($individualRateHandler);
            $individualRateHandler->setNextHandler($generalRateHandler);
            $generalRateHandler->setNextHandler($rateTypeCountHandler);
            $rateTypeCountHandler->setNextHandler($relatedMovieHandler);

            $postHandler->handle($postHandleAttributes);

            $movie['posts'] = $postHandleAttributes['paginatedPosts'];
            $movie['individualRate'] = $postHandleAttributes['userRate'];
            $movie['averageRate'] = $postHandleAttributes['averageRate'];
            $movie['countRate'] = $postHandleAttributes['countRate'];
            $movie['rateTypeCount'] = $postHandleAttributes['typeCount'];
            $movie['related'] = $postHandleAttributes['related'];
        }
        return $movie;
    }
}