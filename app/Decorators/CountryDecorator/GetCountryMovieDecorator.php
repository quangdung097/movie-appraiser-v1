<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:21 AM
 */

namespace App\Decorators\CountryDecorator;


use App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie\GetCountryPaginatedMovieHandler;
use Illuminate\Database\Eloquent\Model;

class GetCountryMovieDecorator extends EloquentCountryDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $country = parent::getModel($attributes, $id);
        $movieHandler = new GetCountryPaginatedMovieHandler();
        $attributes['relatedId'] = $country['id'];
        $movieHandler->handle($attributes);

        if (isset($country['movies'])) {
            unset($country['movies']);
        }
        $country['movies'] = $attributes['paginatedMovies'];

        return $country;
    }
}