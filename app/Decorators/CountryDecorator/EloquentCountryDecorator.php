<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/19/2019
 * Time: 11:54 PM
 */

namespace App\Decorators\CountryDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\CountryService;

class EloquentCountryDecorator extends EloquentDecorator
{
    public function __construct(CountryService $service)
    {
        parent::__construct($service);
    }
}