<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 10:25 AM
 */

namespace App\Decorators\GenreDecorator;


class GetMainGenreDecorator extends EloquentGenreDecorator
{
    public function getAll(array $attributes = [])
    {
        $attributes['limit'] = 12;
        $attributes['order'] = 'asc';
        $attributes['sort'] = 'created_at';
        return parent::getAll($attributes);
    }
}