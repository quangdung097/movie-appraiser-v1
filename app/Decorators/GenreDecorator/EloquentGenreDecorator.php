<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:36 AM
 */

namespace App\Decorators\GenreDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\GenreService;

class EloquentGenreDecorator extends EloquentDecorator
{
    public function __construct(GenreService $service)
    {
        parent::__construct($service);
    }
}