<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:39 AM
 */

namespace App\Decorators\GenreDecorator;


use App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie\GetGenrePaginatedMovieHandler;
use Illuminate\Database\Eloquent\Model;

class GetGenreMovieDecorator extends EloquentGenreDecorator
{
    public function getModel(array $attributes, $id): ?Model
    {
        $genre = parent::getModel($attributes, $id);
        $movieHandler = new GetGenrePaginatedMovieHandler();
        $attributes['relatedId'] = $genre['id'];
        $movieHandler->handle($attributes);

        if (isset($genre['movies'])) {
            unset($genre['movies']);
        }
        $genre['movies'] = $attributes['paginatedMovies'];

        return $genre;

    }
}