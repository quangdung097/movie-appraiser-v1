<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/29/2019
 * Time: 12:33 AM
 */

namespace App\Decorators\PostDecorator;


use App\Handlers\ImageHandler\Eloquent\CreateImage\CreatePostImageHandler;
use App\Handlers\ImageHandler\File\UploadImage\UploadPostImageHandler;
use Illuminate\Database\Eloquent\Model;

class UpdatePostDecorator extends EloquentPostDecorator
{
    public function updateModel(array $attributes, $id): bool
    {
        if (array_key_exists('image', $attributes)) {
            $uploadHandler = new UploadPostImageHandler();
            $createHandler = new CreatePostImageHandler();
            $uploadHandler->setNextHandler($createHandler);
            $uploadHandler->handle($attributes);
        }
        return parent::updateModel($attributes, $id);
    }
}