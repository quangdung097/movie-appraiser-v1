<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/27/2019
 * Time: 11:15 AM
 */

namespace App\Decorators\PostDecorator\GetPost;


use App\Decorators\PostDecorator\EloquentPostDecorator;
use App\Handlers\PostHandler\GetPost\GetPostMovieHandler;

class PostPaginateDecorator extends EloquentPostDecorator
{
    private static $MOVIE_RELATION = 'movie';
    public function paginate(array $attributes)
    {
        $paginatedPosts = parent::paginate($attributes);
        $handler = new GetPostMovieHandler();
        if (array_key_exists('relations', $attributes)) {
            if (in_array(self::$MOVIE_RELATION, $attributes['relations'])) {
                foreach ($paginatedPosts as &$post){
                    $handlerAttributes['post'] = $post;
                    $handler->handle($handlerAttributes);
                    $post = $handlerAttributes['post'];
                }
            }
        }
        return $paginatedPosts;
    }
}