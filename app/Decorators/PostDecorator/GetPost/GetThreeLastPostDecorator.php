<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/29/2019
 * Time: 1:27 AM
 */

namespace App\Decorators\PostDecorator\GetPost;


use App\Decorators\PostDecorator\EloquentPostDecorator;

class GetThreeLastPostDecorator extends EloquentPostDecorator
{
    public function getAll(array $attributes = [])
    {
        $attributes['relations'] = ['images'];
        $attributes['limit'] = 3;
        $attributes['order'] = 'desc';
        $attributes['sort'] = 'created_at';
        return parent::getAll($attributes);
    }
}