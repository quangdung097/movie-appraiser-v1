<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/15/2019
 * Time: 11:39 PM
 */

namespace App\Decorators\PostDecorator\GetPost;


use App\Decorators\PostDecorator\EloquentPostDecorator;
use App\Handlers\CommentHandler\GetPaginatedCommentsHandler;
use App\Handlers\PostHandler\GetPost\GetPostMovieHandler;
use Illuminate\Database\Eloquent\Model;

class PostGetDecorator extends EloquentPostDecorator
{
    private static $MOVIE_RELATION = 'movie';

    public function getAll(array $attributes = [])
    {
        $posts = parent::getAll($attributes);
        $handler = new GetPostMovieHandler();

        if (array_key_exists('relations', $attributes)) {
            if (in_array(self::$MOVIE_RELATION, $attributes['relations'])) {
                foreach ($posts as &$post){
                    $handlerAttributes['post'] = $post;
                    $handler->handle($handlerAttributes);
                    $post = $handlerAttributes['post'];
                }
            }
        }
        return $posts;
    }

    public function getModel(array $relations, $id): ?Model
    {
        $post = parent::getModel($relations, $id);
        $commentHandler = new GetPaginatedCommentsHandler();
        $attributes['post_id'] = $post['id'];
        $commentHandler->handle($attributes);

        if (isset($post['comments'])) {
            unset($post['comments']);
        }
        $post['comments'] = $attributes['paginatedComments'];

        return $post;
    }
}