<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/15/2019
 * Time: 11:37 PM
 */

namespace App\Decorators\PostDecorator;


use App\Decorators\EloquentDecorator;
use App\Services\PostService;

class EloquentPostDecorator extends EloquentDecorator
{
    public function __construct(PostService $service)
    {
        parent::__construct($service);
    }
}