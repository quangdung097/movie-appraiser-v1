<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 10:01 PM
 */

namespace App\Decorators;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class CreateTransactionDecorator extends EloquentDecorator
{
    public function createNewModel(array $attributes): ?Model
    {
        $model = null;
        DB::transaction(function () use ($attributes, $model){
           $model = parent::createNewModel($attributes);
           $checker = $this->attachCreate($model, $attributes);

           if ($checker == false) {
               DB::rollBack();
           }
        });
        return parent::createNewModel($attributes);
    }

    abstract public function attachCreate(Model &$model, $attributes): bool;
}