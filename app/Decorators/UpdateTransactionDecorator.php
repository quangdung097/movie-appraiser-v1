<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 10:06 PM
 */

namespace App\Decorators;


use Illuminate\Support\Facades\DB;

abstract class UpdateTransactionDecorator extends EloquentDecorator
{
    public function updateModel(array $attributes, $id): bool
    {
        $updateChecker = null;
        DB::transaction(function () use ($attributes,$id, $updateChecker) {
           $updateChecker = parent::updateModel($attributes, $id);
           $attachChecker = $this->attachUpdate($updateChecker, $attributes, $id);

           if ($attachChecker == false) {
               DB::rollBack();
           }
        });
        return parent::updateModel($attributes, $id);
    }

    abstract public function attachUpdate(bool $updateChecker, array $attributes, int $id) :bool;
}