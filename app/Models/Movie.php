<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
    protected $fillable = [
        'title', 'budget', 'country_id', 'poster_image_url', 'trailer_url', 'description',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

//    public function image()
//    {
//        return $this->belongsTo(Image::class);
//    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'genre_movie',
            'movie_id', 'genre_id');
    }

    public function actors()
    {
        return $this->belongsToMany(Actor::class, 'actor_movie',
            'movie_id', 'actor_id');
    }

    public function directors()
    {
        return $this->belongsToMany(Director::class, 'director_movie',
            'movie_id', 'director_id');
    }

    public function userRates()
    {
        return $this->hasMany(UserRate::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
