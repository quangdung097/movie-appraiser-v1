<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 12:33 AM
 */

namespace App\Models\News;


abstract class NewsAPI
{
    private static $apiURL = 'https://newsapi.org/v2/';
    private static $apiKey = 'bc71631a14ea4bfdbb111afc2a2e2230';

    public function formatURL(string $query): string
    {
        return self::$apiURL.$this->createExtension().'?apiKey='.self::$apiKey.'&q='.$query;
    }

    abstract public function createExtension(): string;
}