<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 12:45 AM
 */

namespace App\Models\News;


class EverythingNews extends NewsAPI
{
    private static $EVERYTHING = 'everything';
    public function createExtension(): string
    {
        return self::$EVERYTHING;
    }
}