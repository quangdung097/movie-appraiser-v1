<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 12:44 AM
 */

namespace App\Models\News;


class TopHeadLineNews extends NewsAPI
{
    private static $TOP_HEADLINE = 'top-headlines';
    public function createExtension(): string
    {
        return self::$TOP_HEADLINE;
    }
}