<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $fillable = [
        'username', 'password', 'role_id', 'avatar_image_id', 'name'
    ];

    protected $attributes = [
        'avatar_image_id' => 1
    ];

    protected $hidden = [
        'password'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'avatar_image_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function userRates()
    {
        return $this->hasMany(UserRate::class);
    }
}
