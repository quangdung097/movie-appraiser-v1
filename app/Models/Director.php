<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    //
    protected $fillable = [
        'name', 'DOB', 'country_id',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'director_movie',
            'director_id', 'movie_id');
    }
}
