<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    //
    protected $fillable = [
        'user_id', 'movie_id', 'rate',
    ];

    protected $table = 'user_rate';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id',
            'id');
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id',
            'id');
    }
}
