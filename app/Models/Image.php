<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = [
        'id', 'image_name', 'image_url',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

//    public function movie()
//    {
//        return $this->hasOne(Movie::class);
//    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'image_post',
            'image_id', 'post_id');
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'comment_image',
            'image_id', 'comment_id');
    }
}
