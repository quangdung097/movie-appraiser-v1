<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = [
        'country_name',
    ];

    public function movies()
    {
        return $this->hasMany(Movie::class);
    }

    public function directors()
    {
        return $this->hasMany(Director::class);
    }

    public function actors()
    {
        return $this->hasMany(Actor::class);
    }
}
