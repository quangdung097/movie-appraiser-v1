<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:24 AM
 */

namespace App\Services;


interface UserRateService extends Service
{
    public function getRatesWith(array $attributes, array $pairs);
}