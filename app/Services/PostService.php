<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:33 PM
 */

namespace App\Services;


interface PostService extends Service
{
    public function getMoviePaginatedPost(array $attributes, int $movieId);
}