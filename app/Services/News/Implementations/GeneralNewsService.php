<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:16 AM
 */

namespace App\Services\News\Implementations;


use App\Repositories\News\Implementations\GeneralNewsRepository;
use App\Repositories\News\NewsRepository;

class GeneralNewsService extends NewsAPINewsService
{

    public function createNewsRepository(): NewsRepository
    {
        return new GeneralNewsRepository();
    }
}