<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:16 AM
 */

namespace App\Services\News\Implementations;


abstract class NewsAPINewsService extends BaseNewsService
{
    public function getNews(array $attributes): array
    {
        $news = parent::getNews($attributes);
        if (array_key_exists('articles', $news)) {
            return $news['articles'];
        } else {
            return [];
        }
    }

}