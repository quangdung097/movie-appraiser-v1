<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:15 AM
 */

namespace App\Services\News\Implementations;


use App\Repositories\News\Implementations\HeadlineNewsRepository;
use App\Repositories\News\NewsRepository;

class HeadlineNewsService extends NewsAPINewsService
{

    public function createNewsRepository(): NewsRepository
    {
        return new HeadlineNewsRepository();
    }
}