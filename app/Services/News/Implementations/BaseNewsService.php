<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:10 AM
 */

namespace App\Services\News\Implementations;


use App\Repositories\News\NewsRepository;
use App\Services\News\NewsService;

abstract class BaseNewsService implements NewsService
{
    private $newsRepository;
    public function __construct()
    {
        $this->newsRepository = $this->createNewsRepository();
    }

    public function getNews(array $attributes): array
    {
        if (!array_key_exists('query', $attributes)) {
            return [];
        } else {
            //format API 's query for getting articles
            $this->newsRepository->formatQuery($attributes['query']);
            //send get request
            $jsonResponse = $this->newsRepository->requestNews();
            //decode json response
            $arrayResponse = $this->newsRepository->decode($jsonResponse);

            return $arrayResponse;
        }

    }

    abstract public function createNewsRepository(): NewsRepository;
}