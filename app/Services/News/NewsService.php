<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:10 AM
 */

namespace App\Services\News;


interface NewsService
{
    public function getNews(array $attributes): array;
}