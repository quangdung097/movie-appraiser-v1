<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 6:59 PM
 */

namespace App\Services;


use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;

interface Service
{
    public function createNewModel(array $attributes): ?Model;

    public function updateModel(array $attributes, $id): bool;

    public function deleteModel(array $attributes, $id): int;

    public function getModel(array $attributes, $id): ?Model;

    public function count(array $pairs = []): int;

    public function getAll(array $attributes= []);

    public function getBetween($needle, $from, $to, array $relations = []);

    public function getBy(array $pairs, array  $relations = []);

    public function paginate(array $attributes);
}