<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:45 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentRoleRepository;
use App\Repositories\Repository;
use App\Services\RoleService;

class EloquentRoleService extends EloquentService implements RoleService
{

    public function createRepository(): Repository
    {
        return new EloquentRoleRepository();
    }
}