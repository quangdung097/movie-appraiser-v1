<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:44 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentPostRepository;
use App\Repositories\PostRepository;
use App\Repositories\Repository;
use App\Services\PostService;

class EloquentPostService extends EloquentService implements PostService
{
    protected $manyToManyRelations = ['images'];

    public function createRepository(): Repository
    {
        return new EloquentPostRepository();
    }

    public function getMoviePaginatedPost(array $attributes, int $movieId)
    {
        /**
         * @var PostRepository $repository
         */
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'id';
        $repository = $this->getRepository();
        $condition = ['movie_id', '=', $movieId];
        return $repository->getPaginatedPost($condition, $limit, $order, $sort, $relations);
    }
}