<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:42 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentImageRepository;
use App\Repositories\Repository;
use App\Services\ImageService;

class EloquentImageService extends EloquentService implements ImageService
{
    protected $manyToManyRelations = ['posts', 'comments'];

    public function createRepository(): Repository
    {
        return new EloquentImageRepository();
    }
}