<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:36 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\CommentRepository;
use App\Repositories\Eloquent\EloquentCommentRepository;
use App\Repositories\Repository;
use App\Services\CommentService;

class EloquentCommentService extends EloquentService implements CommentService
{
    protected $manyToManyRelations = ['images'];

    public function createRepository(): Repository
    {
        return new EloquentCommentRepository();
    }

    public function getPaginatedPostComment(array $attributes, array $pairs = [])
    {
        /**
         * @var CommentRepository $repository
         */
        $attributes['conditions'] = $this->createConditions($pairs);
        $repository = $this->getRepository();
        return $repository->getPaginatedWithConditions($attributes);
    }
}