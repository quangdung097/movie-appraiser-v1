<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:39 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\DirectorRepository;
use App\Repositories\Eloquent\EloquentDirectorRepository;
use App\Repositories\Repository;
use App\Services\DirectorService;

class EloquentDirectorService extends EloquentService implements DirectorService
{
    protected $manyToManyRelations = ['movies'];

    public function createRepository(): Repository
    {
        return new EloquentDirectorRepository();
    }

    public function searchName(array $attributes)
    {
        /**
         * @var DirectorRepository $repository
         */
        $repository = $this->getRepository();
        return $repository->searchByName($attributes);
    }
}