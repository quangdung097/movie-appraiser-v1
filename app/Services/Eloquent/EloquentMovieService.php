<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:43 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentMovieRepository;
use App\Repositories\MovieRepository;
use App\Repositories\Repository;
use App\Services\MovieService;

class EloquentMovieService extends EloquentService implements MovieService
{
    protected $manyToManyRelations = ['genres', 'actors', 'directors'];

    public function createRepository(): Repository
    {
        return new EloquentMovieRepository();
    }

    public function getPaginatedMovie(array $attributes)
    {
        /**
         * @var MovieRepository $repository
         */
        $repository = $this->getRepository();
        return $repository->getPaginatedMovies($attributes);
    }

    public function getRandomMovie(array $attributes, array $pairs = [])
    {
        /**
         * @var MovieRepository $repository
         */
        $repository = $this->getRepository();
        $attributes['conditions'] = $this->createConditions($pairs);
        return $repository->getRandom($attributes);
    }

    public function searchName(array $attributes)
    {
        /**
         * @var MovieRepository $repository
         */
        $repository = $this->getRepository();
        return $repository->searchByName($attributes);
    }
}