<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:25 AM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentUserRateRepository;
use App\Repositories\Repository;
use App\Repositories\UserRateRepository;
use App\Services\UserRateService;

class EloquentUserRateService extends EloquentService implements UserRateService
{

    public function createRepository(): Repository
    {
        return new EloquentUserRateRepository();
    }

    public function getRatesWith(array $attributes, array $pairs)
    {
        /**
         * @var UserRateRepository $repository
         */
        $repository = $this->getRepository();
        $attributes['conditions'] = $this->createConditions($pairs);
        return $repository->getAllWithCondition($attributes);
    }
}