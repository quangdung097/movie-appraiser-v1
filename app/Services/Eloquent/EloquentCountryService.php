<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:38 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentCountryRepository;
use App\Repositories\Repository;
use App\Services\CountryService;

class EloquentCountryService extends EloquentService implements CountryService
{

    public function createRepository(): Repository
    {
        return new EloquentCountryRepository();
    }
}