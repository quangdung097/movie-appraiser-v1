<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:46 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentUserRepository;
use App\Repositories\Repository;
use App\Services\UserService;

class EloquentUserService extends EloquentService implements UserService
{
    protected $manyToManyRelations = ['movies'];

    public function createRepository(): Repository
    {
        return new EloquentUserRepository();
    }
}