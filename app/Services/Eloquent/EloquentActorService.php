<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:33 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\ActorRepository;
use App\Repositories\Eloquent\EloquentActorRepository;
use App\Repositories\Repository;
use App\Services\ActorService;

class EloquentActorService extends EloquentService implements ActorService
{
    protected $manyToManyRelations = ['movies'];

    public function createRepository(): Repository
    {
        return new EloquentActorRepository();
    }

    public function searchByName(array $attributes)
    {
        /**
         * @var ActorRepository $repository
         */
        $repository = $this->getRepository();
        return $repository->searchActor($attributes);
    }

    public function searchName(array $attributes)
    {
        /**
         * @var ActorRepository $repository
         */
        $repository = $this->getRepository();
        return $repository->searchByName($attributes);
    }
}