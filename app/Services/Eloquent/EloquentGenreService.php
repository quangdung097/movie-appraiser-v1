<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:40 PM
 */

namespace App\Services\Eloquent;


use App\Repositories\Eloquent\EloquentGenreRepository;
use App\Repositories\Repository;
use App\Services\GenreService;

class EloquentGenreService extends EloquentService implements GenreService
{
    protected $manyToManyRelations = ['movies'];

    public function createRepository(): Repository
    {
        return new EloquentGenreRepository();
    }
}