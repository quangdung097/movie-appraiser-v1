<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:08 PM
 */

namespace App\Services;


interface Message
{
    public function getMessage():string ;

    public function setMessage(string $message);
}