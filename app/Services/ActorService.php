<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:30 PM
 */

namespace App\Services;


interface ActorService extends Service
{
    public function searchByName(array $attributes);

    public function searchName(array $attributes);
}