<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:32 PM
 */

namespace App\Services;


interface MovieService extends Service
{
    public function getPaginatedMovie(array $attributes);

    public function getRandomMovie(array $attributes,array $pairs = []);

    public function searchName(array $attributes);
}