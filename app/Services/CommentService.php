<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:31 PM
 */

namespace App\Services;


interface CommentService extends Service
{
    public function getPaginatedPostComment(array $attributes, array $pairs = []);
}