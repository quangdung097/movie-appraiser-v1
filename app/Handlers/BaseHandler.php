<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:24 PM
 */

namespace App\Handlers;


use App\Handlers\HandlerAttributes\HandlerAttributes;
use App\Handlers\HandlerResponse\CreateResponse;
use App\Handlers\HandlerResponse\HandlerResponse;

class BaseHandler implements Handlerable
{
    use CreateResponse;
    /**
     * @var Handlerable $nextHandler
     */
    private $nextHandler;

    public function setNextHandler(Handlerable &$nextHandler)
    {
        $this->nextHandler = $nextHandler;
    }

    public function getNextHandler(): Handlerable
    {
        return $this->nextHandler;
    }

    public function handle(array &$attributes): HandlerResponse
    {
        if ($this->nextHandler != null) {
            return $this->nextHandler->handle($attributes);
        } else {
            return $this->createFinalResponse();
        }
    }

    public function createFinalResponse():HandlerResponse
    {
        return $this->createHandlerResponse('Finish', true);
    }
}