<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/24/2019
 * Time: 12:55 AM
 */

namespace App\Handlers\CommentHandler;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentCommentService;
use App\Services\Service;

class EloquentCommentHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentCommentService();
    }
}