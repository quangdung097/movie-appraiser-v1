<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/24/2019
 * Time: 12:56 AM
 */

namespace App\Handlers\CommentHandler;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Services\CommentService;

class GetPaginatedCommentsHandler extends EloquentCommentHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('post_id', $attributes)) {
            return parent::handle($attributes);
        }
        /**
         * @var CommentService $service
         */
        $service = $this->createHandlerService();
        $pairs = [
            [
                'needle' => 'post_id',
                'value' => $attributes['post_id']
            ]
        ];
        $commentAttributes['limit'] = 5;
        $commentAttributes['sort'] = 'created_at';
        $commentAttributes['order'] = 'desc';

        $attributes['paginatedComments'] = $service->getPaginatedPostComment($commentAttributes, $pairs);

        return parent::handle($attributes);
    }
}