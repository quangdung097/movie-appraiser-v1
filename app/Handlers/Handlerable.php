<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:15 PM
 */

namespace App\Handlers;


use App\Handlers\HandlerAttributes\HandlerAttributes;
use App\Handlers\HandlerResponse\HandlerResponse;

interface Handlerable
{
    public function setNextHandler(Handlerable &$nextHandler);

    public function getNextHandler(): Handlerable;

    public function handle(array &$attributes): HandlerResponse;
}