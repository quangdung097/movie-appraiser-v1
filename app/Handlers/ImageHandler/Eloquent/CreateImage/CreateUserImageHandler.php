<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 12:39 AM
 */

namespace App\Handlers\ImageHandler\Eloquent\CreateImage;


use App\Handlers\ImageHandler\Eloquent\CreateBookImageHandler;

class CreateUserImageHandler extends CreateBookImageHandler
{

    public function addImageField(array &$attributes, int $id)
    {
        $attributes['avatar_image_id'] = $id;
    }
}