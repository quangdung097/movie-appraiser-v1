<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 12:31 AM
 */

namespace App\Handlers\ImageHandler\Eloquent;


use App\Handlers\HandlerResponse\HandlerResponse;

abstract class CreateBookImageHandler extends EloquentImageHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('image_url', $attributes) && !array_key_exists('image_name', $attributes)) {
            return parent::handle($attributes);
        }
        $imageService = $this->createHandlerService();

        $image = $imageService->createNewModel($attributes);

        unset($attributes['image_url']);
        unset($attributes['image_name']);

        if ($image != null) {
            $this->addImageField($attributes, $image['id']);
        }
        return parent::handle($attributes);
    }

    abstract public function addImageField(array &$attributes, int $id);
}