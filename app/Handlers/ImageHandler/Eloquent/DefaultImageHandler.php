<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 12:14 AM
 */

namespace App\Handlers\ImageHandler\Eloquent;


use App\Handlers\HandlerResponse\HandlerResponse;

abstract class DefaultImageHandler extends EloquentImageHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $imageService = $this->createHandlerService();
        $pairs = [
          [
              'needle' => 'image_name',
              'value' => 'No Image'
          ]
        ];
        $image = $imageService->getBy($pairs);
        $this->updateImage($attributes, $image['id']);

        return parent::handle($attributes);
    }

    abstract public function updateImage(array &$attributes, int $imageId);
}