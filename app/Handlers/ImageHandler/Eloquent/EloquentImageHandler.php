<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 12:19 AM
 */

namespace App\Handlers\ImageHandler\Eloquent;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentImageService;
use App\Services\Service;

class EloquentImageHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentImageService();
    }
}