<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 12:25 AM
 */

namespace App\Handlers\ImageHandler\Eloquent\DefaultImageHandlers;


use App\Handlers\ImageHandler\Eloquent\DefaultImageHandler;

class SignInDefaultImageHandler extends DefaultImageHandler
{

    public function updateImage(array &$attributes, int $imageId)
    {
        $attributes['avatar_image_id'] = $imageId;
    }
}