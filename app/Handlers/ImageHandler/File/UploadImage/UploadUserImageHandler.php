<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 12:45 AM
 */

namespace App\Handlers\ImageHandler\File\UploadImage;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\ImageHandler\File\UploadImageHandler;

class UploadUserImageHandler extends UploadImageHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $attributes['file'] = $attributes['avatar'];
        unset($attributes['avatar']);
        return parent::handle($attributes);
    }
}