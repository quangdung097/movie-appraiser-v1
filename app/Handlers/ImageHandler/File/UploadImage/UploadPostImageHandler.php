<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/29/2019
 * Time: 12:34 AM
 */

namespace App\Handlers\ImageHandler\File\UploadImage;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\ImageHandler\File\UploadImageHandler;

class UploadPostImageHandler extends UploadImageHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $attributes['file'] = $attributes['image'];
        unset($attributes['image']);
        return parent::handle($attributes);
    }
}
