<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:07 AM
 */

namespace App\Handlers\UserRateHandler;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentUserRateService;
use App\Services\Service;

class EloquentUserRateHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentUserRateService();
    }
}