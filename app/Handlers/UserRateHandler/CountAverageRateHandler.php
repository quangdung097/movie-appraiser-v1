<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:23 AM
 */

namespace App\Handlers\UserRateHandler;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Services\UserRateService;

class CountAverageRateHandler extends EloquentUserRateHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return parent::handle($attributes);
        }
        $pairs = [
            [
                'needle' => 'movie_id',
                'value' => $attributes['movie_id']
            ]
        ];
        /**
         * @var UserRateService $userRateService
         */
        $userRateService = $this->createHandlerService();

        $userRates = $userRateService->getRatesWith([], $pairs);

        $count = 0;
        $average = 0;
        foreach ($userRates as $userRate) {
            $average += $userRate['rate'];
            $count ++;
        }

        if ($count != 0) {
            $average = round($average/$count, 1);
        }

        $attributes['averageRate'] = $average;
        $attributes['countRate'] = $count;

        return parent::handle($attributes);
    }
}