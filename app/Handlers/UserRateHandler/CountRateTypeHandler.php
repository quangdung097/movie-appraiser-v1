<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 3:01 AM
 */

namespace App\Handlers\UserRateHandler;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\UserRateHandler\CountRateType\CountFiveStarHandler;
use App\Handlers\UserRateHandler\CountRateType\CountFourStarHandler;
use App\Handlers\UserRateHandler\CountRateType\CountOneStarHandler;
use App\Handlers\UserRateHandler\CountRateType\CountThreeStarHandler;
use App\Handlers\UserRateHandler\CountRateType\CountTwoStarHandler;

class CountRateTypeHandler extends EloquentUserRateHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return parent::handle($attributes);
        }
        if (!array_key_exists('countRate', $attributes)) {
            return parent::handle($attributes);
        }

        $countOneHandler = new CountOneStarHandler();
        $countTwoHandler = new CountTwoStarHandler();
        $countThreeHandler = new CountThreeStarHandler();
        $countFourHandler = new CountFourStarHandler();
        $countFiveHandler = new CountFiveStarHandler();

        $countOneHandler->setNextHandler($countTwoHandler);
        $countTwoHandler->setNextHandler($countThreeHandler);
        $countThreeHandler->setNextHandler($countFourHandler);
        $countFourHandler->setNextHandler($countFiveHandler);

        $countTypeAttributes['movie_id'] = $attributes['movie_id'];
        $countTypeAttributes['countRate'] = $attributes['countRate'];

        $countOneHandler->handle($countTypeAttributes);

        unset($countTypeAttributes['movie_id']);
        $attributes['typeCount'] = $countTypeAttributes;

        return parent::handle($attributes);
    }
}