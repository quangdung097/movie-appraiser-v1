<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:52 AM
 */

namespace App\Handlers\UserRateHandler\CountRateType;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\UserRateHandler\EloquentUserRateHandler;
use App\Services\UserRateService;

abstract class BaseCountRateTypeHandler extends EloquentUserRateHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return parent::handle($attributes);
        }
        if (!array_key_exists('countRate', $attributes)) {
            return parent::handle($attributes);
        }
        $pairs = [
            [
                'needle' => 'movie_id',
                'value' => $attributes['movie_id']
            ],
            [
                'needle' => 'rate',
                'value' => $this->createRateType()
            ]
        ];
        /**
         * @var UserRateService $userRateService
         */
        $userRateService = $this->createHandlerService();

        $count = $userRateService->count($pairs);

        $attributes[$this->createRateTypeKey()] = $count;
        $countRate = $attributes['countRate'];
        if ( $countRate == 0) {
            $attributes[$this->createRateTypeKey().'Percent'] = 0;
        } else {
            $attributes[$this->createRateTypeKey().'Percent'] = $count/$countRate*100;
        }

        return parent::handle($attributes);
    }

    abstract public function createRateType(): int;

    abstract public function createRateTypeKey(): string;
}