<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:59 AM
 */

namespace App\Handlers\UserRateHandler\CountRateType;


class CountFourStarHandler extends BaseCountRateTypeHandler
{
    private static $RATE_TYPE = 4;
    private static $RATE_TYPE_KEY = 'rateFour';
    public function createRateType(): int
    {
        return self::$RATE_TYPE;
    }

    public function createRateTypeKey(): string
    {
        return self::$RATE_TYPE_KEY;
    }
}