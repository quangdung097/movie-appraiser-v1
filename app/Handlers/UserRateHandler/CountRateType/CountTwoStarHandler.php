<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:58 AM
 */

namespace App\Handlers\UserRateHandler\CountRateType;


class CountTwoStarHandler extends BaseCountRateTypeHandler
{
    private static $RATE_TYPE = 2;
    private static $RATE_TYPE_KEY = 'rateTwo';
    public function createRateType(): int
    {
        return self::$RATE_TYPE;
    }

    public function createRateTypeKey(): string
    {
        return self::$RATE_TYPE_KEY;
    }
}