<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 3:00 AM
 */

namespace App\Handlers\UserRateHandler\CountRateType;


class CountFiveStarHandler extends BaseCountRateTypeHandler
{
    private static $RATE_TYPE = 5;
    private static $RATE_TYPE_KEY = 'rateFive';
    public function createRateType(): int
    {
        return self::$RATE_TYPE;
    }

    public function createRateTypeKey(): string
    {
        return self::$RATE_TYPE_KEY;
    }
}