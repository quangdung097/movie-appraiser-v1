<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:54 AM
 */

namespace App\Handlers\UserRateHandler\CountRateType;


class CountOneStarHandler extends BaseCountRateTypeHandler
{
    private static $RATE_TYPE = 1;
    private static $RATE_TYPE_KEY = 'rateOne';
    public function createRateType(): int
    {
        return self::$RATE_TYPE;
    }

    public function createRateTypeKey(): string
    {
        return self::$RATE_TYPE_KEY;
    }
}