<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 2:08 AM
 */

namespace App\Handlers\UserRateHandler;


use App\Handlers\HandlerResponse\HandlerResponse;

class GetUserMovieRateHandler extends EloquentUserRateHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return parent::handle($attributes);
        }
        $pairs = [
            [
                'needle' => 'user_id',
                'value' => session('user_id')
            ],
            [
                'needle' => 'movie_id',
                'value' => $attributes['movie_id']
            ]
        ];
        $userRateService = $this->createHandlerService();

        $userRate = $userRateService->getBy($pairs);

        $attributes['userRate'] = $userRate;
        return parent::handle($attributes);
    }
}