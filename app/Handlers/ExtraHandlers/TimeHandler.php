<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/20/2019
 * Time: 1:23 AM
 */

namespace App\Handlers\ExtraHandlers;


use App\Handlers\BaseHandler;
use App\Handlers\HandlerAttributes\HandlerAttributes;
use App\Handlers\HandlerResponse\HandlerResponse;

abstract class TimeHandler extends BaseHandler
{
    public function handle(HandlerAttributes &$attributes): HandlerResponse
    {
        $rawTime = $this->geTime($attributes);

        if ($rawTime == null) {
            return $this->createGetTimeMessage();
        }

        $time = date('Y-m-d', strtotime($rawTime));

        $this->setTime($attributes, $time);

        return parent::handle($attributes);
    }

    abstract public function geTime(HandlerAttributes &$attributes): ?string;

    abstract public function createGetTimeMessage(): HandlerResponse;

    abstract public function setTime(HandlerAttributes &$attributes, $date);
}