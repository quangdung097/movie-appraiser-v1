<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/3/2019
 * Time: 5:40 PM
 */

namespace App\Handlers\ExtraHandlers;


use App\Handlers\BaseHandler;
use App\Handlers\HandlerAttributes\HandlerAttributes;
use App\Handlers\HandlerResponse\HandlerResponse;

class GMTimeHandler extends BaseHandler
{
    public function handle(HandlerAttributes &$attributes): HandlerResponse
    {
        $model = &$attributes->getModel();
        $createdTime = $model['created_at'];
        $a = gmdate("M d Y H:i:s",
            strtotime($createdTime));
        unset($model['created_at'] );
        $model['createdAt'] = $a;
        return parent::handle($attributes);
    }
}