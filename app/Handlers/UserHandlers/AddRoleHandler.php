<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 12:07 AM
 */

namespace App\Handlers\UserHandlers;


use App\Handlers\BaseEloquentHandler;
use App\Handlers\HandlerResponse\HandlerResponse;
use App\Services\Eloquent\EloquentRoleService;
use App\Services\Service;

class AddRoleHandler extends BaseEloquentHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $service = $this->createHandlerService();
        $pairs = [
            [
                'needle' => 'role_type',
                'value' => 'user'
            ]
        ];
        $role = $service->getBy($pairs);
        $attributes['role_id'] = $role['id'];

        return parent::handle($attributes);
    }

    public function createHandlerService(): Service
    {
        return new EloquentRoleService();
    }
}