<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:35 PM
 */

namespace App\Handlers\UserHandlers;


use App\Handlers\BaseHandler;
use App\Handlers\HandlerResponse\HandlerResponse;

class HashPasswordHandler extends BaseHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $password = $attributes['password'];
        $hashPassword = hash('md5', $password);
        $attributes['password'] = $hashPassword;

        return parent::handle($attributes);
    }
}