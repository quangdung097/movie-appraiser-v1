<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:07 PM
 */

namespace App\Handlers\UserHandlers;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentUserService;
use App\Services\Service;

class EloquentUserHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentUserService();
    }
}