<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:07 PM
 */

namespace App\Handlers\UserHandlers;


use App\Handlers\HandlerResponse\HandlerResponse;

class GetNewUserHandler extends EloquentUserHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $userService = $this->createHandlerService();
        $attributes['newUsers'] = [
          'count' => count($userService->getBetween('created_at', 20190501, 20190531)),
        ];
        return parent::handle($attributes);
    }
}