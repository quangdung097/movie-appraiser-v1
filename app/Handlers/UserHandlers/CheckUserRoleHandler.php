<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/29/2019
 * Time: 9:11 AM
 */

namespace App\Handlers\UserHandlers;


use App\Handlers\HandlerResponse\HandlerResponse;

class CheckUserRoleHandler extends EloquentUserHandler
{
    private static $MISSING_UPDATED_ID_ERROR = 'Missing updated user id';
    private static $UNAUTHORIZED_ACCOUNT_ERROR = 'This account is not authorized';

    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('updated_user_id', $attributes)) {
            return $this->createHandlerResponse(self::$MISSING_UPDATED_ID_ERROR, false);
        }

        $sessionId = session('user_id');
        if ($sessionId == null) {
            return $this->createHandlerResponse(self::$UNAUTHORIZED_ACCOUNT_ERROR, false);
        }

        if ($sessionId != $attributes['updated_user_id']) {
            $userService = $this->createHandlerService();
            $accountUser = $userService->getModel(['role'], $sessionId);

            $accountUserRole = $accountUser['role'];
            if (strcasecmp($accountUserRole['role_type'], 'admin') == 0) {
                return parent::handle($attributes);
            }
            return $this->createHandlerResponse(self::$UNAUTHORIZED_ACCOUNT_ERROR, false);
        }
        return parent::handle($attributes);
    }
}