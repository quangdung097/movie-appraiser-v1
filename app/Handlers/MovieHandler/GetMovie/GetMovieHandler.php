<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/4/2019
 * Time: 4:38 PM
 */

namespace App\Handlers\MovieHandler\GetMovie;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\MovieHandler\EloquentMovieHandler;

class GetMovieHandler extends EloquentMovieHandler
{
    private static $NULL_MOVIE_ID = 'Invalid movie id';
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movieId', $attributes)) {
            return $this->createHandlerResponse(self::$NULL_MOVIE_ID, false);
        }
        $movieService = $this->createHandlerService();
        $movie = $movieService->getModel($this->createMovieRelations(), $attributes['movieId']);

        $attributes['movie'] = $movie;
        return parent::handle($attributes);
    }

    public function createMovieRelations(): array
    {
        return [];
    }
}