<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:14 AM
 */

namespace App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie;


use App\Handlers\MovieHandler\GetMovie\GetPaginateMovieHandler;

class GetCountryPaginatedMovieHandler extends GetPaginateMovieHandler
{

    public function createCondition(int $relatedId): array
    {
        return ['country_id', '=', $relatedId];
    }
}