<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 1:12 AM
 */

namespace App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\MovieHandler\GetMovie\GetPaginateMovieHandler;

abstract class GetMTMMovieHandler extends GetPaginateMovieHandler
{
    abstract public function createJoinedTable(): string;

    public function handle(array &$attributes): HandlerResponse
    {
        $attributes['table'] = $this->createJoinedTable();
        return parent::handle($attributes);
    }
}