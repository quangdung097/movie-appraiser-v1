<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:14 AM
 */

namespace App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie;



class GetActorPaginatedMovieHandler extends GetMTMMovieHandler
{

    public function createCondition(int $actorId): array
    {
        return ['actor_id', '=', $actorId];
    }

    public function createJoinedTable(): string
    {
        return 'actor_movie';
    }
}