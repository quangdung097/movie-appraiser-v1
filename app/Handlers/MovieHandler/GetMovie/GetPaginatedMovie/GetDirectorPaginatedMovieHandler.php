<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:14 AM
 */

namespace App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie;



class GetDirectorPaginatedMovieHandler extends GetMTMMovieHandler
{

    public function createCondition(int $directorId): array
    {
        return ['director_id', '=', $directorId];
    }

    public function createJoinedTable(): string
    {
        return 'director_movie';
    }
}