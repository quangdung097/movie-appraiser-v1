<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 12:14 AM
 */

namespace App\Handlers\MovieHandler\GetMovie\GetPaginatedMovie;



class GetGenrePaginatedMovieHandler extends GetMTMMovieHandler
{

    public function createCondition(int $relatedId): array
    {
        return ['genre_id', '=', $relatedId];
    }

    public function createJoinedTable(): string
    {
        return 'genre_movie';
    }
}