<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/19/2019
 * Time: 11:56 PM
 */

namespace App\Handlers\MovieHandler\GetMovie;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\MovieHandler\EloquentMovieHandler;
use App\Services\MovieService;

abstract class GetPaginateMovieHandler extends EloquentMovieHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('relatedId', $attributes)) {
            return parent::handle($attributes);
        }
        /**
         * @var MovieService $movieService
         */

        $movieService = $this->createHandlerService();
        $paginatedAttributes['relations'] = ['actors', 'directors', 'genres', 'country'];
        $paginatedAttributes['limit'] = isset($attributes['limit']) ? $attributes['limit'] : 4;
        $paginatedAttributes['table'] = isset($attributes['table']) ? $attributes['table'] : null;
        $paginatedAttributes['condition'] = $this->createCondition($attributes['relatedId']);
        $attributes['paginatedMovies'] = $movieService->getPaginatedMovie($paginatedAttributes);

        return parent::handle($attributes);
    }

    abstract public function createCondition(int $relatedId): array;
}