<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/4/2019
 * Time: 4:45 PM
 */

namespace App\Handlers\MovieHandler\GetMovie;


class GetExtendMovieHandler extends GetMovieHandler
{
    public function createMovieRelations(): array
    {
        return ['actors', 'directors', 'genres', 'country', 'userRates'];
    }
}