<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 4:41 AM
 */

namespace App\Handlers\MovieHandler;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Services\MovieService;

class GetRelatedMovieHandler extends EloquentMovieHandler
{
    private static $NULL_MOVIE_ID = 'Invalid movie id';
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return $this->createHandlerResponse(self::$NULL_MOVIE_ID, false);
        }
        /**
         * @var MovieService $movieService
         */
        $movieService = $this->createHandlerService();

        $getAttributes['limit'] = 3;
        $attributes['related'] = $movieService->getRandomMovie($attributes);

        return parent::handle($attributes);
    }
}