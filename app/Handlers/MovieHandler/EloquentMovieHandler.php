<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/16/2019
 * Time: 12:23 AM
 */

namespace App\Handlers\MovieHandler;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentMovieService;
use App\Services\Service;

class EloquentMovieHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentMovieService();
    }
}