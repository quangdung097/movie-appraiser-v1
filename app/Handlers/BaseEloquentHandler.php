<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:30 PM
 */

namespace App\Handlers;


use App\Services\Service;

abstract class BaseEloquentHandler extends BaseHandler
{
    abstract public function createHandlerService() : Service;
}