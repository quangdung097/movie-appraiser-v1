<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/25/2019
 * Time: 10:55 PM
 */

namespace App\Handlers;


use App\Services\File\FileService;

abstract class BaseFileHandler extends BaseHandler
{
    abstract public function createHandlerFileService(): FileService;
}