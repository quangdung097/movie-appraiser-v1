<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:17 PM
 */

namespace App\Handlers\HandlerResponse;


class HandlerResponse
{
    private $message;
    private $status;

    private static $RESPONSE;

    private function __construct()
    {
        $this->message = '';
        $this->status = false;
    }

    public static function getInstance(): HandlerResponse
    {
        if (self::$RESPONSE == null) {
            self::$RESPONSE = new HandlerResponse();
        }
        return self::$RESPONSE;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }




}