<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:20 PM
 */

namespace App\Handlers\HandlerResponse;


trait CreateResponse
{
    public function createHandlerResponse(string $content, bool $status)
    {
        $response = HandlerResponse::getInstance();
        $response->setMessage($content);
        $response->setStatus($status);

        return $response;
    }
}