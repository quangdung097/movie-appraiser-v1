<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:13 PM
 */

namespace App\Handlers\PostHandler;


use App\Handlers\HandlerResponse\HandlerResponse;

class GetLastPostHandler extends EloquentPostHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $postService = $this->createHandlerService();

        $getAttributes['limit'] = 1;
        $getAttributes['order'] = 'desc';
        $getAttributes['sort'] = 'created_at';

        $attributes['lastPost'] = $postService->getAll($getAttributes);
        return parent::handle($attributes);
    }
}