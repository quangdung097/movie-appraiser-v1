<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/4/2019
 * Time: 4:49 PM
 */

namespace App\Handlers\PostHandler\GetPost;


use App\Handlers\BaseHandler;
use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\MovieHandler\GetMovie\GetExtendMovieHandler;

class GetPostMovieHandler extends BaseHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $post = $attributes['post'];
        $movieHandler = new GetExtendMovieHandler();
        $handlerAttributes = [];

        $movie = $post['movie'];
        $handlerAttributes['movieId'] = $movie['id'];
        $movieHandler->handle($handlerAttributes);
        unset($post['movie']);
        $post['movie'] = $handlerAttributes['movie'];
        $attributes['post'] = $post;
        return parent::handle($attributes);
    }
}