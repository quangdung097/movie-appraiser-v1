<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/21/2019
 * Time: 9:46 PM
 */

namespace App\Handlers\PostHandler\GetPost;


use App\Handlers\HandlerResponse\HandlerResponse;
use App\Handlers\PostHandler\EloquentPostHandler;
use App\Services\PostService;

class GetPaginatedPostsHandler extends EloquentPostHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        if (!array_key_exists('movie_id', $attributes)) {
            return parent::handle($attributes);
        }
        /**
         * @var PostService $service
         */
        $service = $this->createHandlerService();
        $attributes['paginatedPosts'] = $service->getMoviePaginatedPost($attributes, $attributes['movie_id']);

        return parent::handle($attributes);
    }
}