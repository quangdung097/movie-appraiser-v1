<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 11:13 PM
 */

namespace App\Handlers\PostHandler;


use App\Handlers\HandlerResponse\HandlerResponse;

class GetNewPostHandler extends EloquentPostHandler
{
    public function handle(array &$attributes): HandlerResponse
    {
        $postService = $this->createHandlerService();
        $attributes['newPosts'] = [
            'count' => count($postService->getBetween('created_at', 20190501, 20190531)),
        ];
        return parent::handle($attributes);
    }
}