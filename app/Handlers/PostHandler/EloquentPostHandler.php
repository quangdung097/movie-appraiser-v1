<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/4/2019
 * Time: 3:06 PM
 */

namespace App\Handlers\PostHandler;


use App\Handlers\BaseEloquentHandler;
use App\Services\Eloquent\EloquentPostService;
use App\Services\Service;

class EloquentPostHandler extends BaseEloquentHandler
{

    public function createHandlerService(): Service
    {
        return new EloquentPostService();
    }
}