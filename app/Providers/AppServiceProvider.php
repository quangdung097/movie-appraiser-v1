<?php

namespace App\Providers;

use App\Services\ActorService;
use App\Services\CommentService;
use App\Services\CountryService;
use App\Services\DirectorService;
use App\Services\Eloquent\EloquentActorService;
use App\Services\Eloquent\EloquentCommentService;
use App\Services\Eloquent\EloquentCountryService;
use App\Services\Eloquent\EloquentDirectorService;
use App\Services\Eloquent\EloquentGenreService;
use App\Services\Eloquent\EloquentImageService;
use App\Services\Eloquent\EloquentMovieService;
use App\Services\Eloquent\EloquentPostService;
use App\Services\Eloquent\EloquentRoleService;
use App\Services\Eloquent\EloquentService;
use App\Services\Eloquent\EloquentUserRateService;
use App\Services\Eloquent\EloquentUserService;
use App\Services\GenreService;
use App\Services\ImageService;
use App\Services\MovieService;
use App\Services\PostService;
use App\Services\RoleService;
use App\Services\Service;
use App\Services\UserRateService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        $this->app->singleton(Service::class, EloquentService::class);
        $this->app->singleton(ActorService::class, EloquentActorService::class);
        $this->app->singleton(CommentService::class, EloquentCommentService::class);
        $this->app->singleton(CountryService::class, EloquentCountryService::class);
        $this->app->singleton(DirectorService::class, EloquentDirectorService::class);
        $this->app->singleton(GenreService::class, EloquentGenreService::class);
        $this->app->singleton(ImageService::class, EloquentImageService::class);
        $this->app->singleton(MovieService::class, EloquentMovieService::class);
        $this->app->singleton(PostService::class, EloquentPostService::class);
        $this->app->singleton(RoleService::class, EloquentRoleService::class);
        $this->app->singleton(UserService::class, EloquentUserService::class);
        $this->app->singleton(UserRateService::class, EloquentUserRateService::class);
    }
}
