<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:19 AM
 */

namespace App\Http\Controllers\News;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Requests\News\NewsGetRequest;
use App\Services\News\NewsService;

class NewsController extends Controller
{
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    public function _get(NewsGetRequest $request)
    {
        $attributes['query'] = $request->getQuery();
        return $this->newsService->getNews($attributes);
    }
}