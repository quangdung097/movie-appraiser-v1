<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:37 AM
 */

namespace App\Http\Controllers\News;


use App\Http\Controllers\Requests\News\NewsAPI\NewsAPINewsGetRequest;
use App\Services\News\Implementations\GeneralNewsService;

class GeneralNewsController extends NewsController
{
    public function __construct(GeneralNewsService $newsService)
    {
        parent::__construct($newsService);
    }

    public function get(NewsAPINewsGetRequest $request)
    {
        return parent::_get($request);
    }
}