<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:35 AM
 */

namespace App\Http\Controllers\News;


use App\Http\Controllers\Requests\News\NewsAPI\NewsAPINewsGetRequest;
use App\Services\News\Implementations\HeadlineNewsService;

class HeadlineNewsController extends NewsController
{
    public function __construct(HeadlineNewsService $newsService)
    {
        parent::__construct($newsService);
    }

    public function get(NewsAPINewsGetRequest $request)
    {
        return parent::_get($request);
    }
}