<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/26/2019
 * Time: 10:36 PM
 */

namespace App\Http\Controllers\View;


use App\Services\ActorService;
use App\Services\DirectorService;
use App\Services\MovieService;
use Illuminate\Http\Request;

class SearchController extends ViewController
{
    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = ['country'];

    private static $CONTENT_KEY = 'movies';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.searchPages.account.accountMovieResult';
    private static $INIT_CONTENT_PAGE = 'pages.searchPages.initial.initMovieResult';

    private $actorService;
    private $directorService;
    public function __construct(MovieService $service, ActorService $actorService, DirectorService $directorService)
    {
        parent::__construct($service);
        $this->actorService = $actorService;
        $this->directorService = $directorService;
    }

    public function getActorPage(Request $request)
    {
        $this->setService($this->actorService);
        self::$CONTENT_KEY = 'actors';
        self::$INIT_CONTENT_PAGE = 'pages.searchPages.initial.initActorResult';
        self::$ACCOUNT_CONTENT_PAGE = 'pages.searchPages.account.accountActorResult';
        return $this->getPage($request);
    }

    public function getDirectorPage(Request $request)
    {
        $this->setService($this->directorService);
        self::$CONTENT_KEY = 'directors';
        self::$INIT_CONTENT_PAGE = 'pages.searchPages.initial.initDirectorResult';
        self::$ACCOUNT_CONTENT_PAGE = 'pages.searchPages.account.accountDirectorResult';
        return $this->getPage($request);
    }

    public function getPage(Request $request)
    {
        $extends = [];
        $model = parent::getService()->searchName($request->all());
        if ($model == null) {
            return redirect('/');
        }
        $userId = $request->session()->get('user_id');
        if ($userId == null) {
            return view($this->createInitContentPage(), [
                $this->createContentKey() => $model,
                'extends' => $extends
            ]);
        } else {
            $user = $this->getUserService()->getModel([], $userId);
            return view($this->createAccountContentPage(), [
                $this->createContentKey() => $model,
                'user' => $user,
                'extends' => $extends
            ]);
        }
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function getService() : MovieService
    {
        return parent::getService();
    }


}