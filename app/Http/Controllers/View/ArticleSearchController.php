<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/28/2019
 * Time: 8:54 AM
 */

namespace App\Http\Controllers\View;


use App\Services\News\Implementations\GeneralNewsService;
use App\Services\News\Implementations\HeadlineNewsService;
use App\Services\UserService;
use Illuminate\Http\Request;

class ArticleSearchController extends ViewController
{
    private static $CONTENT_KEY = 'articles';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.searchPages.account.accountArticleResult';
    private static $INIT_CONTENT_PAGE = 'pages.searchPages.initial.initArticleResult';

    private $newsService;
    private $headlineNewsService;

    public function __construct(UserService $service, GeneralNewsService $newsService,
                                HeadlineNewsService $headlineNewsService)
    {
        parent::__construct($service);
        $this->newsService = $newsService;
        $this->headlineNewsService = $headlineNewsService;
    }

    public function getGeneralNewsPage(Request $request)
    {
        return $this->getPage($request, true);
    }

    public function getHeadlineNewsPage(Request $request)
    {
        $this->newsService = $this->headlineNewsService;
        return $this->getPage($request, false);
    }


    public function getPage(Request $request, bool $checkPoint)
    {
        $newsAttributes['query'] = $request->get('q');
        $extends['query'] = $newsAttributes['query'];
        $extends['checkpoint'] = $checkPoint;
        $model = $this->newsService->getNews($newsAttributes);

        $userId = $request->session()->get('user_id');
        if ($userId == null) {
            return view($this->createInitContentPage(), [
                $this->createContentKey() => $model,
                'extends' => $extends
            ]);
        } else {
            $user = $this->getUserService()->getModel([], $userId);
            return view($this->createAccountContentPage(), [
                $this->createContentKey() => $model,
                'user' => $user,
                'extends' => $extends
            ]);
        }
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}