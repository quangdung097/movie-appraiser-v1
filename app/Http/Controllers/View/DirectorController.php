<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 11:39 PM
 */

namespace App\Http\Controllers\View;


use App\Decorators\DirectorDecorator\GetDirectorMovieDecorator;
use App\Services\CountryService;
use App\Services\DirectorService;
use Illuminate\Http\Request;

class DirectorController extends ViewController
{
    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = ['country'];

    private static $CONTENT_KEY = 'director';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedDirector';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedDirector';

    private $countryService;
    private $getService;

    public function __construct(DirectorService $service, CountryService $countryService,
                                GetDirectorMovieDecorator $getService)
    {
        parent::__construct($service);
        $this->countryService = $countryService;
        $this->getService = $getService;
    }

    public function adminDirectorPaginate(Request $request)
    {
        $extends['countries'] = $this->countryService->getAll();
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION,
            $extends);
    }

    public function adminPostDirector(Request $request)
    {
        $extends['countries'] = $this->countryService->getAll();
        return parent::getAdminView($request,
            'pages.createPages.createDirector.createDirector',
            $extends);
    }

    public function getPage(Request $request, int $id)
    {
        $relations = ['movies'];
        $this->setService($this->getService);
        $a =  parent::getContentPage($request, $relations, $id);
        return $a;
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return 'pages.directors';
    }

    public function createPaginateKey(): string
    {
        return 'directors';
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}