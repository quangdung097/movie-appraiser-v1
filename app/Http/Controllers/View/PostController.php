<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 1:07 AM
 */

namespace App\Http\Controllers\View;


use App\Decorators\GenreDecorator\GetMainGenreDecorator;
use App\Decorators\MovieDecorator\GetCreatePostMovieDecorator;
use App\Decorators\PostDecorator\GetPost\GetThreeLastPostDecorator;
use App\Decorators\PostDecorator\GetPost\PostGetDecorator;
use App\Decorators\PostDecorator\GetPost\PostPaginateDecorator;
use App\Services\News\Implementations\HeadlineNewsService;
use Illuminate\Http\Request;

class PostController extends ViewController
{
    private static $INIT_PAGINATED_PAGE = 'pages.paginatedPages.initial.initialPaginatedPost';
    private static $ACCOUNT_PAGINATED_PAGE = 'pages.paginatedPages.account.accountPaginatedPost';
    private static $PAGINATED_KEY = 'posts';

    private static $CONTENT_KEY = 'post';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedPost';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedPost';

    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = ['movie', 'comments', 'user'];

    private $getDecorator;
    private $newsService;
    private $genreService;
    private $movieService;
    private $postDecorator;

    public function __construct(PostPaginateDecorator $paginatedDecorator, PostGetDecorator $getDecorator,
                                HeadlineNewsService $newsService,  GetMainGenreDecorator $genreService,
                                GetCreatePostMovieDecorator $movieService, GetThreeLastPostDecorator $postDecorator)
    {
        parent::__construct($paginatedDecorator);
        $this->getDecorator = $getDecorator;
        $this->newsService = $newsService;
        $this->genreService = $genreService;
        $this->movieService = $movieService;
        $this->postDecorator = $postDecorator;
    }

    public function postPaginate(Request $request)
    {
        $attributes['query'] = 'movie';
        $extends['articles'] = $this->newsService->getNews($attributes);
        $extends['genres'] = $this->genreService->getAll();
        $extends['posts'] = $this->postDecorator->getAll();
        return parent::paginate($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION,
            $extends);
    }

    public function adminPostPaginate(Request $request)
    {
        self::$ACCOUNT_PAGINATED_PAGE = 'pages.posts';
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION);
    }

    public function adminPostPost(Request $request)
    {
        $extends['movies'] = $this->movieService->getAll();
        return parent::getAdminView($request, 'pages.createPages.createPost', $extends);
    }

    public function adminUpdatePost(Request $request)
    {
        $id = $request->get('id');
        $extends['movies'] = $this->movieService->getAll();
        $extends['post'] = $this->getService()->getModel([], $id);
        return parent::getAdminView($request, 'pages.createPages.updatePost', $extends);
    }

    public function getPage(Request $request, int $id)
    {
        $this->setService($this->getDecorator);
        return parent::getContentPage($request, self::$PAGINATED_RELATION, $id);
    }

    public function createInitPaginatePage(): string
    {
        return self::$INIT_PAGINATED_PAGE;
    }

    public function createAccountPaginatePage(): string
    {
        return self::$ACCOUNT_PAGINATED_PAGE;
    }

    public function createPaginateKey(): string
    {
        return self::$PAGINATED_KEY;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}