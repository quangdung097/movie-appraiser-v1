<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 1:07 AM
 */

namespace App\Http\Controllers\View;


use App\Decorators\GenreDecorator\GetMainGenreDecorator;
use App\Decorators\MovieDecorator\GetMoviePostDecorator;
use App\Decorators\NewsDecorator\General\GetLimitNewsDecorator;
use App\Decorators\PostDecorator\GetPost\GetThreeLastPostDecorator;
use App\Models\Actor;
use App\Models\Country;
use App\Models\Director;
use App\Models\Genre;
use App\Models\Movie;
use App\Services\MovieService;
use App\Services\News\Implementations\HeadlineNewsService;
use Illuminate\Http\Request;

class MovieController extends ViewController
{
    private static $INIT_PAGINATED_PAGE = 'pages.paginatedPages.initial.initialPaginatedMovie';
    private static $ACCOUNT_PAGINATED_PAGE = 'pages.paginatedPages.account.accountPaginatedMovie';
    private static $PAGINATED_KEY = 'movies';

    private static $CONTENT_KEY = 'movie';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedMovie';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedMovie';

    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = ['directors', 'actors', 'genres', 'country'];

    private $newsService;
    private $generalNewsService;
    private $getDecorator;
    private $genreService;
    private $postService;

    public function __construct(MovieService $service, HeadlineNewsService $newsService, GetMainGenreDecorator $genreService,
                                GetLimitNewsDecorator $generalNewsService, GetMoviePostDecorator $getDecorator,
                                GetThreeLastPostDecorator $postService)
    {
        parent::__construct($service);
        $this->newsService = $newsService;
        $this->getDecorator = $getDecorator;
        $this->generalNewsService = $generalNewsService;
        $this->genreService = $genreService;
        $this->postService = $postService;
    }

    public function createInitPaginatePage(): string
    {
        return self::$INIT_PAGINATED_PAGE;
    }

    public function createAccountPaginatePage(): string
    {
        return self::$ACCOUNT_PAGINATED_PAGE;
    }

    public function createPaginateKey(): string
    {
        return self::$PAGINATED_KEY;
    }

    public function moviePaginate(Request $request)
    {
        $attributes['query'] = 'movie';
        $extends['articles'] = [];
        $extends['genres'] = $this->genreService->getAll();
        $extends['posts'] = $this->postService->getAll();
        return parent::paginate($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION,
            $extends);
    }

    public function adminMoviePaginate(Request $request)
    {
        self::$ACCOUNT_PAGINATED_PAGE = 'pages.movies';
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION);
    }


    public function adminPostMovie(Request $request)
    {
        $extends['actors'] = Actor::query()->orderBy('name', 'desc')->get(['id', 'name']);
        $extends['countries'] = Country::query()->orderBy('country_name', 'asc')->get(['id', 'country_name']);
        $extends['directors'] = Director::query()->orderBy('name', 'desc')->get(['id', 'name']);
        $extends['genres'] = Genre::query()->orderBy('genre_name', 'asc')->get(['id', 'genre_name']);
        return parent::getAdminView($request, 'pages.createPages.createMovie.createMovie', $extends);
    }

    public function adminUpdateMovie(Request $request, int $id = null)
    {
        $id = ($id == null) ? $request->get('id') : $id;
        $extends['actors'] = Actor::query()->orderBy('name', 'desc')->get(['id', 'name']);
        $extends['countries'] = Country::query()->orderBy('country_name', 'asc')->get(['id', 'country_name']);
        $extends['directors'] = Director::query()->orderBy('name', 'desc')->get(['id', 'name']);
        $extends['genres'] = Genre::query()->orderBy('genre_name', 'asc')->get(['id', 'genre_name']);
        $extends['genres'] = Genre::query()->orderBy('genre_name', 'asc')->get(['id', 'genre_name']);
        $extends['movie'] = Movie::query()->find($id);
        return parent::getAdminView($request, 'pages.createPages.createMovie.updateMovie', $extends);
    }

    public function getPage(Request $request, int $id)
    {
        $service = $this->getService();
        $movie = $service->getModel([], $id);
        if ($movie != null) {
            $attributes['query'] = $movie['title'];
            $extends['articles'] = $this->generalNewsService->getNews($attributes);
        } else {
            $extends['articles'] = [];
        }
        $this->setService($this->getDecorator);
        return parent::getContentPage($request, self::$PAGINATED_RELATION, $id, $extends);
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }

    public function getService(): MovieService
    {
        return parent::getService();
    }
}