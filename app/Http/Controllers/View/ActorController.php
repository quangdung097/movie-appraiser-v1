<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 11:38 PM
 */

namespace App\Http\Controllers\View;


use App\Decorators\ActorDecorator\GetActorMovieDecorator;
use App\Services\ActorService;
use App\Services\CountryService;
use Illuminate\Http\Request;

class ActorController extends ViewController
{
    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = ['country'];

    private static $CONTENT_KEY = 'actor';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedActor';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedActor';

    private $countryService;
    private $getService;

    public function __construct(ActorService $service, CountryService $countryService,
                                GetActorMovieDecorator $getService)
    {
        parent::__construct($service);
        $this->countryService = $countryService;
        $this->getService = $getService;
    }

    public function adminActorPaginate(Request $request)
    {
        $extends['countries'] = $this->countryService->getAll();
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION,
            $extends);
    }

    public function adminPostActor(Request $request)
    {
        $extends['countries'] = $this->countryService->getAll();
        return parent::getAdminView($request, 'pages.createPages.createActor.createActor', $extends);
    }

    public function getPage(Request $request, int $id)
    {
        $relations = ['movies'];
        $this->setService($this->getService);
        $a =  parent::getContentPage($request, $relations, $id);
        return $a;
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return 'pages.actors';
    }

    public function createPaginateKey(): string
    {
        return 'actors';
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}