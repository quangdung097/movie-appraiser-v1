<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 11:40 PM
 */

namespace App\Http\Controllers\View;


use App\Decorators\CountryDecorator\GetCountryMovieDecorator;
use App\Services\CountryService;
use Illuminate\Http\Request;

class CountryController extends ViewController
{
    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = [];

    private static $CONTENT_KEY = 'country';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedCountry';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedCountry';

    private $getDecorator;
    public function __construct(CountryService $service, GetCountryMovieDecorator $getDecorator)
    {
        parent::__construct($service);
        $this->getDecorator = $getDecorator;
    }

    public function adminCountryPaginate(Request $request)
    {
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION);
    }

    public function getPage(Request $request, int $id)
    {
        $relations = ['movies'];
        $this->setService($this->getDecorator);
        return parent::getContentPage($request, $relations, $id);
    }

    public function adminPostCountry(Request $request)
    {
        return parent::getAdminView($request, 'pages.createPages.createCountry.createCountry');
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return 'pages.countries';
    }

    public function createPaginateKey(): string
    {
        return 'countries';
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}