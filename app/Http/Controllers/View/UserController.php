<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 2:48 AM
 */

namespace App\Http\Controllers\View;


use App\Models\Comment;
use App\Models\UserRate;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends ViewController
{
    private static $CONTENT_KEY = 'contentUser';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.accountPages.profile';
    private static $INIT_CONTENT_PAGE = '/';

    public function __construct(UserService $service)
    {
        parent::__construct($service);
    }

    public function getPage(Request $request)
    {
        $sessionId = $request->session()->get('user_id');
        $id = ($sessionId == null) ? 0 : $sessionId;
        $extends['recentVotes'] = UserRate::query()->where('user_id', '=', $sessionId)
            ->orderBy('created_at', 'desc')->limit('3')->get();
        $extends['recentComments'] = Comment::query()->where('user_id', '=', $sessionId)
            ->orderBy('created_at', 'desc')->limit('3')->get();
        return parent::getContentPage($request, [], $id, $extends);
    }

    public function getUserPaginatedView(Request $request)
    {
        $adminView = 'pages.users';
        $attributes['limit'] = 5;
        $extends['paginatedAccounts'] = $this->getService()->paginate($attributes);
        return parent::getAdminView($request, $adminView, $extends);
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return '/';
    }

    public function createPaginateKey(): string
    {
        return 'users';
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}