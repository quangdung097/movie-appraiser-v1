<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/26/2019
 * Time: 12:56 AM
 */

namespace App\Http\Controllers\View;


use App\Decorators\UserDecorator\Login\AdminDecorator;
use App\Http\Controllers\Controller;
use App\Services\Eloquent\EloquentUserService;
use App\Services\Service;
use App\Services\UserService;
use Illuminate\Http\Request;

abstract class ViewController extends Controller
{
    private $service;
    private $userService;
    private $adminService;

    public function __construct(Service $service)
    {
        $this->userService = new EloquentUserService();
        $this->service = $service;
        $this->adminService = new AdminDecorator($this->userService);
    }

    public function paginate(Request $request, string $sort, string $limit,
                             string $order, array $relations, array $extends = [])
    {
        $attributes['sort'] = $sort;
        $attributes['order'] = $order;
        $attributes['limit'] = $limit;
        $attributes['relations'] = $relations;

        $model = $this->service->paginate($attributes);
        $userId = $request->session()->get('user_id');
        if ($userId == null) {
            return view($this->createInitPaginatePage(), [
                $this->createPaginateKey() => $model,
                'extends' => $extends
            ]);
        } else {
            $user = $this->userService->getModel([], $userId);
            return view($this->createAccountPaginatePage(), [
                $this->createPaginateKey() => $model,
                'user' => $user,
                'extends' => $extends
            ]);
        }
    }

    public function getAdminPaginatedView(Request $request, string $sort, string $limit,
                                          string $order, array $relations, array $extends = [])
    {
        $attributes['sort'] = $sort;
        $attributes['order'] = $order;
        $attributes['limit'] = $limit;
        $attributes['relations'] = $relations;

        $model = $this->service->paginate($attributes);
        $userId = $request->session()->get('user_id');
        if ($userId != null) {
            $user = $this->adminService->getModel(['role'], $userId);
            if (strcasecmp($user['role']['role_type'], 'admin') == 0) {
                return view($this->createAccountPaginatePage(), [
                    'user' => $user,
                    'extends' => $extends,
                    $this->createPaginateKey() => $model,
                ]);
            }
        }
        return redirect('/');
    }

    public function getContentPage(Request $request, array $relations, int $id, array $extends = [])
    {
        $model = $this->service->getModel($relations, $id);
        if ($model == null) {
            return redirect('/');
        }
        $userId = $request->session()->get('user_id');
        if ($userId == null) {
            return view($this->createInitContentPage(), [
                $this->createContentKey() => $model,
                'extends' => $extends
            ]);
        } else {
            $user = $this->userService->getModel([], $userId);
            return view($this->createAccountContentPage(), [
                $this->createContentKey() => $model,
                'user' => $user,
                'extends' => $extends
            ]);
        }
    }

    public function getAdminView(Request $request, string $viewURL, array $extends = [])
    {
        $userId = $request->session()->get('user_id');
        if ($userId != null) {
            $user = $this->adminService->getModel(['role'], $userId);
            if (strcasecmp($user['role']['role_type'], 'admin') == 0) {
                return view($viewURL, [
                    'user' => $user,
                    'extends' => $extends
                ]);
            }
        }
        return redirect('/');
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return '/';
    }

    public function createPaginateKey(): string
    {
        return '';
    }

    abstract public function createAccountContentPage(): string;

    abstract public function createInitContentPage(): string;

    abstract public function createContentKey(): string;

    public function setService(Service $service): void
    {
        $this->service = $service;
    }

    public function getService()
    {
        return $this->service;
    }

    public function getUserService(): UserService
    {
        return $this->userService;
    }
}