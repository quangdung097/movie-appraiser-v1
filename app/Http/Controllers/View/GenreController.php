<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 11:38 PM
 */

namespace App\Http\Controllers\View;


use App\Decorators\GenreDecorator\GetGenreMovieDecorator;
use App\Services\GenreService;
use Illuminate\Http\Request;

class GenreController extends ViewController
{
    private static $PAGINATED_SORT = 'created_at';
    private static $PAGINATED_LIMIT = 4;
    private static $PAGINATED_ORDER = 'desc';
    private static $PAGINATED_RELATION = [];

    private static $CONTENT_KEY = 'genre';
    private static $ACCOUNT_CONTENT_PAGE = 'pages.contentPages.account.accountDetailedGenre';
    private static $INIT_CONTENT_PAGE = 'pages.contentPages.initial.initialDetailedGenre';

    private $getDecorator;

    public function __construct(GenreService $service, GetGenreMovieDecorator $getDecorator)
    {
        parent::__construct($service);
        $this->getDecorator = $getDecorator;
    }

    public function adminGenrePaginate(Request $request)
    {
        return parent::getAdminPaginatedView($request,
            self::$PAGINATED_SORT,
            self::$PAGINATED_LIMIT,
            self::$PAGINATED_ORDER,
            self::$PAGINATED_RELATION);
    }

    public function getPage(Request $request, int $id)
    {
        $relations = ['movies'];
        $this->setService($this->getDecorator);
        return parent::getContentPage($request, $relations, $id);
    }

    public function adminPostGenre(Request $request)
    {
        return parent::getAdminView($request, 'pages.createPages.createGenre.createGenre');
    }

    public function createInitPaginatePage(): string
    {
        return '/';
    }

    public function createAccountPaginatePage(): string
    {
        return 'pages.genres';
    }

    public function createPaginateKey(): string
    {
        return 'genres';
    }

    public function createAccountContentPage(): string
    {
        return self::$ACCOUNT_CONTENT_PAGE;
    }

    public function createInitContentPage(): string
    {
        return self::$INIT_CONTENT_PAGE;
    }

    public function createContentKey(): string
    {
        return self::$CONTENT_KEY;
    }
}