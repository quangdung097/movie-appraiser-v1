<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:49 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Requests\API\Movie\MovieDeleteRequest;
use App\Http\Controllers\Requests\API\Movie\MovieGetRequest;
use App\Http\Controllers\Requests\API\Movie\MoviePatchRequest;
use App\Http\Controllers\Requests\API\Movie\MoviePostRequest;
use App\Models\Movie;
use App\Services\MovieService;

class MovieController extends APIController
{
    public function __construct(MovieService $service)
    {
        parent::__construct($service);
    }

    public function get(MovieGetRequest $request, int $id = null)
    {
        return parent::_get($request, $id);
    }

    public function patch(MoviePatchRequest $request, int $id = null)
    {
        return parent::_patch($request, $id);
    }

    public function post(MoviePostRequest $request)
    {
        return parent::_post($request);
    }

    public function delete(MovieDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }

    public function all(MovieGetRequest $request)
    {
        return parent::_all($request);
    }

    public function search(MovieGetRequest $request)
    {
        return $this->getService()->searchName($request->all());
    }

    public function getService(): MovieService
    {
        return parent::getService();
    }

    public function paginate() {
        $attributes['sort'] = 'created_at';
        $attributes['order'] = 'desc';
        $attributes['limit'] = 4;
        $attributes['relations'] = ['directors', 'actors'];

        $movies = $this->getService()->paginate($attributes);
        return view('pages.paginatedHome', ['movies' => $movies]);
    }
}