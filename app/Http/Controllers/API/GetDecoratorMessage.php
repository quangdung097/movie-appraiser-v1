<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/20/2019
 * Time: 4:09 PM
 */

namespace App\Http\Controllers\API;


use App\Services\Message;
use App\Services\Service;

trait GetDecoratorMessage
{
    public function getDecoratorMessage(Service $service)
    {
        /**
         * @var Message $service
         */
        return $service->getMessage();
    }
}