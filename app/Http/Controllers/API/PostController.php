<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:54 PM
 */

namespace App\Http\Controllers\API;


use App\Decorators\PostDecorator\CreatePostDecorator;
use App\Decorators\PostDecorator\GetPost\PostGetDecorator;
use App\Decorators\PostDecorator\UpdatePostDecorator;
use App\Http\Controllers\Requests\API\Post\PostDeleteRequest;
use App\Http\Controllers\Requests\API\Post\PostGetRequest;
use App\Http\Controllers\Requests\API\Post\PostPatchRequest;
use App\Http\Controllers\Requests\API\Post\PostPostRequest;
use App\Services\PostService;

class PostController extends APIController
{
    public function __construct(PostService $service)
    {
        parent::__construct($service);
    }

    public function get(PostGetRequest $request, int $id = null)
    {
        $enhancedGetService = new PostGetDecorator($this->getService());
        return $enhancedGetService->getModel($request->getRelations(), $id);
    }

    public function post(PostPostRequest $request)
    {
        $this->setService(new CreatePostDecorator($this->getService()));
        return parent::_post($request);
    }

    public function patch(PostPatchRequest $request, int $id = null)
    {
        $this->setService(new UpdatePostDecorator($this->getService()));
        return parent::_patch($request, $id);
    }

    public function delete(PostDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }

    public function all(PostGetRequest $request)
    {
        $enhancedGetService = new PostGetDecorator($this->getService());
        return $enhancedGetService->getAll($request->all());
    }

    public function getService(): PostService
    {
        return parent::getService();
    }
}