<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:06 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Requests\API\Actor\ActorDeleteRequest;
use App\Http\Controllers\Requests\API\Actor\ActorGetRequest;
use App\Http\Controllers\Requests\API\Actor\ActorPatchRequest;
use App\Http\Controllers\Requests\API\Actor\ActorPostRequest;
use App\Services\ActorService;

class ActorController extends APIController
{
    public function __construct(ActorService $service)
    {
        parent::__construct($service);
    }

    public function get(ActorGetRequest $request, int $id = null)
    {
        return parent::_get($request, $id);
    }

    public function post(ActorPostRequest $request)
    {
        return parent::_post($request);
    }

    public function patch(ActorPatchRequest $request, int $id = null)
    {
        return parent::_patch($request, $id);
    }

    public function delete(ActorDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }

    public function search(ActorGetRequest $request)
    {
        return $this->getService()->searchName($request->all());
    }

    public function getService(): ActorService
    {
        return parent::getService();
    }
}