<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:03 PM
 */

namespace App\Http\Controllers\API;


use App\Decorators\UserDecorator\Login\ChangePasswordDecorator;
use App\Decorators\UserDecorator\Login\LoginDecorator;
use App\Decorators\UserDecorator\Register\RegisterDecorator;
use App\Decorators\UserDecorator\UpdateUserDecorator;
use App\Http\Controllers\Requests\API\User\UserChangePasswordRequest;
use App\Http\Controllers\Requests\API\User\UserDeleteRequest;
use App\Http\Controllers\Requests\API\User\UserGetRequest;
use App\Http\Controllers\Requests\API\User\UserLoginRequest;
use App\Http\Controllers\Requests\API\User\UserLogoutRequest;
use App\Http\Controllers\Requests\API\User\UserPatchRequest;
use App\Http\Controllers\Requests\API\User\UserPostRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends APIController
{
    use GetDecoratorMessage;

    public function __construct(UserService $service)
    {
        parent::__construct($service);
    }

    public function get(UserGetRequest $request, int $id = null)
    {
        return parent::_get($request, $id);
    }

    public function register(UserPostRequest $request)
    {
        $userService = $this->getService();
        $registerDecorator = new RegisterDecorator($userService);
        $newUser =  $registerDecorator->createNewModel($request->all());
        if ($newUser == null) {
            return response([
                'Message' => 'Register failed'
            ], 200);
        }
        if ($request->session()->get('user_id') == null) {
            $request->session()->put('user_id', $newUser['id']);
        }

        return response([
            'Message' => 'Register successfully',
            'User' => $newUser
        ], 200);
    }

    public function login(UserLoginRequest $request)
    {
        $userService = $this->getService();
        $registerDecorator = new LoginDecorator($userService);
        $user = $registerDecorator->getModel($request->all(), 0);
        if ($user == null) {
            return response([
               'Message' => 'Invalid username or password'
            ],200);
        }
        $request->session()->put('user_id', $user['id']);
        return response([
            'Message' => 'Login successfully',
            'User' => $user
            ], 200);
    }

    public function logout(UserLogoutRequest $request)
    {
        $userId = $request->get('user_id');
        $sessionUserId = $request->session()->get('user_id');

        if ($sessionUserId == null || strcasecmp($sessionUserId, $userId) != 0) {
            return response(['Invalid request'], 403);
        }

        $request->session()->flush();
        return response([
            'Message' => 'Logout successfully',
        ], 200);
    }

    public function getSessionData(Request $request) {
        $userId = $request->session()->get('user_id');
        if ($userId != null) {
            return response([
                'State' => true,
                'user_id' => $userId
            ], 200);
        }
        return response([
            'State' => false,
        ], 200);
    }

    public function changePassword(UserChangePasswordRequest $request)
    {
        $userService = $this->getService();
        $changeDecorator = new ChangePasswordDecorator($userService);
        $state =  $changeDecorator->updateModel($request->all(), 0);
        if ($state == true) {
            return response(
                ['message' => 'Đổi mật khẩu thành công'],
                200);
        }
        return response(
            ['message' => $this->getDecoratorMessage($changeDecorator)],
            403);
    }

    public function patch(UserPatchRequest $request, int $id = null)
    {
        $userService = $this->getService();
        $changeDecorator = new UpdateUserDecorator($userService);
        $state =  $changeDecorator->updateModel($request->all(), $id);
        if ($state == true) {
            return response(
                ['message' => 'Thay đổi thành công'],
                200);
        }
        return response(
            ['message' => $this->getDecoratorMessage($changeDecorator)],
            403);
    }

    public function getService(): UserService
    {
        return parent::getService();
    }

    public function delete(UserDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }
}