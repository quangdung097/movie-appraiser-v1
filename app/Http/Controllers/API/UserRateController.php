<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:21 AM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Requests\API\UserRate\UserRateDeleteRequest;
use App\Http\Controllers\Requests\API\UserRate\UserRateGetRequest;
use App\Http\Controllers\Requests\API\UserRate\UserRatePatchRequest;
use App\Http\Controllers\Requests\API\UserRate\UserRatePostRequest;
use App\Services\UserRateService;

class UserRateController extends APIController
{
    public function __construct(UserRateService $service)
    {
        parent::__construct($service);
    }

    public function post(UserRatePostRequest $request)
    {
        return parent::_post($request);
    }

    public function patch(UserRatePatchRequest $request, int $id = null)
    {
        return parent::_patch($request, $id);
    }

    public function delete(UserRateDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }

    public function all(UserRateGetRequest $request)
    {
        return parent::_all($request);
    }
}