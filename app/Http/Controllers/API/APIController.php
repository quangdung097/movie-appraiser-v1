<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/5/2019
 * Time: 1:39 AM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Requests\DeleteRequest;
use App\Http\Controllers\Requests\GetRequest;
use App\Http\Controllers\Requests\PatchRequest;
use App\Http\Controllers\Requests\PostRequest;
use App\Services\Message;
use App\Services\Service;

class APIController extends Controller
{
    private $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function _get(GetRequest $request, int $id = null)
    {
        return $this->service->getModel($request->getRelations(), $id);
    }

    public function _post(PostRequest $request)
    {
        $model = $this->service->createNewModel($request->all());
        if ($model != null) {
            return $model;
        }
        else {
            return response(['Message' => $this->getServiceMessage()], 500);
        }
    }

    public function _patch(PatchRequest $request, int $id = null)
    {
        $checker = $this->service->updateModel($request->all(), $id);
        if ($checker == true) {
            return ['Success'];
        }
        return ['Failed'];
    }

    public function _delete(DeleteRequest $request, int $id = null)
    {
        $checker = $this->service->deleteModel($request->all(), $id);
        if ($checker == true) {
            return ['Success'];
        }
        return ['Failed'];
    }

    public function _all(GetRequest $request)
    {
        return $this->service->getAll($request->all());
    }

    public function getService()
    {
        return $this->service;
    }

    public function getServiceMessage():string
    {
        /**
         * @var Message $message
         */
        $message = $this->service;
        return $message->getMessage();
    }

    public function setService(Service $service)
    {
        $this->service = $service;
    }

}