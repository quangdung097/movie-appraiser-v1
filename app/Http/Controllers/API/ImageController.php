<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:48 PM
 */

namespace App\Http\Controllers\API;


use App\Handlers\HandlerAttributes\Image\ImageUploadAttributes;
use App\Handlers\ImageHandler\File\UploadImageHandler;
use App\Http\Controllers\Requests\API\Image\ImageDeleteRequest;
use App\Http\Controllers\Requests\API\Image\ImagePatchRequest;
use App\Http\Controllers\Requests\API\Image\ImagePostRequest;
use App\Services\ImageService;

class ImageController extends APIController
{
    public function __construct(ImageService $service)
    {
        parent::__construct($service);
    }

    public function post(ImagePostRequest $request)
    {
        $attributes = $request->all();
        $handler = new UploadImageHandler();
        $imageUploadAttributes = new ImageUploadAttributes($attributes);
        $handler->handle($imageUploadAttributes);

        $a =$imageUploadAttributes->getAttributes();
        return $this->getService()->createNewModel($imageUploadAttributes->getAttributes());
//        return parent::_post($request);
    }

    public function patch(ImagePatchRequest $request, int $id = null)
    {
        return parent::_patch($request, $id);
    }

    public function delete(ImageDeleteRequest $request, int $id = null)
    {
        return parent::_delete($request, $id);
    }
}