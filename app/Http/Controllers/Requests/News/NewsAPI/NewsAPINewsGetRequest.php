<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:31 AM
 */

namespace App\Http\Controllers\Requests\News\NewsAPI;


use App\Http\Controllers\Requests\News\NewsGetRequest;

class NewsAPINewsGetRequest extends NewsGetRequest
{

    function filterRules(): array
    {
        return [
            'q' => 'required',
        ];
    }
}