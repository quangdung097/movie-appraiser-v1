<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:22 AM
 */

namespace App\Http\Controllers\Requests\News;


use App\Http\Controllers\Requests\RestfulRequest;
use Illuminate\Support\Facades\Input;

abstract class NewsGetRequest extends RestfulRequest
{
    abstract function filterRules(): array;

    public function rules(): array
    {
        return array_merge([
            'q' => 'string',
        ], $this->filterRules());
    }

    public function getQuery(): ?string
    {
        return Input::get('q');
    }

    public function getFilterRules(): array
    {
        return $this->filterRules();
    }
}