<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:17 PM
 */

namespace App\Http\Controllers\Requests\API\Post;


use App\Http\Controllers\Requests\PatchRequest;

class PostPatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string',
            'user_id' => 'int|exists:users,id',
            'movie_id' => 'int|exists:movies,id',
            'content' => 'string',
            'image' => 'image',
        ];
    }
}