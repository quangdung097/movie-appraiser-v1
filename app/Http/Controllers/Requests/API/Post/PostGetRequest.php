<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:18 PM
 */

namespace App\Http\Controllers\Requests\API\Post;


use App\Http\Controllers\Requests\GetRequest;

class PostGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['movie', 'comments', 'user'];
    }
}