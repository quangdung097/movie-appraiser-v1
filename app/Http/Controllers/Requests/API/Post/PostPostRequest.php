<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:13 PM
 */

namespace App\Http\Controllers\Requests\API\Post;


use App\Http\Controllers\Requests\PostRequest;

class PostPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string|required',
            'user_id' => 'int|required|exists:users,id',
            'movie_id' => 'int|required|exists:movies,id',
            'content' => 'string|required',
            'image' => 'image'
        ];
    }
}