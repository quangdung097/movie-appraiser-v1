<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:20 PM
 */

namespace App\Http\Controllers\Requests\API\Role;


use App\Http\Controllers\Requests\PostRequest;

class RolePostRequest extends PostRequest
{
    public function rules():array
    {
        return [
            'role_type' => 'string|required|unique:roles,id'
        ];
    }
}