<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:21 PM
 */

namespace App\Http\Controllers\Requests\API\Role;


use App\Http\Controllers\Requests\PatchRequest;

class RolePatchRequest extends PatchRequest
{
    public function rules():array
    {
        return [
            'role_type' => 'string|unique:roles,id'
        ];
    }
}