<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:30 PM
 */

namespace App\Http\Controllers\Requests\API\Country;


use App\Http\Controllers\Requests\PatchRequest;

class CountryPatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            'country_name' => 'string|unique:countries,country_name'
        ];
    }
}