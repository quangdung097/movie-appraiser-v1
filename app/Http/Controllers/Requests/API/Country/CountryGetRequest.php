<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:27 PM
 */

namespace App\Http\Controllers\Requests\API\Country;


use App\Http\Controllers\Requests\GetRequest;

class CountryGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['movies', 'actors', 'directors'];
    }
}