<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:29 PM
 */

namespace App\Http\Controllers\Requests\API\Country;


use App\Http\Controllers\Requests\PostRequest;

class CountryPostRequest extends PostRequest
{
    public function rules():array
    {
        return [
            'country_name' => 'string|required|unique:countries,country_name'
        ];
    }
}