<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:02 PM
 */

namespace App\Http\Controllers\Requests\API\Genre;


use App\Http\Controllers\Requests\PostRequest;

class GenrePostRequest extends PostRequest
{
    public function rules()
    {
        return [
            'genre_name' => 'string|required|unique:genres,genre_name'
        ];
    }
}