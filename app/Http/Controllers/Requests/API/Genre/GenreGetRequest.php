<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:01 PM
 */

namespace App\Http\Controllers\Requests\API\Genre;


use App\Http\Controllers\Requests\GetRequest;

class GenreGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['movies'];
    }
}