<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:03 PM
 */

namespace App\Http\Controllers\Requests\API\Genre;


use App\Http\Controllers\Requests\PatchRequest;

class GenrePatchRequest extends PatchRequest
{
    public function rules()
    {
        return [
            'genre_name' => 'string|unique:genres,genre_name'
        ];
    }
}