<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 1:48 AM
 */

namespace App\Http\Controllers\Requests\API\User;

use App\Http\Controllers\Requests\PatchRequest;

class UserChangePasswordRequest extends PatchRequest
{
    public function rules():array
    {
        return [
            'oldPassword' => 'string|required',
            'newPassword' => 'string|required|min:6',
            'cPassword' => 'required|same:newPassword'
        ];
    }
}