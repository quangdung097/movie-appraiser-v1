<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:21 PM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\PostRequest;

class UserPatchRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'avatar' => 'image',
            'name' => 'string',
        ];
    }
}