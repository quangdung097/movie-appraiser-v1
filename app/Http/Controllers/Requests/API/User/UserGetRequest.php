<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 10:57 PM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\GetRequest;

class UserGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['posts', 'comments', 'role', 'image'];
    }
}