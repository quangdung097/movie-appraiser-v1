<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/15/2019
 * Time: 1:30 AM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\PostRequest;

class UserLogoutRequest extends PostRequest
{
    public function rules():array
    {
        return [
            'user_id' => 'int|required|exists:users,id'
        ];

    }
}