<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 1:48 AM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\PostRequest;

class UserLoginRequest extends PostRequest
{
    public function rules():array
    {
        return [
            'username' => 'email|required|exists:users,username',
            'password' => 'string|required|min:6'
        ];
    }
}