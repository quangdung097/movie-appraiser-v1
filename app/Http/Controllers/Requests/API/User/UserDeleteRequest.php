<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:22 PM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\DeleteRequest;

class UserDeleteRequest extends DeleteRequest
{

}