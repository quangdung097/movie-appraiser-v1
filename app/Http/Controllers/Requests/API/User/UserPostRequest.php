<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 11:00 PM
 */

namespace App\Http\Controllers\Requests\API\User;


use App\Http\Controllers\Requests\PostRequest;

class UserPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string',
            'username' => 'email|required|unique:users,username',
            'password' => 'string|required|min:6',
            'c_password' => 'string|required|same:password',
            'avatar' => 'image',
        ];
    }
}