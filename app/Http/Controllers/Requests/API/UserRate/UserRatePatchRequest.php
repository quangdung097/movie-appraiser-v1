<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:33 AM
 */

namespace App\Http\Controllers\Requests\API\UserRate;


use App\Http\Controllers\Requests\PatchRequest;

class UserRatePatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            'rate' => 'int|min:1|max:5',
        ];
    }
}