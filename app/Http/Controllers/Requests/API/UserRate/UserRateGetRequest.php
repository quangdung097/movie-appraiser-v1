<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:36 AM
 */

namespace App\Http\Controllers\Requests\API\UserRate;


use App\Http\Controllers\Requests\GetRequest;

class UserRateGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return [];
    }

    function relations(): array
    {
        return ['users', 'movies'];
    }
}