<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:32 AM
 */

namespace App\Http\Controllers\Requests\API\UserRate;


use App\Http\Controllers\Requests\PostRequest;

class UserRatePostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'user_id' => 'int|required|exists:users,id',
            'movie_id' => 'int|required|exists:movies,id',
            'rate' => 'int|required|min:1|max:5',
        ];
    }
}