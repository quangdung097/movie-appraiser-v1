<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:12 PM
 */

namespace App\Http\Controllers\Requests\API\Actor;


use App\Http\Controllers\Requests\PostRequest;

class ActorPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string|required|unique:actors,name',
            'DOB' => 'date_format:Y-m-d|required',
            'country_id' => 'int|required|exists:countries,id'
        ];
    }
}