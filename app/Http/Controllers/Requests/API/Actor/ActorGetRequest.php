<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:11 PM
 */

namespace App\Http\Controllers\Requests\API\Actor;


use App\Http\Controllers\Requests\GetRequest;

class ActorGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['country', 'movies'];
    }
}