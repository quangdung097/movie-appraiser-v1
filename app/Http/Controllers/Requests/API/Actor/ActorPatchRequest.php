<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:18 PM
 */

namespace App\Http\Controllers\Requests\API\Actor;


use App\Http\Controllers\Requests\PatchRequest;

class ActorPatchRequest extends PatchRequest
{
    public function rules() :array
    {
        return [
            'name' => 'string|unique:actors,name',
            'DOB' => 'date_format:Y-m-d',
            'country_id' => 'int|exists:countries,id'
        ];
    }
}