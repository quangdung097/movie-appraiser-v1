<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:22 PM
 */

namespace App\Http\Controllers\Requests\API\Comment;


use App\Http\Controllers\Requests\PostRequest;

class CommentPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'user_id' => 'int|required|exists:users,id',
            'post_id' => 'int|required|exists:posts,id',
            'content' => 'string|required'
        ];
    }
}