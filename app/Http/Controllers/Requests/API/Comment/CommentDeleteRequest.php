<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:26 PM
 */

namespace App\Http\Controllers\Requests\API\Comment;


use App\Http\Controllers\Requests\DeleteRequest;

class CommentDeleteRequest extends DeleteRequest
{

}