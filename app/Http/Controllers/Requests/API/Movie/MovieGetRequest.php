<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:08 PM
 */

namespace App\Http\Controllers\Requests\API\Movie;


use App\Http\Controllers\Requests\GetRequest;

class MovieGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['actors', 'directors', 'genres', 'country'];
    }
}