<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:12 PM
 */

namespace App\Http\Controllers\Requests\API\Movie;


use App\Http\Controllers\Requests\PatchRequest;

class MoviePatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string',
            'budget' => 'string',
            'country_id' => 'int|exists:countries,id',
            'poster_image_url' => 'string',
            'trailer_url' => 'string',
            'description' => 'string',
        ];
    }
}