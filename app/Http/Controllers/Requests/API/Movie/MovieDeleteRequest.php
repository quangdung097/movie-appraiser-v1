<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:12 PM
 */

namespace App\Http\Controllers\Requests\API\Movie;


use App\Http\Controllers\Requests\DeleteRequest;

class MovieDeleteRequest extends DeleteRequest
{

}