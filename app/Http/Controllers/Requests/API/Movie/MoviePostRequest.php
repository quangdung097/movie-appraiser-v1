<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:09 PM
 */

namespace App\Http\Controllers\Requests\API\Movie;


use App\Http\Controllers\Requests\PostRequest;

class MoviePostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string|required',
            'budget' => 'string|required',
            'country_id' => 'int|required|exists:countries,id',
            'poster_image_url' => 'string|required',
            'trailer_url' => 'string|required',
            'description' => 'string|required',
            'genres' => 'array|required',
            'genres.*' => 'int|distinct|exists:genres,id',
            'actors'=> 'array|required',
            'actors.*'=> 'int|distinct|exists:actors,id',
            'directors' => 'array|required',
            'directors.*' => 'int|distinct|exists:directors,id'
        ];
    }
}