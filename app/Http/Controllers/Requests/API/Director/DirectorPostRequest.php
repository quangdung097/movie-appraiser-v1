<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:48 PM
 */

namespace App\Http\Controllers\Requests\API\Director;


use App\Http\Controllers\Requests\PostRequest;

class DirectorPostRequest extends PostRequest
{
    public function rules(): array
    {
        return[
            'name' => 'string|required|unique:directors,name',
            'DOB' => 'date_format:Y-m-d|required',
            'country_id' => 'int|required|exists:countries,id'
        ];
    }
}