<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:49 PM
 */

namespace App\Http\Controllers\Requests\API\Director;


use App\Http\Controllers\Requests\PatchRequest;

class DirectorPatchRequest extends PatchRequest
{
    public function rules():array
    {
        return [
            'name' => 'string|unique:directors,name',
            'DOB' => 'date_format:Y-m-d',
            'country_id' => 'int|exists:countries,id'
        ];
    }
}