<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 10:47 PM
 */

namespace App\Http\Controllers\Requests\API\Director;


use App\Http\Controllers\Requests\GetRequest;

class DirectorGetRequest extends GetRequest
{

    function filterRules(): array
    {
        return [];
    }

    function sort(): array
    {
        return ['id'];
    }

    function relations(): array
    {
        return ['movies', 'country'];
    }
}