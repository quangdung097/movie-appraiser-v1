<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:04 PM
 */

namespace App\Http\Controllers\Requests\API\Image;


use App\Http\Controllers\Requests\PostRequest;

class ImagePostRequest extends PostRequest
{
    public function rules()
    {
        return [
            'image_name' => 'string',
            'file' => 'image|required'
        ];
    }
}