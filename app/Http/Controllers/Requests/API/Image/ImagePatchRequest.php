<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/11/2019
 * Time: 11:05 PM
 */

namespace App\Http\Controllers\Requests\API\Image;


use App\Http\Controllers\Requests\PatchRequest;

class ImagePatchRequest extends PatchRequest
{
    public function rules()
    {
        return [
            'image_name' => 'string',
            'image_url' => 'string',
        ];
    }
}