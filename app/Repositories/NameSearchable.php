<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/26/2019
 * Time: 8:59 PM
 */

namespace App\Repositories;


interface NameSearchable
{
    public function searchByName(array $attributes);
}