<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:14 PM
 */

namespace App\Repositories;


interface DirectorRepository extends Repository
{
    public function searchByName(array $attributes);
}