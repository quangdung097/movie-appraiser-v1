<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:15 PM
 */

namespace App\Repositories;


interface ActorRepository extends Repository
{
    public function searchActor(array $attributes);

    public function searchByName(array $attributes);
}