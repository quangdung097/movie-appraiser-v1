<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:14 PM
 */

namespace App\Repositories;


interface MovieRepository extends Repository
{
    public function getPaginatedMovies(array $attributes);

    public function getRandom(array $attributes);

    public function searchByName(array $attributes);
}