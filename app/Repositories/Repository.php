<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 6:21 PM
 */

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

interface Repository
{
    public function get(int $id, array $relation = []) :?Model;

    public function create(array $attributes): ?Model;

    public function update(array $attributes, Model $model): bool;

    public function delete(array $attributes, int $id): int;

    public function count(array $conditions = []) :int;

    public function attach(array $attributes, $relation,  Model $model);

    public function syncModel(array $attributes, $relation,  Model $model);

    public function getAll(array $attributes = []);

    public function getBetween($needle, array $duration, array $relations = []);

    public function getBy(array $conditions, array $relations = []);

    public function paginate(array $attributes);
}