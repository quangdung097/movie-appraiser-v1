<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:07 AM
 */

namespace App\Repositories\News\Implementations;


use App\Models\News\NewsAPI;
use App\Models\News\TopHeadLineNews;

class HeadlineNewsRepository extends BaseNewsRepository
{

    public function createNewsModel(): NewsAPI
    {
        return new TopHeadLineNews();
    }
}