<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 12:52 AM
 */

namespace App\Repositories\News\Implementations;


use App\Models\News\NewsAPI;
use App\Repositories\News\NewsRepository;

abstract class BaseNewsRepository implements NewsRepository
{
    private $newsModels;
    private $getURL;

    public function __construct()
    {
        $this->newsModels = $this->createNewsModel();
        $this->getURL = null;
    }

    public function formatQuery(string $query): void
    {
        $formattedQuery = str_replace(' ', '%20', $query);
        $this->getURL = $this->newsModels->formatURL($formattedQuery);
    }

    public function decode(string $json): array
    {
        return json_decode($json, true);
    }

    public function requestNews(): string
    {
        return file_get_contents($this->getURL);
    }

    abstract public function createNewsModel(): NewsAPI;
}