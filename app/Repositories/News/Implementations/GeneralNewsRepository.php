<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 1:08 AM
 */

namespace App\Repositories\News\Implementations;


use App\Models\News\EverythingNews;
use App\Models\News\NewsAPI;

class GeneralNewsRepository extends BaseNewsRepository
{

    public function createNewsModel(): NewsAPI
    {
        return new EverythingNews();
    }
}