<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/2019
 * Time: 12:47 AM
 */

namespace App\Repositories\News;


interface NewsRepository
{
    public function formatQuery(string $query): void;

    public function decode(string $json): array;

    public function requestNews(): string;
}