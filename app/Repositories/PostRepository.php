<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:17 PM
 */

namespace App\Repositories;


interface PostRepository extends Repository
{
    public function getPaginatedPost(array $condition, int $limit, string $order, string $sort, array $relations);
}