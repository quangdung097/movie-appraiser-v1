<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/26/2019
 * Time: 9:06 PM
 */

namespace App\Repositories\Eloquent\SearchRepository;



use App\Models\Movie;

class MovieTitleSearchRepository extends NameSearchRepository
{
    public function __construct()
    {
        parent::__construct(Movie::query());
    }

    private static $NAME = 'title';
    public function createNameColumn(): string
    {
        return self::$NAME;
    }
}