<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/26/2019
 * Time: 9:00 PM
 */

namespace App\Repositories\Eloquent\SearchRepository;


use App\Repositories\NameSearchable;
use Illuminate\Database\Eloquent\Builder;

abstract class NameSearchRepository implements NameSearchable
{
    private $queryBuilder;

    abstract public function createNameColumn() :string ;

    public function __construct(Builder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    public function searchByName(array $attributes)
    {
        if (isset($attributes['q'])) {
            return $this->newQuery()->where($this->createNameColumn(), 'like', '%'.$attributes['q'].'%')->limit(5)
                ->get();
        }
        return null;
    }

    public function newQuery()
    {
        return clone $this->queryBuilder;
    }
}