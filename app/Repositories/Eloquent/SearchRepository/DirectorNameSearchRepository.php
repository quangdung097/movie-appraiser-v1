<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/26/2019
 * Time: 9:08 PM
 */

namespace App\Repositories\Eloquent\SearchRepository;


use App\Models\Director;

class DirectorNameSearchRepository extends NameSearchRepository
{
    public function __construct()
    {
        parent::__construct(Director::query());
    }

    private static $NAME = 'name';
    public function createNameColumn(): string
    {
        return self::$NAME;
    }
}