<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:18 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Actor;
use App\Repositories\ActorRepository;
use App\Repositories\Eloquent\SearchRepository\ActorNameSearchRepository;
use App\Repositories\Eloquent\SearchRepository\NameSearchRepository;

class EloquentActorRepository extends EloquentRepository implements ActorRepository
{
    private $nameSearchRepository;
    public function __construct()
    {
        parent::__construct(Actor::query());
        $this->nameSearchRepository = $this->createSearcher();
    }

    public function searchActor(array $attributes)
    {
        return $this->newQuery()->where('name', 'like', '%'.$attributes['q'].'%')->first();
    }

    public function searchByName(array $attributes)
    {
        return $this->nameSearchRepository->searchByName($attributes);
    }

    private function createSearcher():NameSearchRepository
    {
        return new ActorNameSearchRepository();
    }
}