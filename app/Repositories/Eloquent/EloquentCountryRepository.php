<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:20 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Country;
use App\Repositories\CountryRepository;

class EloquentCountryRepository extends EloquentRepository implements CountryRepository
{
    public function __construct()
    {
        parent::__construct(Country::query());
    }
}