<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:26 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Role;
use App\Repositories\RoleRepository;

class EloquentRoleRepository extends EloquentRepository implements RoleRepository
{
    public function __construct()
    {
        parent::__construct(Role::query());
    }
}