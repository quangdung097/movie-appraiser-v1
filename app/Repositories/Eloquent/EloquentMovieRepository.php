<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:24 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Movie;
use App\Repositories\Eloquent\SearchRepository\MovieTitleSearchRepository;
use App\Repositories\Eloquent\SearchRepository\NameSearchRepository;
use App\Repositories\MovieRepository;

class EloquentMovieRepository extends EloquentRepository implements MovieRepository
{
    private $nameSearchRepository;

    public function __construct()
    {
        parent::__construct(Movie::query());
        $this->nameSearchRepository = $this->createSearcher();
    }

    public function getPaginatedMovies(array $attributes)
    {
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'movies.id';
        if (isset($attributes['condition'])) {
            $joinTable = $attributes['table'];
            if (isset($joinTable)) {
                return $this->newQuery()->with($relations)->select('movies.*')
                    ->join($joinTable, 'movies.id' , '=' , $joinTable.'.movie_id')
                    ->where([$attributes['condition']])->orderBy($sort, $order)->paginate($limit);
            }
            return $this->newQuery()->with($relations)
                ->where([$attributes['condition']])->orderBy($sort, $order)->paginate($limit);
        }
        return $this->newQuery()->with($relations)->orderBy($sort, $order)->paginate($limit);
    }

    public function getRandom(array $attributes)
    {
        $conditions = ($attributes['conditions'] == null) ? [] : $attributes['conditions'];
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        return $this->newQuery()->where($conditions)->inRandomOrder()->limit($limit)->get();
    }

    public function searchByName(array $attributes)
    {
        return $this->nameSearchRepository->searchByName($attributes);
    }


    private function createSearcher():NameSearchRepository
    {
        return new MovieTitleSearchRepository();
    }
}