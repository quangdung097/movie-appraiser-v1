<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:27 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\User;
use App\Repositories\UserRepository;

class EloquentUserRepository extends EloquentRepository implements UserRepository
{
    public function __construct()
    {
        parent::__construct(User::query());
    }
}