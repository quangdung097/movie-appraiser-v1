<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:25 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Post;
use App\Repositories\PostRepository;

class EloquentPostRepository extends EloquentRepository implements PostRepository
{
    public function __construct()
    {
        parent::__construct(Post::query());
    }

    public function getPaginatedPost(array $conditions, int $limit, string $order, string $sort, array $relations)
    {
        return $this->newQuery()->where([$conditions])->with($relations)->orderBy($sort, $order)->paginate($limit);
    }
}