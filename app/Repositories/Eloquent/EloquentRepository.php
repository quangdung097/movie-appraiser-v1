<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 6:24 PM
 */

namespace App\Repositories\Eloquent;


use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentRepository implements Repository
{
    private $queryBuilder;

    public function __construct(Builder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    public function get(int $id, array $relation = []): ?Model
    {
        return $this->newQuery()->with($relation)->find($id);
    }

    public function create(array $attributes): ?Model
    {
        return $this->newQuery()->create($attributes);
    }

    public function update(array $attributes, Model $model): bool
    {
        return $model->update($attributes);
    }

    public function delete(array $attributes, int $id): int
    {
        return $this->newQuery()->getModel()::destroy($id);
    }

    public function count(array $conditions = []): int
    {
        return $this->newQuery()->where($conditions)->count();
    }

    public function attach(array $attributes, $relation, Model $model)
    {
        $model->$relation()->attach($attributes[$relation]);
    }

    public function syncModel(array $attributes, $relation,  Model $model)
    {
        $model->$relation()->sync($attributes[$relation]);
    }

    public function getAll(array $attributes = [])
    {
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'id';
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $offset = isset($attributes['offset']) ? $attributes['offset'] : 0;

        return $this->newQuery()->with($relations)->limit($limit)->offset($offset)->orderBy($sort, $order)->get();
    }

    public function getBetween($needle, array $duration, array $relations = [])
    {
        return $this->newQuery()->whereBetween($needle, $duration)->with($relations)->get();
    }

    public function getBy(array $conditions, array $relations = [])
    {
        return $this->newQuery()->where($conditions)->with($relations)->first();
    }

    public function paginate(array $attributes)
    {
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'id';
        return $this->newQuery()->with($relations)->orderBy($sort, $order)->paginate($limit);
    }

    public function newQuery()
    {
        return clone $this->queryBuilder;
    }
}