<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:23 AM
 */

namespace App\Repositories\Eloquent;


use App\Models\UserRate;
use App\Repositories\UserRateRepository;

class EloquentUserRateRepository extends EloquentRepository implements UserRateRepository
{
    public function __construct()
    {
        parent::__construct(UserRate::query());
    }

    public function getAllWithCondition(array $attributes = [])
    {
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'id';
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $offset = isset($attributes['offset']) ? $attributes['offset'] : 0;
        $conditions = isset($attributes['conditions']) ? $attributes['conditions'] : [];

        return $this->newQuery()->where($conditions)->with($relations)
            ->limit($limit)->offset($offset)->orderBy($sort, $order)->get();
    }
}