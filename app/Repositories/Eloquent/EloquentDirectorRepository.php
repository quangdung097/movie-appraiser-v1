<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:21 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Director;
use App\Repositories\DirectorRepository;
use App\Repositories\Eloquent\SearchRepository\DirectorNameSearchRepository;
use App\Repositories\Eloquent\SearchRepository\NameSearchRepository;

class EloquentDirectorRepository extends EloquentRepository implements DirectorRepository
{
    private $nameSearchRepository;

    public function __construct()
    {
        parent::__construct(Director::query());

        $this->nameSearchRepository = $this->createSearcher();
    }

    public function searchByName(array $attributes)
    {
        return $this->nameSearchRepository->searchByName($attributes);
    }

    private function createSearcher():NameSearchRepository
    {
        return new DirectorNameSearchRepository();
    }
}