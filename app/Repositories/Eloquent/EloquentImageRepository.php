<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:23 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Image;
use App\Repositories\ImageRepository;

class EloquentImageRepository extends EloquentRepository implements ImageRepository
{
    public function __construct()
    {
        parent::__construct(Image::query());
    }
}