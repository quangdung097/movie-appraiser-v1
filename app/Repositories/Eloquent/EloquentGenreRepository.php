<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:22 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Genre;
use App\Repositories\GenreRepository;

class EloquentGenreRepository extends EloquentRepository implements GenreRepository
{
    public function __construct()
    {
        parent::__construct(Genre::query());
    }
}