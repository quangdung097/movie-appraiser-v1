<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/10/2019
 * Time: 7:19 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Comment;
use App\Repositories\CommentRepository;

class EloquentCommentRepository extends EloquentRepository implements CommentRepository
{
    public function __construct()
    {
        parent::__construct(Comment::query());
    }

    public function getPaginatedWithConditions(array $attributes)
    {
        $limit = isset($attributes['limit']) ? $attributes['limit'] : 20;
        $relations = isset($attributes['relations']) ? $attributes['relations'] : [];
        $order = isset($attributes['order']) ? $attributes['order'] : 'asc';
        $sort = isset($attributes['sort']) ? $attributes['sort'] : 'id';
        $conditions = isset($attributes['conditions']) ? $attributes['conditions'] : [];
        return $this->newQuery()->where($conditions)->with($relations)->orderBy($sort, $order)->paginate($limit);
    }
}