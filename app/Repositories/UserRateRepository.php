<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2019
 * Time: 1:23 AM
 */

namespace App\Repositories;


interface UserRateRepository extends Repository
{
    public function getAllWithCondition(array $attributes = []);
}