<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('images')->insert([
            'image_name' => 'No Image',
            'image_url' => '/storage/images/no-image.png'
        ]);

        DB::table('roles')->insert([
            'role_type' => 'user'
        ]);

        DB::table('roles')->insert([
            'role_type' => 'admin'
        ]);

        DB::table('users')->insert([
            'username' => 'admin@example.com',
            'password' => hash('md5', 123456),
            'role_id' => 2,
            'avatar_image_id' => 1,
            'name' => 'ADMINISTRATOR'
        ]);
    }
}
