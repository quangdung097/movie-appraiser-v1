$(document).ready(function () {
    let rateInput = $('.fa-star');
    let idNumber;
    const checkClass = ' checked';
    const initClass = 'fa fa-star';
    const starClass = $(this).attr('class');

    rateInput.hover(function () {
        idNumber = getStarId($(this));
        starRelated(idNumber, checkClass);
    }, function () {
        idNumber = getStarId($(this));
        starRelated(idNumber, '');
    });

    rateInput.on('click', function () {
        let point = getStarId($(this));
        rate(point);
    });

    function starRelated(idNumber, checkClass) {
        for (let i = 1; i <= idNumber; i++) {
            $('#star-0' + i).attr('class', initClass + checkClass);
        }
    }

    function getStarId(star) {
        let id = star.attr('id');
        return id.substring(6, id.length);
    }

    function isUpdate()
    {
        let isSet = getRateId();
        return (isSet !== null);
    }

    function rate(point) {
        if (!isUpdate()) {
            $.ajax({
                url: "http://movie.localhost/api/v1/rates/post",
                type: "post",
                dataType: "json",
                data: {
                    user_id : getUserId(),
                    movie_id : getMovieId(),
                    rate : point,
                },
                success: function () {
                    starRelated(point, checkClass);
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        } else {
            $.ajax({
                url: "http://movie.localhost/api/v1/rates/patch/" + getRateId(),
                type: "patch",
                dataType: "json",
                data: {
                    rate : point,
                },
                success: function () {
                    starRelated(5, initClass);
                    starRelated(point, checkClass);
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function getUserId() {
        let userId = $('li.user-dropdown').attr('id');
        if (userId == null) {
            return null;
        }
        console.log(userId.substring(14, userId.length));
        return userId.substring(14, userId.length);
    }

    function getMovieId() {
        let movieId = $('h3.movie-title').attr('id');
        if (movieId == null) {
            return null;
        }
        return movieId.substring(12, movieId.length);
    }

    function getRateId() {
        let rateId = $('span.user-rate-title').attr('id');
        if (rateId == null) {
            return null;
        }
        console.log(rateId.substring(11, rateId.length));
        return rateId.substring(11, rateId.length);

    }
});
