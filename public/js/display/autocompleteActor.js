$(document).ready(function () {
    let query;
    let actorInput =  $('input[type=text]')[0].val();

    actorInput.on('keyup', function () {
        query = $(this).val();
        console.log(query);
        loadUpdateModal(query);
    });

    function loadUpdateModal(query) {
        if (event.keyCode !== 13) {
            if ((query.length) !== 0) {
                $.ajax({
                    url: "http://movie.localhost/api/v1/actors/search?q=" + query,
                    type: "GET",
                    dataType: "json",
                    success: function (actor) {
                        clearSuggestions();
                        let nameContainer = document.getElementById('input-movie-actors');
                        let a = document.createElement('option');
                        a.value =  actor.name;
                        nameContainer.appendChild(a);
                    },
                    error: function (e) {
                        console.log("ERROR: ", e);
                    }
                });
            } else {
                clearSuggestions();
            }

        } else {
            clearDirectorInput();
        }

    }

    function clearDirectorInput() {
        directorInput.val('').change();
    }
});
