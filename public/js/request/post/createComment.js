$(document).ready(function () {

    $('#create-comment-btn').on('click', function () {
        comment();
        emptyCommentArea();
    });

    $('#create-comment-area').on('keydown', function (event) {
        if (event.keyCode === 13) {
            comment();
            emptyCommentArea();
        }
    });

    function comment() {
        let userId = getUserId();
        if (userId !== null) {
            let content = $('#create-comment-area').val();
            if (content.length !== 0) {
                $.ajax({
                    type: 'post',
                    url: 'http://movie.localhost/api/v1/comments/post',
                    dataType: "json",
                    data: {
                        post_id: getPostId(),
                        user_id: userId,
                        content: content,
                    },
                    success: function (newComment) {
                        $.ajax({
                            type: 'GET',
                            url: 'http://movie.localhost/api/v1/comments/get/' + newComment.id,
                            dataType: "json",
                            success: function (comment) {
                                $.ajax({
                                    type: 'GET',
                                    url: 'http://movie.localhost/api/v1/users/get/' + userId + '?relations[]=image',
                                    dataType: "json",
                                    success: function (user) {
                                        let commentSection = $("div.comment-section");
                                        let markup =
                                            "<div class='media mb-4'>" +
                                            " <img class='d-flex mr-3 rounded-circle inset' src='" +user.image.image_url+ "' alt=''>" +
                                            " <div class='media-body'>" +
                                            " <h5 class='mt-0'>" + user.name + "</h5>" +
                                            comment.content +
                                            " </div>" +
                                            " </div>";
                                        let commentSectionContent = commentSection.html();
                                        commentSection.empty();
                                        commentSection.append(markup);
                                        commentSection.append(commentSectionContent);
                                    },
                                    error: function (xhr) {
                                        console.log(xhr);
                                    }
                                });
                            },
                            error: function (xhr) {
                                console.log(xhr);
                            }
                        });
                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }
        } else {
            let message = 'Hãy đăng nhập để bình luận';
            showModal(message);
        }
    }

    function emptyCommentArea() {
        $('#create-comment-area').val('').change();
    }

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
            $modal.find('#login-nav-btn').on('click', function () {
                window.location.href = host + "/login";
            })
        });
        confirmModal.modal('show');
    }

    function getPostId() {
        let postId = $('div.post-content').attr('id');
        return postId .substring(13, postId .length);
    }

    function getUserId() {
        let userId = $('li.user-dropdown').attr('id');
        if (userId == null) {
            return null;
        }
        return userId.substring(14, userId.length);
    }
});