$(document).ready(function() {
    $('#modal-create-btn').on('click', function () {
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/countries/post',
            dataType: "json",
            data: {
                country_name: $('#create-country-name').val(),
            },
            success: function (country) {
                window.location.reload(true);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });
});