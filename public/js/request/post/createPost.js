$(document).ready(function() {
    $('#post-movie-select').chosen({no_results_text: "Oops, nothing found!"});

    $('#post-creator').on('click', function () {
        let file =  $('input[type=file]')[0].files[0];
        if (file != null) {
            createWithImage(file);
        } else {
            createWithoutImage();
        }
    });

    function createWithoutImage() {
        $.ajax({
            type: 'post',
            url: host + '/api/v1/posts/post',
            dataType: "json",
            data: {
                title: $('#title-input').val(),
                movie_id : getMovieId(),
                content : CKEDITOR.instances.editor.getData(),
                user_id : getUserId(),
            },
            success: function (response) {
                console.log(response);
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function createWithImage(file) {
        let formData = new FormData();
        formData.append('title', $('#title-input').val());
        formData.append('movie_id', getMovieId());
        formData.append('content', CKEDITOR.instances.editor.getData());
        formData.append('user_id', getUserId());
        formData.append('image',file);

        $.ajax({
            type: 'post',
            url: host + '/api/v1/posts/post',
            dataType: "formData",
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
        showModal('Cập nhật thành công');
    }

    function getMovieId() {
        let countryId = $('#post-movie-select').find('option:selected').attr('id');
        console.log(countryId.substring(13, countryId.length));
        return countryId.substring(13, countryId.length);
    }

    function getUserId() {
        let userId = $('li.user-dropdown').attr('id');
        if (userId == null) {
            return null;
        }
        return userId.substring(14, userId.length);
    }

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
        });
        confirmModal.modal('show');
    }
});