$(document).ready(function () {
    $('#modal-create-btn').on('click', function () {
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/actors/post',
            dataType: "json",
            data: {
                name: $('#create-actor-name').val(),
                DOB: $('#datepicker2').val(),
                country_id: getCountryId(),
            },
            success: function (newActor) {
                window.location.reload(true);
                // $.ajax({
                //     type: 'GET',
                //     url: 'http://movie.localhost/api/v1/countries/get/' + newActor.country_id,
                //     dataType: "json",
                //     success: function (country) {
                //         let tBody =  $("tbody");
                //         let markup = "<tr id='actor-member-" +newActor.id +"'>" +
                //             "<td>" + newActor.id + "</td>" +
                //             "<td>" + newActor.name + "</td>" +
                //             "<td>" + newActor.DOB +"</td>" +
                //             "<td>" + country.country_name + "</td>" +
                //             "<td>"+ "sit" +"</td>" +
                //             "<td>" +
                //             "<a class='btn btn-outline-primary edit-btn' id='edit-btn-" +newActor.id +"' href='#'>Sửa</a></td>" +
                //             "<td>"+
                //             "<a class='btn btn-outline-primary delete-btn' id='delete-btn-" +newActor.id +"' href='#'>Xóa</a></td>"+
                //             "</tr>";
                //         $("tr.actor-member").last().remove();
                //         let tableContent = tBody.html();
                //         tBody.empty();
                //         tBody.append(markup);
                //         tBody.append(tableContent);
                //     },
                //     error: function (xhr) {
                //         console.log(xhr);
                //     }
                // });
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });
    });

    function getCountryId() {
        let countryId = $('#country-create-option').find('option:selected').attr('id');
        return countryId.substring(15, countryId.length);
    }
});