$(document).ready(function() {
    $('#modal-create-btn').on('click', function () {
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/genres/post',
            dataType: "json",
            data: {
                genre_name: $('#create-genre-name').val(),
            },
            success: function (response) {
                window.location.reload(true);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });
});