$(document).ready(function () {
    $('#modal-create-btn').on('click', function () {
        console.log($('#account-password-input').val());
        console.log($('#input-account-name').val());
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/users/register',
            dataType: "json",
            data: {
                name: $('#input-account-name').val(),
                username: $('#input-account-email').val(),
                password : $('#account-password-input').val(),
                c_password : $('#confirm-password-input').val(),
            },
            success: function (response) {
                console.log(response);
                window.location.reload(true);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });
});