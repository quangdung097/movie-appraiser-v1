$(document).ready(function() {
    $("#input-movie-actors").chosen({max_selected_options: 3});
    $("#input-movie-country").chosen({max_selected_options: 1});
    $("#input-movie-genres").chosen({max_selected_options: 3});
    $("#input-movie-directors").chosen({max_selected_options: 3});

    let title;
    let poster;
    let budget;
    let trailer_url;
    let description;
    let country;
    let directors, genres, actors;

    $('#movie-creator').on('click', function () {
        title = $('#input-movie-title').val();
        budget = $('#input-movie-budget').val();
        poster = $('#input-movie-image').val();
        trailer_url = $('#input-movie-trailer').val();
        description = $('#input-movie-description').val();
        country = getCountryId();
        actors = getActorId();
        directors = getDirectorId();
        genres = getGenreId();

        $.ajax({
            type: 'post',
            url: host + '/api/v1/movies/post',
            dataType: "json",
            data: {
                title: title,
                budget: budget,
                poster_image_url : poster,
                trailer_url : trailer_url,
                description : description,
                country_id : country,
                actors : actors,
                directors : directors,
                genres : genres,
            },
            success: function (newMovie) {
                showModal('Thêm mơi phim '+ newMovie.title+' thành công');
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });

    function getCountryId() {
        let countryId = $('#input-movie-country').find('option:selected').attr('id');
        return countryId.substring(15, countryId.length);
    }

    function getGenreId() {
        let genres = [];
        $('#input-movie-genres').find('option:selected').each(function () {
            let genreId = ($(this).attr('id'));
            genres.push(genreId.substring(13, genreId.length));
        });
        return genres;
    }

    function getActorId() {
        let actors = [];
        $('#input-movie-actors').find('option:selected').each(function () {
            let actorId = ($(this).attr('id'));
            actors.push(actorId.substring(13, actorId.length));
        });
        return actors;
    }

    function getDirectorId() {
        let directors = [];
        $('#input-movie-directors').find('option:selected').each(function () {
            let directorId = ($(this).attr('id'));
            directors.push(directorId.substring(16, directorId.length));
        });
        return directors;
    }

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
        });
        confirmModal.modal('show');
    }
});