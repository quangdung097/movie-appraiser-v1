$(document).ready(function () {
    $(document).on('click', "a[id*=nb-logout]", function() {
        let userId;
        $.ajax({
            url: "http://movie.localhost/api/v1/users/check",
            type: "GET",
            dataType: "json",
            success: function (sessionResponse) {
                userId = sessionResponse.user_id;
                logOut(userId)
            },
            error: function (e) {
                console.log(e);
            }
        });
    });

    function logOut(id) {
        $.ajax({
            url: "http://movie.localhost/api/v1/users/logout",
            type: "POST",
            dataType: "json",
            data: {
                user_id: id
            },
            success: function (response) {
                // showModal(response.Message);
                window.location.replace('http://movie.localhost/');
            },
            error: function (e) {
                console.log("ERROR: ", e);
            }
        });
    }

    function showModal(message) {
        let a = $('#login-modal');
        a.on('show.bs.modal', function(e) {
            let $modal = $(this);
            $modal.find('#login-modal-content').html(message);
        });

        a.modal('show');
    }
});