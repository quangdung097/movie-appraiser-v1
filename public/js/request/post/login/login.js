$(document).ready(function() {

    $('#login_password').on('keyup', function (event) {
        if (event.keyCode === 13) {
            login();
        }
    });

    $('#login_email').on('keyup', function (event) {
        if (event.keyCode === 13) {
            login();
        }
    });

    $('#btn_login').on('click', function () {
        login()
    });

    function login() {
        if (!checkNull()) {
            $.ajax({
                type: 'post',
                url: 'http://movie.localhost/api/v1/users/login',
                dataType: "json",
                data: {
                    username: $('#login_email').val(),
                    password : $('#login_password').val(),
                },
                success: function (response) {
                    console.log(response);
                    if (response.User == null) {
                        showModal(response.Message);
                    } else {
                        // showModal(response.Message);
                        window.location.replace(host);
                    }
                }, error: function (xhr) {
                    console.log(xhr);
                }
            });
        }
    }

    function showModal(message) {
        let a = $('#login-modal');
        a.on('show.bs.modal', function(e) {
            let $modal = $(this);
            $modal.find('#login-modal-content').html(message);
        });

        a.modal('show');
    }

    function checkNull() {
        let email = $('#login_email').val();
        let pass = $('#login_password').val();
        return ((email.length === 0) || (pass.length === 0))
    }
});