$(document).ready(function() {
    $('#confirm-change-password').on('click', function () {
        let newPassword = $('#new-password-input').val();
        let confirmPassword = $('#confirm-password-input').val();
        if (newPassword.length >= 6) {
            if (newPassword === confirmPassword) {
                $.ajax({
                    type: 'patch',
                    url: 'http://movie.localhost/api/v1/users/changePassword',
                    dataType: "json",
                    data: {
                        oldPassword : $('#old-password-input').val(),
                        newPassword : newPassword,
                        cPassword :confirmPassword,
                    },
                    success: function (response) {
                        showModal(response.message);
                    }, error: function (xhr) {
                        showModal(xhr.responseJSON.message);
                        console.log(xhr);
                    }
                });
            } else  {
                showModal("Xác nhận mật khẩu mới sai.");
            }
        } else  {
            showModal("Mật khẩu mới phải dài ít nhất 6 ký tự.");
        }

    });

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
        });
        confirmModal.modal('show');
    }
});