$(document).ready(function() {
    let id = getQueryString('id', window.location.href);
    $('#post-movie-select').chosen({no_results_text: "Oops, nothing found!"});

    $('#post-creator').on('click', function () {
        let file =  $('input[type=file]')[0].files[0];
        if (file != null) {
            updateWithImage(file, id);
        } else {
           updateWithoutImage(id);
        }
    });

    function updateWithoutImage(id) {
        $.ajax({
            type: 'patch',
            url: host + '/api/v1/posts/patch/' + id,
            dataType: "json",
            data: {
                title: $('#title-input').val(),
                movie_id : getMovieId(),
                content : CKEDITOR.instances.editor.getData(),
                user_id : getUserId(),
            },
            success: function (response) {
                console.log(response);
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function updateWithImage(file, id) {
        let formData = new FormData();
        formData.append('title', $('#title-input').val());
        formData.append('movie_id', getMovieId());
        formData.append('content', CKEDITOR.instances.editor.getData());
        formData.append('user_id', getUserId());
        formData.append('image',file);

        $.ajax({
            type: 'post',
            url: host + '/api/v1/posts/patch/' + id,
            dataType: "formData",
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
        showModal('Cập nhật thành công');
    }

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
        });
        confirmModal.modal('show');
    }


    function getQueryString ( field, url ) {
        let href = url ? url : window.location.href;
        let reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        let string = reg.exec(href);
        return string ? string[1] : null;
    }

    function getMovieId() {
        let countryId = $('#post-movie-select').find('option:selected').attr('id');
        console.log(countryId.substring(13, countryId.length));
        return countryId.substring(13, countryId.length);
    }

    function getUserId() {
        let userId = $('li.user-dropdown').attr('id');
        if (userId == null) {
            return null;
        }
        return userId.substring(14, userId.length);
    }
});