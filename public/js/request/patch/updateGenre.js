$(document).ready(function() {
    let id = getQueryString('id', window.location.href);
    $('#post-creator').on('click', function () {
        $.ajax({
            type: 'patch',
            url: 'http://movie.localhost/api/v1/posts/patch/' + id,
            dataType: "json",
            data: {
                title: $('#title-input').val(),
                movie_id : $('#movie-input').val(),
                content : CKEDITOR.instances.editor.getData(),
                user_id : 1,
            },
            success: function (response) {
                console.log(response);
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });


    function getQueryString ( field, url ) {
        let href = url ? url : window.location.href;
        let reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        let string = reg.exec(href);
        return string ? string[1] : null;
    }
});