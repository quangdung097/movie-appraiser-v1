$(document).ready(function() {
    let id = getQueryString('id', window.location.href);
    $('#post-creator').on('click', function () {
        $.ajax({
            type: 'patch',
            url: 'http://movie.localhost/api/v1/posts/patch/' + id,
            dataType: "json",
            data: {
                name: $('#create-actor-name').val(),
                DOB : $('#datepicker2').val(),
                country_id : 2,
            },
            success: function (response) {
                console.log(response);
                alert(response);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    });


    function getQueryString ( field, url ) {
        let href = url ? url : window.location.href;
        let reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        let string = reg.exec(href);
        return string ? string[1] : null;
    }
});