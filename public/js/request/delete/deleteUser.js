$(document).ready(function() {
    $('.delete-btn').on('click', function () {
        let message = 'Xác nhận xóa Tài khoản?';
        let id = getId($(this));
        showModal(message, id);
    });

    function showModal(message, id) {
        let confirmModal = $('#confirm-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#confirm-modal-content').html(message);
            $modal.find('.btn-primary').on('click', function () {
                requestDelete(id)
            })
        });
        confirmModal.modal('show');
    }

    function requestDelete(id) {
        let tables = 'users';
        $.ajax({
            type: "delete",
            url: host + "/api/v1"+"/"+tables+"/delete/"+id,
            success: function (res) {
                if (!res.error) {
                    $('#user-member-' + id).remove();
                    $('#confirm-modal').modal('hide');
                    window.location.reload(true);
                }
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function getId(element) {
        let inputId = element.attr('id');
        return inputId.substring(11, inputId.length);
    }
});


