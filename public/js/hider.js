$(document).ready(function(){

		  	$('#loader').on('click', function(){
		        $.ajax({
		            url: "http://movie.localhost/api/v1/users/get/5",
		            type: "GET",
		            dataType : "json",
		            success: function(data) {
		                console.log(data);
		                $("#updateUserId").val(data.id);
                        $("#updateUserName").val(data.name);
                        $("#updateUserEmail").val(data.email);
		            },
		            error : function(e) {
		                console.log("ERROR: ", e);
		            }
		        });
    
		  });


			// create

			$('#create_user').on('click', function () {
				$.ajax({
					type: 'post',
					// url: "{{asset('api/v1')}}"+"/"+tables,
					url: 'http://movie.localhost/api/v1/users/createUser',
					dataType: "json",
					data: {
						name: $('#inputUserName').val(),
						email: $('#inputUserEmail').val(),
						password : $('#inputUserPassword').val(),
					},
					success: function (response) {
						// $('#addModal').modal('hide');

						alert('hehe');
					}, error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr);

					}

				});
			});



			/////////////////
			//upload method//
			/////////////////

			$('#update_user').on('click', function () {
				var id=$('#edit_id').val();
				$.ajax({
					type: 'patch',
					url: 'http://movie.localhost/api/v1/users/updateUser',
					dataType: "json",
					data: {
						id: $('#updateUserId').val(),
						name: $('#updateUserName').val(),
						email: $('#updateUserEmail').val(),

					},
					success: function (response) {
                        console.log('update success!');
					}, error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr);

					}

				});
			});
		});













//////////////////
//delete method //
//////////////////



function deleteF(id) {
    swal({
            title: "Bạn có chắc muốn xóa?",
            // text: "Bạn sẽ không thể khôi phục lại bản ghi này!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Không",
            confirmButtonText: "Có",
            // closeOnConfirm: false,
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "delete",
                    url: "{{asset('api/v1')}}"+"/"+tables+"/"+id,
                    success: function (res) {
                        if (!res.error) {
                            toastr.success('Xóa thành công!');
                            $('#category_' + id).remove();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr.error(thrownError);
                    }
                });
            } else {
                toastr.error("Thao tác xóa đã bị huỷ bỏ!");
            }
        });
};
