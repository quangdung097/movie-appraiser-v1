$(document).ready(function() {
    let userId;
    $.ajax({
        url: "http://movie.localhost/api/v1/users/check",
        type: "GET",
        dataType: "json",
        success: function (sessionResponse) {
            let sessionState = sessionResponse.State;
            if (sessionState === false) {
                arrangeInitialDropDown();
            } else {
                userId = sessionResponse.user_id;
                requestInformation();
            }
        },
        error: function (e) {
            console.log(e);
        }
    });

    function requestInformation() {
        $.ajax({
            url: "http://movie.localhost/api/v1/users/get/" + userId + '?relations[]=role',
            type: "GET",
            dataType: "json",
            success: function (userData) {
                console.log(userData);
                arrangeMenu(userData);
                arrangeNavBar(userData);

            },
            error: function (e) {
                console.log("ERROR: ", e);
            }
        });
    }

    function arrangeNavBar(data) {
        arrangeAccountDropDown();
        loadUserInformation(data);
    }

    function arrangeMenu(data) {

    }

    function loadUserInformation(data) {
        let userInformationContainer= document.getElementById('navbarDropdown');

        //set username
        userInformationContainer.innerHTML = data.username;
    }

    function arrangeAccountDropDown() {
        let template = document.querySelector('.account-dropdown');
        arrangeDropDown(template);
    }

    function arrangeInitialDropDown() {
        let template = document.querySelector('.init-dropdown');
        arrangeDropDown(template);
    }

    function arrangeDropDown(template){
        let dropDownContainer= document.getElementById('navBar-dropdown-container');
        template = document.importNode(template.content, true);
        dropDownContainer.appendChild(template);
    }
});
