$(document).ready(function() {
    //load template
    let template = document.querySelector('.post');
    let directorTagTemplate = document.querySelector('.post-tag');

    $.ajax({
        url: "http://movie.localhost/api/v1/movies/all?relations[]=actors&relations[]=genres&relations[]=directors",
        type: "GET",
        dataType: "json",
        success: function (data) {
            console.log(data);
            let postContainer= document.getElementById('post_column');

            data.forEach(
                function (element) {

                    // Clone the new row and insert it into the table
                    let clone = document.importNode(template.content, true);

                    //get container for tag
                    let directorContainer= clone.getElementById('director-tag-container');
                    let actorContainer= clone.getElementById('actor-tag-container');

                    //set title
                    let postTitle = clone.getElementById('post_title');
                    postTitle.innerHTML = element.title;
                    postTitle.setAttribute('href', 'http://movie.localhost/post');

                    //set image
                    let image = clone.getElementById('post_image');
                    image.setAttribute('src', element.poster_image_url);
                    image.setAttribute('alt', element.title);

                    //setImageLink
                    let imageLink = clone.getElementById('image_link');
                    imageLink.setAttribute('href', 'http://movie.localhost/post');

                    //set director and actor
                    let tagContent;
                    let actors = element.actors;

                    actors.forEach(function (actor) {
                        let tagClone= document.importNode(directorTagTemplate.content, true);
                       tagContent = tagClone.getElementById('post-tag-content');
                       tagContent.innerHTML = actor.name + ' ,';
                       actorContainer.appendChild(tagClone);
                    });

                    let directors = element.directors;
                    directors.forEach(function (director) {
                        let tagClone = document.importNode(directorTagTemplate.content, true);
                        tagContent = tagClone.getElementById('post-tag-content');
                        tagContent.innerHTML = director.name;
                        directorContainer.appendChild(tagClone);
                    });

                    //set description
                    let description= clone.getElementById('post-description');
                    description.innerHTML = element.description;

                    //append to post to <div>
                    postContainer.appendChild(clone);
                }
            )
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });



});
