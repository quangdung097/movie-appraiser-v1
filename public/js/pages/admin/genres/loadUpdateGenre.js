$(document).ready(function() {
    let mainID;
    let count;
    let initGenreName;

    $('.edit-btn').on('click', function () {
        let id = getId($(this));
        mainID = id;
        count = 1;
        showModal('',id);
    });

    function showModal(message, id) {
        console.log("show modal " + id);
        let updateModal = $('#update-modal');
        updateModal.on('show.bs.modal', function() {
            let modal = $(this);
            modal.find('.modal-hide').hide();
            loadUpdateModal(id);
            modal.find('#input-genre-name').html('');
            modal.find('.btn-primary').on('click', function () {
                requestUpdate(id);
            })
        });
        updateModal.modal('show');
    }

    function loadUpdateModal(id)
    {
        if (id === mainID && count === 1) {
            count++;
            console.log("load modal " + id);
            $.ajax({
                url: "http://movie.localhost/api/v1/genres/get/" + id,
                type: "GET",
                dataType: "json",
                success: function (genre) {
                    let nameContainer= document.getElementById('input-genre-name');
                    nameContainer.value = genre.genre_name;
                    initGenreName = genre.genre_name;
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function requestUpdate(id)
    {
        let newName = $('#input-genre-name').val();
        if ((!compare(initGenreName, newName))) {
            $.ajax({
                type: 'patch',
                url: 'http://movie.localhost/api/v1/genres/patch/' + id,
                dataType: "json",
                data:
                    {
                        genre_name: newName,
                    },
                success: function (response) {
                    window.location.reload(true);
                }, error: function (xhr) {
                    console.log(xhr);
                }
            });
        } else {
            window.location.reload(true);
        }
    }

    function compare(subject1, subject2) {
        return (subject1 === subject2);
    }

    function getId(element) {
        let inputId = element.attr('id');
        return inputId.substring(9, inputId.length);
    }

});
