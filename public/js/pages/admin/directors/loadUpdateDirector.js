$(document).ready(function () {
    let mainID;
    let count;
    let initDirectorName;

    $('.edit-btn').on('click', function () {
        let id = getId($(this));
        mainID = id;
        count = 1;
        showModal('', id);
    });

    function showModal(message, id) {
        let updateModal = $('#update-modal');
        updateModal.on('show.bs.modal', function () {
            let modal = $(this);
            loadUpdateModal(id);
            modal.find('.modal-hide').hide();
            modal.find('#input-director-name').html('');
            modal.find('.btn-primary').on('click', function () {
                requestUpdate(id);
            })
        });
        updateModal.modal('show');
    }

    function loadUpdateModal(id) {
        if (id === mainID && count === 1) {
            count++;
            $.ajax({
                url: host + "/api/v1/directors/get/" + id + "?relations[]=country",
                type: "GET",
                dataType: "json",
                success: function (director) {
                    let nameContainer = document.getElementById('input-director-name');
                    nameContainer.value = director.name;
                    initDirectorName = director.name;
                    let dobContainer = document.getElementById('datepicker');
                    dobContainer.value = director.DOB;
                    let countryContainer = document.getElementById('country-update-option');
                    countryContainer.value = director.country.country_name;
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function requestUpdate(id) {
        let newName = $('#input-director-name').val();
        let DOB = $('#datepicker').val();
        $.ajax({
            type: 'patch',
            url: 'http://movie.localhost/api/v1/directors/patch/' + id,
            dataType: "json",
            data: (compare(initDirectorName, newName)) ?
                {
                    DOB: DOB,
                    country_id: getCountryId(),
                }
                :
                {
                    name: newName,
                    DOB: DOB,
                    country_id: getCountryId(),
                },
            success: function (response) {
                window.location.reload(true);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function getCountryId() {
        let countryId = $('#country-update-option').find('option:selected').attr('id');
        return countryId.substring(15, countryId.length);
    }

    function compare(subject1, subject2) {
        return (subject1 === subject2);
    }

    function getId(element) {
        let inputId = element.attr('id');
        return inputId.substring(9, inputId.length);
    }

});
