$(document).ready(function () {
    let mainID;
    let count;
    let initEmail;

    $('.edit-btn').on('click', function () {
        let id = getId($(this));
        mainID = id;
        count = 1;
        showModal('', id);
    });

    function showModal(message, id) {
        let updateModal = $('#update-modal');
        updateModal.on('show.bs.modal', function () {
            let modal = $(this);
            loadUpdateModal(id);
            modal.find('.modal-hide').hide();
            modal.find('#input-account-name').html('');
            modal.find('#modal-update-btn').on('click', function () {
                requestUpdate(id);
            });
        });
        updateModal.modal('show');
    }

    function loadUpdateModal(id) {
        if (id === mainID && count === 1) {
            count++;
            $.ajax({
                url: "http://movie.localhost/api/v1/users/get/" + id,
                type: "GET",
                dataType: "json",
                success: function (actor) {
                    let nameContainer = document.getElementById('update-account-name');
                    nameContainer.value = actor.name;
                    let emailContainer = document.getElementById('update-account-email');
                    emailContainer.value = actor.username;
                    initEmail = actor.username;
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function requestUpdate(id) {
        count++;
        let newName = $('#update-account-name').val();
        let newEmail = $('#update-account-email').val();
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/users/patch/' + id,
            dataType: "json",
            data: (compare(initEmail, newEmail)) ?
                {
                    name: newName,
                }
                :
                {
                    name: newName,
                    username: newEmail,
                },
            success: function (response) {
                window.location.reload(true);
            }, error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function compare(subject1, subject2) {
        return (subject1 === subject2);
    }

    function getId(element) {
        let inputId = element.attr('id');
        return inputId.substring(9, inputId.length);
    }

});
