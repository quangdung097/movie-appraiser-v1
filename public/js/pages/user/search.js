$(document).ready(function () {
    let query;
    let nextLink;

    $('.nav-search').on('keyup', function (event) {
        query = $(this).val();
        if (event.keyCode === 13) {
            loadPage(query)
        }
    });

    function loadPage(query) {
        console.log(host + '?q=' + query);
        window.open(host + '/home/search/movies?q=' + query, '_blank').focus();
    }

    $('.search-addition').on('click', function (e) {
        e.preventDefault();
        query = getQueryString('q', window.location.href);
        nextLink = $(this).attr('href');
        window.location.replace(host + nextLink + '?q=' + query);
    });


    function getQueryString ( field, url ) {
        let href = url ? url : window.location.href;
        let reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        let string = reg.exec(href);
        return string ? string[1] : null;
    }
});
