$(document).ready(function () {
    let mainID;
    let count;
    let initUserName;

    $('#change-name-btn').on('click', function () {
        let id = getId();
        mainID = id;
        count = 1;
        loadModal('', id);
    });

    function loadModal(message, id) {
        let updateModal = $('#update-name-modal');
        updateModal.on('show.bs.modal', function () {
            let modal = $(this);
            loadUpdateModal(id);
            modal.find('#confirm-update-name').on('click', function () {
                requestUpdate(id);
            });
            modal.find('.btn-secondary').on('click', function () {
                window.location.reload(true);
            });
        });
        updateModal.modal('show');
    }

    function loadUpdateModal(id) {
        if (id === mainID && count === 1) {
            count++;
            $.ajax({
                url: "http://movie.localhost/api/v1/users/get/" + id,
                type: "GET",
                dataType: "json",
                success: function (user) {
                    // let nameContainer = document.getElementById('new-name-input');
                    // nameContainer.value = user.name;
                    initUserName = user.name;
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function requestUpdate(id) {
        count++;
        let newName = $('#new-name-input').val();
        let file =  $('input[type=file]')[0].files[0];

        let formData = new FormData();
        if (!compare(initUserName, newName)) {
            formData.append('name', newName);
        }
        if((newName.length) === 0){
            formData.append('name', initUserName);
        }
        if (file != null) {
            formData.append('avatar',file);
        }
        $.ajax({
            type: 'post',
            url: 'http://movie.localhost/api/v1/users/patch/' + id,
            dataType: "formData",
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {

            }, error: function (xhr) {
                console.log(xhr);
            }
        });
        showModal('Cập nhật thành công');

    }

    function getId() {
        let userId = $('li.user-dropdown').attr('id');
        if (userId == null) {
            return null;
        }
        return userId.substring(14, userId.length);
    }

    function compare(subject1, subject2) {
        return (subject1 === subject2);
    }

    function showModal(message) {
        let confirmModal = $('#warning-modal');
        confirmModal.on('show.bs.modal', function() {
            let $modal = $(this);
            $modal.find('#warning-modal-content').html(message);
        });
        confirmModal.modal('show');
    }

    // var formData = new FormData();
    // formData.append('title', title);
    // for(var auth_id in author_id_){
    //     formData.append('authors[]',author_id_[auth_id]);
    // }
    // for (var ge_id in genre_id_){
    //     formData.append('genres[]',genre_id_[ge_id]);
    // }
    //
    // formData.append('publisher_id', publisher_id);
    // formData.append('publishedYear', publishedYear);
    // formData.append('image', $('input[type=file]')[0].files[0]);
    // for(var pair of formData.entries()) {
    //     console.log(pair[0]+ ', '+ pair[1]);
    // }
});
