$(document).ready(function() {
    const maxLen = 80;

    $('p.movie-card-description.card-text').each(function () {
        let element = $(this);
        let truncated = element.html();

        if (truncated.length > maxLen) {
            truncated = truncated.substr(0,maxLen) + '...';
        }
        element.html(truncated);
    });
});