$(document).ready(function () {
    let query;

    $('.article-search-input').on('keyup', function (event) {
        query = $(this).val();
        if (event.keyCode === 13) {
            if (query.toString().length > 0) {
                loadPage(query);
            }
        }
    });

    $('.article-search-btn').on('click', function () {
        query = $('.article-search-input').val();
        if (query.toString().length > 0) {
            loadPage(query);
        }
    });

    function loadPage(query) {
        console.log(host + '?q=' + query);
        // window.location.replace(host + '/home/search/articles/headline?q=' + query);
        window.open(host + '/home/search/articles/headline?q=' + query, '_blank').focus();
    }
});