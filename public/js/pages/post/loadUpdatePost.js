$(document).ready(function() {
    let id = getQueryString('id', window.location.href);
    $.ajax({
        url: host + "/api/v1/posts/get/" + id + "?relations[]=movie",
        type: "GET",
        dataType: "json",
        success: function (post) {
            console.log(post);
            let titleContainer= document.getElementById('title-input');
            titleContainer.value = post.title;
            CKEDITOR.instances.editor.setData(post.content);
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });

    function getQueryString ( field, url ) {
        let href = url ? url : window.location.href;
        let reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        let string = reg.exec(href);
        return string ? string[1] : null;
    }

});
