<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    Route::prefix('users')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\UserController@get',
            'as' => 'api/v1/users/get'
        ]);

        Route::post('post', [
            'uses' => 'API\UserController@post',
            'as' => 'api/v1/users/post'
        ]);

        Route::post('patch/{id?}', [
            'uses' => 'API\UserController@patch',
            'as' => 'api/v1/users/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\UserController@delete',
            'as' => 'api/v1/users/delete'
        ]);

        Route::post('register', [
            'uses' => 'API\UserController@register',
            'as' => 'api/v1/users/register'
        ]);

        Route::post('login', [
            'uses' => 'API\UserController@login',
            'as' => 'api/v1/users/login'
        ]);

        Route::post('logout', [
            'uses' => 'API\UserController@logout',
            'as' => 'api/v1/users/logout'
        ]);

        Route::get('check', [
            'uses' => 'API\UserController@getSessionData',
            'as' => 'api/v1/users/check'
        ]);

        Route::patch('changePassword', [
            'uses' => 'API\UserController@changePassword',
            'as' => 'api/v1/users/changePassword'
        ]);

    });
    Route::prefix('actors')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\ActorController@get',
            'as' => 'api/v1/actors/get'
        ]);

        Route::post('post', [
            'uses' => 'API\ActorController@post',
            'as' => 'api/v1/actors/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\ActorController@patch',
            'as' => 'api/v1/actors/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\ActorController@delete',
            'as' => 'api/v1/actors/delete'
        ]);

        Route::get('search', [
            'uses' => 'API\ActorController@search',
            'as' => 'api/v1/actors/search'
        ]);

    });

    Route::prefix('comments')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\CommentController@get',
            'as' => 'api/v1/comments/get'
        ]);

        Route::post('post', [
            'uses' => 'API\CommentController@post',
            'as' => 'api/v1/comments/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\CommentController@patch',
            'as' => 'api/v1/comments/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\CommentController@delete',
            'as' => 'api/v1/comments/delete'
        ]);

    });

    Route::prefix('countries')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\CountryController@get',
            'as' => 'api/v1/countries/get'
        ]);

        Route::post('post', [
            'uses' => 'API\CountryController@post',
            'as' => 'api/v1/countries/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\CountryController@patch',
            'as' => 'api/v1/countries/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\CountryController@delete',
            'as' => 'api/v1/countries/delete'
        ]);

    });


    Route::prefix('directors')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\DirectorController@get',
            'as' => 'api/v1/directors/get'
        ]);

        Route::post('post', [
            'uses' => 'API\DirectorController@post',
            'as' => 'api/v1/directors/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\DirectorController@patch',
            'as' => 'api/v1/directors/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\DirectorController@delete',
            'as' => 'api/v1/directors/delete'
        ]);

        Route::get('search', [
            'uses' => 'API\DirectorController@search',
            'as' => 'api/v1/directors/search'
        ]);

    });

    Route::prefix('genres')->group(function () {

        Route::get('get/{id?}', [
            'uses' => 'API\GenreController@get',
            'as' => 'api/v1/genres/get'
        ]);

        Route::post('post', [
            'uses' => 'API\GenreController@post',
            'as' => 'api/v1/genres/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\GenreController@patch',
            'as' => 'api/v1/genres/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\GenreController@delete',
            'as' => 'api/v1/genres/delete'
        ]);

    });

    Route::prefix('images')->group(function () {


        Route::post('post', [
            'uses' => 'API\ImageController@post',
            'as' => 'api/v1/images/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\ImageController@patch',
            'as' => 'api/v1/images/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\ImageController@delete',
            'as' => 'api/v1/images/delete'
        ]);

    });

    Route::prefix('movies')->group(function () {
        Route::get('get/{id?}', [
            'uses' => 'API\MovieController@get',
            'as' => 'api/v1/movies/get'
        ]);

        Route::post('post', [
            'uses' => 'API\MovieController@post',
            'as' => 'api/v1/movies/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\MovieController@patch',
            'as' => 'api/v1/movies/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\MovieController@delete',
            'as' => 'api/v1/movies/delete'
        ]);

        Route::get('all', [
            'uses' => 'API\MovieController@all',
            'as' => 'api/v1/movies/all'
        ]);

        Route::post('paginate', [
            'uses' => 'API\MovieController@paginate',
            'as' => 'api/v1/posts/paginate'
        ]);

        Route::get('search', [
            'uses' => 'API\MovieController@search',
            'as' => 'api/v1/movies/search'
        ]);

    });

    Route::prefix('posts')->group(function () {
        Route::get('get/{id?}', [
            'uses' => 'API\PostController@get',
            'as' => 'api/v1/posts/get'
        ]);

        Route::post('post', [
            'uses' => 'API\PostController@post',
            'as' => 'api/v1/posts/post'
        ]);

        Route::post('patch/{id?}', [
            'uses' => 'API\PostController@patch',
            'as' => 'api/v1/posts/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\PostController@delete',
            'as' => 'api/v1/posts/delete'
        ]);

        Route::get('all', [
            'uses' => 'API\PostController@all',
            'as' => 'api/v1/posts/all'
        ]);
    });


    Route::prefix('roles')->group(function () {
        Route::get('get/{id?}', [
            'uses' => 'API\RoleController@get',
            'as' => 'api/v1/roles/get'
        ]);

        Route::post('post', [
            'uses' => 'API\RoleController@post',
            'as' => 'api/v1/roles/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\RoleController@patch',
            'as' => 'api/v1/roles/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\RoleController@delete',
            'as' => 'api/v1/roles/delete'
        ]);

    });

    Route::prefix('rates')->group(function () {

        Route::post('post', [
            'uses' => 'API\UserRateController@post',
            'as' => 'api/v1/rates/post'
        ]);

        Route::patch('patch/{id?}', [
            'uses' => 'API\UserRateController@patch',
            'as' => 'api/v1/rates/patch'
        ]);

        Route::delete('delete/{id?}', [
            'uses' => 'API\UserRateController@delete',
            'as' => 'api/v1/rates/delete'
        ]);

        Route::get('all', [
            'uses' => 'API\UserRateController@get',
            'as' => 'api/v1/rates/get'
        ]);
    });

    Route::prefix('news')->group(function () {
        Route::get('headline', [
            'uses' => 'News\HeadlineNewsController@get',
            'as' => 'api/v1/news/headline'
        ]);

        Route::get('general', [
            'uses' => 'News\GeneralNewsController@get',
            'as' => 'api/v1/news/general'
        ]);

    });

});

