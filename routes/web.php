<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('home/movie');
});

Route::get('/index', function () {
    return view('pages.home');
});




Route::get('/users', function () {
    return view('pages.users');
});

Route::get('/demo', function () {
    return view('demo');
});

Route::get('/login', function () {
    return view('pages.accountPages.login');
});

Route::get('/register', function () {
    return view('pages.accountPages.register');
});

Route::get('/test', function () {
    return view('test');
});

Route::prefix('admin')->group(function () {

    Route::prefix('manager')->group(function () {

        Route::prefix('posts')->group(function () {
            Route::get('', [
                'uses' => 'View\PostController@adminPostPaginate',
                'as' => 'admin/manager/posts'
            ]);

            Route::get('create', [
                'uses' => 'View\PostController@adminPostPost',
                'as' => 'admin/manager/posts/create'
            ]);

            Route::get('update', [
                'uses' => 'View\PostController@adminUpdatePost',
                'as' => 'admin/manager/posts/update'
            ]);
        });

        Route::prefix('movies')->group(function () {
            Route::get('', [
                'uses' => 'View\MovieController@adminMoviePaginate',
                'as' => 'admin/manager/movies'
            ]);

            Route::get('create', [
                'uses' => 'View\MovieController@adminPostMovie',
                'as' => 'admin/manager/movies/create'
            ]);

            Route::get('update', [
                'uses' => 'View\MovieController@adminUpdateMovie',
                'as' => 'admin/manager/movies/update'
            ]);
        });

        Route::prefix('directors')->group(function () {
            Route::get('', [
                'uses' => 'View\DirectorController@adminDirectorPaginate',
                'as' => 'admin/manager/director'
            ]);

            Route::get('create', [
                'uses' => 'View\DirectorController@adminPostDirector',
                'as' => 'admin/manager/directors/create'
            ]);
        });

        Route::prefix('actors')->group(function () {
            Route::get('', [
                'uses' => 'View\ActorController@adminActorPaginate',
                'as' => 'admin/manager/actors'
            ]);

            Route::get('create', [
                'uses' => 'View\ActorController@adminPostActor',
                'as' => 'admin/manager/actors/create'
            ]);
        });

        Route::prefix('genres')->group(function () {
            Route::get('', [
                'uses' => 'View\GenreController@adminGenrePaginate',
                'as' => 'admin/manager/genres'
            ]);

            Route::get('create', [
                'uses' => 'View\GenreController@adminPostGenre',
                'as' => 'admin/manager/genres/create'
            ]);
        });

        Route::prefix('countries')->group(function () {
            Route::get('', [
                'uses' => 'View\CountryController@adminCountryPaginate',
                'as' => 'admin/manager/countries'
            ]);

            Route::get('create', [
                'uses' => 'View\CountryController@adminPostCountry',
                'as' => 'admin/manager/countries/create'
            ]);
        });

        Route::prefix('users')->group(function () {
            Route::get('', [
                'uses' => 'View\UserController@getUserPaginatedView',
                'as' => 'admin/manager/users'
            ]);
        });
    });
});


Route::prefix('posts')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\PostController@getPage',
        'as' => 'posts/get'
    ]);

});

Route::prefix('genres')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\GenreController@getPage',
        'as' => 'genres/get'
    ]);

});

Route::prefix('actors')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\ActorController@getPage',
        'as' => 'actors/get'
    ]);

});

Route::prefix('directors')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\DirectorController@getPage',
        'as' => 'directors/get'
    ]);

});

Route::prefix('countries')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\CountryController@getPage',
        'as' => 'countries/get'
    ]);

});

Route::prefix('user')->group(function () {

    Route::get('profile', [
        'uses' => 'View\UserController@getPage',
        'as' => 'user/profile'
    ]);

});

Route::prefix('movies')->group(function () {

    Route::get('{id}', [
        'uses' => 'View\MovieController@getPage',
        'as' => 'movies/get'
    ]);

});


Route::prefix('home')->group(function () {
    Route::get('movies', [
        'uses' => 'View\MovieController@moviePaginate',
        'as' => 'home/movie'
    ]);

    Route::get('posts', [
        'uses' => 'View\PostController@postPaginate',
        'as' => 'home/posts'
    ]);

    Route::get('search/movies', [
        'uses' => 'View\SearchController@getPage',
        'as' => 'home/movies/search'
    ]);

    Route::get('search/actors', [
        'uses' => 'View\SearchController@getActorPage',
        'as' => 'home/actors/search'
    ]);

    Route::get('search/directors', [
        'uses' => 'View\SearchController@getDirectorPage',
        'as' => 'home/directors/search'
    ]);

    Route::prefix('search/articles')->group(function () {
        Route::get('headline', [
            'uses' => 'View\ArticleSearchController@getHeadlineNewsPage',
            'as' => 'home/articles/search/headline'
        ]);

        Route::get('general', [
            'uses' => 'View\ArticleSearchController@getGeneralNewsPage',
            'as' => 'home/articles/search/general'
        ]);
    });
});
