<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials._head')
        @yield('stylesheet')
        @include('partials._generalScript')
        @include('partials._generalStyleSheet')
    </head>
    <body>
        <div class="container body-content">
            @yield('navBar')
            @yield('content')
            @include('partials._footer')
        </div>{{--end of .container--}}

        @yield('script')
    </body>
</html>