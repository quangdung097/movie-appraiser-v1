<div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đổi mật khẩu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="user-email">Mật khẩu cũ</label>
                    <input type="password" class="form-control" id="old-password-input" placeholder="Nhập mật khẩu hiện tại">
                </div>
                <div class="form-group">
                    <label for="user-email">Mật khẩu mới</label>
                    <input type="password" class="form-control" id="new-password-input" placeholder="Nhập mật khẩu mới">
                </div>
                <div class="form-group">
                    <label for="user-email">Xác nhận mật khẩu</label>
                    <input type="password" class="form-control" id="confirm-password-input" placeholder="Nhập lại mật khẩu mới">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tắt</button>
                <button type="button" class="btn btn-primary" id="confirm-change-password">Đổi mật khẩu</button>
            </div>
        </div>
    </div>
</div>