<div class="modal fade" id="update-name-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cập nhật thông tin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Thay Hình đại diện</label>
                        <input type="file" class="form-control-file" id="new-file-input">
                    </div>
                </form>
                <div class="form-group">
                    <label for="user-email">Tên mới</label>
                    <input class="form-control" id="new-name-input" placeholder="{{$user->name}}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tắt</button>
                <button type="button" class="btn btn-primary" id="confirm-update-name">Cập nhật</button>
            </div>
        </div>
    </div>
</div>