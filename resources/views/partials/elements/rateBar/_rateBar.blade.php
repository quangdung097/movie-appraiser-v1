<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@if(isset($user))
    @if(isset($movie['individualRate']))
        <span class="heading user-rate-title" id="rate-title-{{$movie['individualRate']['id']}}">Bạn đánh giá</span>
    @else
        <span class="heading">Hãy đánh giá</span>
    @endif

    @for($i = 1; $i <= $movie['individualRate']['rate']; $i++)
        <span class="fa fa-star checked" id="star-0{{$i}}"></span>
    @endfor
    @for($i = $movie['individualRate']['rate'] + 1; $i <=5; $i++)
        <span class="fa fa-star" id="star-0{{$i}}"></span>
    @endfor
@endif

<p>
    <small>
        <b>
            Trung bình {{$movie['averageRate']}} điểm dựa trên {{$movie['countRate']}} đánh giá.
        </b>
    </small>
</p>
<hr style="border:3px solid #f1f1f1">

<div class="row">
    <div class="side">
        <div>5 star</div>
    </div>
    <div class="middle">
        <div class="bar-container">
            <div class="bar-5" style="width: {{$movie['rateTypeCount']['rateFivePercent']}}%;"></div>
        </div>
    </div>
    <div class="side right">
        <div>{{$movie['rateTypeCount']['rateFive']}}</div>
    </div>
    <div class="side">
        <div>4 star</div>
    </div>
    <div class="middle">
        <div class="bar-container">
            <div class="bar-4" style="width: {{$movie['rateTypeCount']['rateFourPercent']}}%;"></div>
        </div>
    </div>
    <div class="side right">
        <div>{{$movie['rateTypeCount']['rateFour']}}</div>
    </div>
    <div class="side">
        <div>3 star</div>
    </div>
    <div class="middle">
        <div class="bar-container">
            <div class="bar-3" style="width: {{$movie['rateTypeCount']['rateThreePercent']}}%;"></div>
        </div>
    </div>
    <div class="side right">
        <div>{{$movie['rateTypeCount']['rateThree']}}</div>
    </div>
    <div class="side">
        <div>2 star</div>
    </div>
    <div class="middle">
        <div class="bar-container">
            <div class="bar-2" style="width: {{$movie['rateTypeCount']['rateTwoPercent']}}%;"></div>
        </div>
    </div>
    <div class="side right">
        <div>{{$movie['rateTypeCount']['rateTwo']}}</div>
    </div>
    <div class="side">
        <div>1 star</div>
    </div>
    <div class="middle">
        <div class="bar-container">
            <div class="bar-1" style="width: {{$movie['rateTypeCount']['rateOnePercent']}}%;"></div>
        </div>
    </div>
    <div class="side right">
        <div>{{$movie['rateTypeCount']['rateOne']}}</div>
    </div>
</div>

<link rel="stylesheet" href="/css/elements/rateBar/rateBar.css">
<script src="/js/display/rateBar.js"></script>