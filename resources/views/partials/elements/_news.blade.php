@foreach($extends['articles'] as $article)
    <h5>
        <a href="{{$article['url']}}" target="_blank" class="user-link">
            {{$article['title']}}
        </a>
    </h5>
    <p class="article-description">
        {{$article['description']}}
    </p>
    <hr>
@endforeach
