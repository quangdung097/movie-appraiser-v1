<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="{{$post->movie->poster_image_url}}" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title">{{$post->title}}</h5>
        <p class="card-text">{{$post->movie->description}}.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
</div>
<hr>