<div id="post_element" class="row no-gutters">
    <div class="col-md-4">
        <a href="/posts/{{$post->id}}" class="image_link" target="_blank">
            <img src="{{$post->movie->poster_image_url}}" alt="{{$post->movie->poster_image_url}}" class="card-img"
                 id="post_image">
        </a>
    </div>
    <div class="col-md-8">
        <div class="card-body">
            {{--<iframe width="560" height="350" src="https://www.youtube.com/embed/{{$post->movie->trailer_url}}"--}}
                    {{--frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"--}}
                    {{--allowfullscreen></iframe>--}}
            <h2 class="card-title">
                <a href="/posts/{{$post->id}}" class="post_title" target="_blank">{{$post->title}}</a>
            </h2>
            <div class="card-text" style="font-size: 120%; color: #4c4c4f;">
                <div>
                    <span id="director-tag-container">Directors: </span>
                    @foreach($post->movie->directors as $directors)
                        <span>
                            <a class="post-link" id="post-tag-content" href="#">
                                {{$directors->name}}
                            </a>
                         </span>
                    @endforeach
                </div>
                <div>
                    <span id="actor-tag-container">Actors:</span>
                    @foreach($post->movie->actors as $actor)
                        <span>
                            <a class="post-link" id="post-tag-content" href="#">
                                {{$actor->name}}
                            </a>
                         </span>
                    @endforeach
                </div>
            </div>
            <p class="card-text">
                <small class="text-muted" id="post-description">
                    {{$post->movie->description}}
                </small>
            </p>
            <p>
            <a class="btn btn-primary btn-log" href="#" role="button">
            Read more
            </a>
            </p>
        </div>

    </div>

</div>
<hr>