<div class="card">
    <img class="card-img-top" src="{{$post->movie->poster_image_url}}" alt="Card image cap" style="width:100px;height:149px;">
    <div class="card-body">
        <h5 class="card-title">{{$post->title}}</h5>
        <p class="card-text">{{$post->movie->description}}</p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
</div>
<hr>