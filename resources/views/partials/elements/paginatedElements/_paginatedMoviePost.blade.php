<div id="post_element" class="row no-gutters" >
    <div class="col-md-4">
        <a href="/movies/{{$movie->id}}" class="image_link" target="_blank">
            <img src="{{$movie->poster_image_url}}" alt="{{$movie->poster_image_url}}" class="card-img" id="post_image">
        </a>
    </div>
    <div class="col-md-8">
        <div class="card-body">
            <h2 class="card-title">
                <a href="/movies/{{$movie->id}}" class="post_title" target="_blank">{{$movie->title}}</a>
            </h2>
            <div class="card-text" style="font-size: 120%; color: #4c4c4f;">
                <div>
                    <span id="director-tag-container">Directors: </span>
                    @foreach($movie->directors as $director)
                        <span>
                            <a class="post-link" id="post-tag-content"
                               href="/directors/{{$director->id}}" target="_blank">
                                {{$director->name}}
                            </a>
                         </span>
                    @endforeach
                </div>
                <div>
                    <span id="actor-tag-container">Actors:</span>
                    @foreach($movie->actors as $actor)
                        <span>
                            <a class="post-link" id="post-tag-content"
                               href="/actors/{{$actor->id}}" target="_blank">
                                {{$actor->name}}
                            </a>
                         </span>
                    @endforeach
                </div>
            </div>
            <p class="card-text">
                <small class="text-muted" id="post-description">
                    {{$movie->description}}
                </small>
            </p>
            {{--<p>--}}
                {{--<a class="btn btn-primary btn-log" href="#" role="button">--}}
                    {{--Read more--}}
                {{--</a>--}}
            {{--</p>--}}
        </div>

    </div>

</div>
<hr>