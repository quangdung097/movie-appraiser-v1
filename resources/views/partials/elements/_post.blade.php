<template class="post card mb-3" >
    <div id="post_element" class="row no-gutters" >
        <div class="col-md-4">
            <a href="" id="image_link" target="_blank">
                <img src="" alt="img-here" class="card-img" id="post_image">
            </a>
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h2 class="card-title">
                    <a href="#" id="post_title" target="_blank">Post title</a>
                </h2>
                <div class="card-text" style="font-size: 120%; color: #4c4c4f;">
                    <div>
                        <span id="director-tag-container">Directors: </span>
                    </div>
                    <div>
                        <span id="actor-tag-container">Actors:</span>
                    </div>
                </div>
                <p class="card-text"> <small class="text-muted" id="post-description">Tuy nhiên, tiền đạo người Việt Nam không thể hiện được
                        nhiều (thậm chí không tung nổi cú dứt điểm).</small></p>
                <p><a class="btn btn-primary btn-log" href="#" role="button">Read more</a></p>
            </div>

        </div>

    </div>
    <hr>
</template>
