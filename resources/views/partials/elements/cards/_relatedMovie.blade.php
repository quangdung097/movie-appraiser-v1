<div class="card-group">
    @for($i = 0; $i < count($movie['related']); $i++)
        <div class="card related-movie-card">
            {{--<img class="card-img-top" src="{{$movie['related'][$i]->poster_image_url}}" alt="Card image cap">--}}
            <div class="card-body">
                <h5 class="card-title">
                    <a href="/movies/{{$movie['related'][$i]->id}}" class="user-link">{{$movie['related'][$i]->title}}</a>
                </h5>
                <p class="card-text movie-card-description">{{$movie['related'][$i]->description}}</p>
                <p class="card-text" style="padding-bottom: 5px;"><small class="text-muted">Cập nhật: {{$movie['related'][$i]->updated_at}}</small></p>
            </div>
        </div>
    @endfor

    <script src="/js/pages/post/loadRelated.js"></script>
</div>