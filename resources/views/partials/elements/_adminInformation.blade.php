<div class="row mb-2">
    <?php
        $lastPost = $user['extends']['lastPost'][0];
    ?>
    <div class="col-md-6">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary">Bài viết mới</strong>
                <h3 class="mb-0">{{$lastPost->title}}</h3>
                <div class="mb-1 text-muted">{{date('F d', strtotime($lastPost->created_at))}}</div>
                <p class="card-text mb-auto">Phim: {{$lastPost->movie->title}}</p>
                <p class="card-text mb-auto">
                    Có
                    <b>
                        {{count($lastPost->comments)}}
                    </b>
                     bình luận.
                </p>
                <a href="/posts/{{$lastPost->id}}" target="_blank" class="stretched-link">Đọc tiếp</a>
            </div>
            <div class="col-auto d-none d-lg-block">
                <svg class="bd-placeholder-img" width="200" height="250"
                     xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title>
                        Placeholder
                    </title>
                    <rect width="100%" height="100%" fill="#55595c"/>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em" font-size="150px">
                        🦀
                    </text>
                </svg>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-success">Thống kê</strong>
                <h3 class="mb-0">Trong tháng này:</h3>
                <br>
                <div class="mb-1 text-muted">{{date('F')}} 1 - {{date('F d')}}</div>
                <p class="mb-auto">Có
                    <b>
                        {{$user['extends']['newUsers']['count']}}
                    </b>
                    người dùng mới
                    <br>
                    Có
                    <b>
                        {{$user['extends']['newPosts']['count']}}
                    </b>
                    bài viết mới
                </p>
                <a href="/user/profile#navbar-admin-content" class="stretched-link">Trang quản trị</a>
            </div>
            <div class="col-auto d-none d-lg-block">
                <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title>
                        Placeholder
                    </title>
                    <rect width="100%" height="100%" fill="#55595c"/>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em" font-size="150px">
                        🌗
                    </text>
                </svg>
            </div>
        </div>
    </div>
</div>