{{--Boostrapt Navbar--}}
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Movie Blog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <form class="d-inline my-2 my-lg-2" id="movieNb-search-form">
            <input class="form-control nav-search" type="search" placeholder="Tìm kiếm ..." aria-label="Search">
            {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
        </form>

        <ul class="navbar-nav mr-auto" id="movieNb-right-menu">
            {{--<li class="nav-item movieNb-item @yield('homeActive')">--}}
                {{--<a class="nav-link" href="/">Trang chủ<span class="sr-only">(current)</span></a>--}}
            {{--</li>--}}
            {{--<li class="nav-item movieNb-item @yield('postActive')">--}}
                {{--<a class="nav-link" href="/posts">Bài viết</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item movieNb-item @yield('userActive')">--}}
                {{--<a class="nav-link" href="/users">Người sử dụng</a>--}}
            {{--</li>--}}
        </ul>

        <ul class="navbar-nav ml-auto" id="navBar-dropdown-container">
            @yield('dropDownContent')
        </ul>

    </div>
</nav>

<link rel="stylesheet" href="/css/elements/navBar/navBar.css">

<script src="/js/pages/user/search.js"></script>
{{--End BootstraptNavbar--}}