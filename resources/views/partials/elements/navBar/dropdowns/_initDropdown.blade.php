@extends('partials.elements.navBar._navBar')
@section('dropDownContent')
        <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                Tài khoản
                </a>
                <div class="dropdown-menu dropdown-menu-right"  aria-labelledby="navbarDropdown">
                        @if(Route::has('api/v1/users/login'))
                                <a class="dropdown-item" href="/login" >Đăng nhập</a>
                        @endif
                        @if(Route::has('api/v1/users/register'))
                                <a class="dropdown-item" href="/register">Đăng ký tài khoản</a>
                        @endif
                </div>
        </li>
@endsection
