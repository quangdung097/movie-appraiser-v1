@extends('partials.elements.navBar._navBar')
@section('dropDownContent')
    <li>
        <div class="inset">
            <img src="{{$user->image->image_url}}" alt="avatar">
        </div>
    </li>
    <li class="nav-item dropdown user-dropdown" id="user-dropdown-{{$user->id}}">

        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{$user->name}}
        </a>

        <div class="dropdown-menu dropdown-menu-right"  aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/user/profile" >Quản lý tài khoản</a>

            <div class="dropdown-divider"></div>

            <a class="dropdown-item" id="nb-logout" href="#">Đăng xuất</a>
        </div>
    </li>
    <script type="text/javascript" src="/js/request/post/login/logout.js"></script>

    @include('partials.modals.loginModal')
@endsection


