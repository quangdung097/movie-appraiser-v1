<template class="account-dropdown">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >

        </a>
        <div class="dropdown-menu dropdown-menu-right"  aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/" >Quản lý tài khoản</a>

            <div class="dropdown-divider"></div>

            <a class="dropdown-item" id="nb-logout" href="#">Đăng xuất</a>
        </div>
    </li>

</template>

<script type="text/javascript" src="/js/request/pages/login/logout.js"></script>
