<div class="bd-example">
    <?php
    $posts = $extends['posts'];
    ?>
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            @for($i = 0; $i < 3; $i++)
                @if($i == 1)
                    <div class="carousel-item active" id="post-carousel-{{$posts[$i]['id']}}">
                        <img src="{{$posts[$i]['images'][0]['image_url']}}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 style="background-color:rgba(24,5,19,0.5);">{{$posts[$i]['title']}}</h2>
                        </div>
                    </div>
                @else
                    <div class="carousel-item" id="post-carousel-{{$posts[$i]['id']}}">
                        <img src="{{$posts[$i]['images'][0]['image_url']}}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 style="background-color:rgba(24,5,19,0.5);">{{$posts[$i]['title']}}</h2>
                        </div>
                    </div>
                @endif
            @endfor
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <script src="/js/pages/home/carousel.js"></script>
</div>