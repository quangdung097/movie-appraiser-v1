<footer class="py-5 bg-dark" style="max-height: 220px;">
    {{--<div class="container">--}}
        {{--<p class="m-0 text-center text-white">Best Movie Validation Website</p>--}}
    {{--</div>--}}
    <!-- Footer Links -->
    <div style="margin-top: -15px">
        <div class="container text-center text-md-left">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-7 mx-auto">
                    <ul class="list-unstyled white-text">
                        <li>
                            <small>
                                <h5>
                                    Hệ thống đánh giá phim MovieBlog<br>
                                </h5>
                                Hệ thống được xây dựng như là thành quả của của môn Project II - IT3920 <br>
                                Viện Công nghệ thông tin và Truyền thông - Trường Đại học Bách Khoa Hà Nội <br>
                                Địa chỉ: <b>Số 1 - Đường Đại Cồ Việt - Hai Bà Trưng - Hà Nội</b><br>
                                Điện thoại: <b>03-6856-6003</b> - Email: <b>phanquangdng@gmail.com</b><br>
                            </small>


                        </li>
                    </ul>

                </div>
                <!-- Grid column -->
                <div class="col-md-3 offset-2 mx-auto white-text">
                    <ul class="list-unstyled">
                        <li>
                            <small>
                                <h5>
                                    Thông tin<br>
                                </h5>
                                Sinh viên: Phan Quang Dũng <br>
                                MSSV: 20150703 <br>
                                Lớp: CNTT 2-4 - Viện CNTT&TT <br>
                                Trường Đại học Bách Khoa Hà Nội <br>
                            </small>
                        </li>
                    </ul>

                </div>
                <!-- Grid column -->



            </div>

            <div class="row">
                <div class="col-md-8 offset-2">
                    <small class="white-text">
                        <i>
                            Mọi hành động sử dụng nội dung đăng tải trên Hệ thống MovieBlog phải
                            có sự đồng ý bằng văn bản của người sở hữu.
                        </i>
                    </small>
                </div>

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

    </div>
</footer>