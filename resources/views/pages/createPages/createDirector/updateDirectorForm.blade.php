<form>
    <div class="form-row">
        <label for="input-director-name">Tên</label>
        <input class="form-control" id="input-director-name" placeholder="Nhập tên">
    </div>
    <div class="form-group">
        <label for="inputAddress">Ngày sinh</label>
        @include('partials.elements.timePicker.datePicker')

    </div>
    <div class="form-group">
        <label for="country-update-option">Quốc gia</label>
        <select id="country-update-option" class="form-control"
                onfocus='this.size=5;' onblur='this.size=1;' onchange='this.size=1; this.blur();'>
            @foreach($extends['countries'] as $country)
                <option id="country-option-{{$country->id}}">{{$country->country_name}}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary modal-hide" style="float:top">Tạo mới</button>
</form>