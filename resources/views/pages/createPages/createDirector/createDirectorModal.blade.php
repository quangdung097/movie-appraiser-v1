@extends('pages.createPages.createModal')

@section('createModalTitle', 'Thêm mới Đạo diễn')
@section('createModalCreateButtonName', 'Thêm mới')

@section('createModalBody')
    @include('pages.createPages.createDirector.createDirectorForm')
@endsection