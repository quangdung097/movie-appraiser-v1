@extends('pages.createPages.updateModal')

@section('updateModalTitle', 'Cập nhật Đạo diễn')
@section('updateModalCreateButtonName', 'Cập nhật')

@section('updateModalBody')
    @include('pages.createPages.createDirector.updateDirectorForm')
@endsection

@section('modalScript')
    {{--<script src="/js/pages/admin/actors/loadUpdateActor"></script>--}}
@endsection