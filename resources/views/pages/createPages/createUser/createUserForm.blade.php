<form>
    <div class="form-row">
        <label for="input-account-name">Tên</label>
        <input class="form-control" id="input-account-name" placeholder="Nhập tên">
    </div>

    <div class="form-row">
        <label for="input-account-email">Email</label>
        <input class="form-control" id="input-account-email" placeholder="Nhập email">
    </div>

    <div class="form-group">
        <label for="user-email">Mật khẩu </label>
        <input type="password" class="form-control" id="account-password-input" placeholder="Nhập mật khẩu ">
    </div>
    <div class="form-group">
        <label for="user-email">Xác nhận mật khẩu</label>
        <input type="password" class="form-control" id="confirm-password-input" placeholder="Nhập lại mật khẩu">
    </div>

    <button type="submit" class="btn btn-primary modal-hide" style="float:top">Tạo mới</button>
</form>