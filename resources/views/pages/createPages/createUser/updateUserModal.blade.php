@extends('pages.createPages.updateModal')

@section('updateModalTitle', 'Cập nhật Tài khoản')
@section('updateModalCreateButtonName', 'Cập nhật')

@section('updateModalBody')
    @include('pages.createPages.createUser.updateUserForm')
@endsection

@section('modalScript')
    {{--<script src="/js/pages/admin/actors/loadUpdateActor"></script>--}}
@endsection