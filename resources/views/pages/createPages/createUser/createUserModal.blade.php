@extends('pages.createPages.createModal')

@section('createModalTitle', 'Tạo tài khoản mới')
@section('createModalCreateButtonName', 'Thêm mới')

@section('createModalBody')
    @include('pages.createPages.createUser.createUserForm')
@overwrite