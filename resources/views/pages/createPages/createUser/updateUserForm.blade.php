<form>
    <div class="form-row">
        <label for="input-account-name">Tên</label>
        <input class="form-control" id="update-account-name" placeholder="Nhập tên">
    </div>

    <div class="form-row">
        <label for="input-account-email">Email</label>
        <input class="form-control" id="update-account-email" placeholder="Nhập email">
    </div>

    <button type="submit" class="btn btn-primary modal-hide" style="float:top">Tạo mới</button>
</form>