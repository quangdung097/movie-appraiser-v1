@extends('layouts.mains.accountMain')
@section('content')
    <div class="container">
        <h2>
            @yield('modifyTitle')
        </h2>
        <hr>
        <h4>
            Tiêu đề
        </h4>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Tiêu đề</span>
            </div>
            <input type="text" id="title-input" class="form-control" aria-label="Sizing example input"
                   aria-describedby="inputGroup-sizing-default" title="title-input">
        </div>
        <form>
            <div class="form-group">
                <h4>
                    <label for="exampleFormControlFile1">Hình ảnh</label>
                </h4>
                <input type="file" class="form-control-file" id="new-file-input">
            </div>
        </form>
        <div class="form-group">
            <h4>
                <label for="post-movie-select">
                    Phim
                </label>
            </h4>
            <select class="form-control" id="post-movie-select">
                @if(isset($extends['post']))
                    <option id="movie-option-{{$extends['post']->movie->id}}"
                            selected>{{$extends['post']->movie->title}}</option>
                @endif
                @foreach($extends['movies'] as $movie)
                    <option id="movie-option-{{$movie->id}}">{{$movie->title}}</option>
                @endforeach
            </select>
        </div>
        <h4>
            Nội dung bài viết
        </h4>
        <div>
        <textarea name="content" id="editor" title="content-input">
            {{--This is some sample content.--}}
        </textarea>
        </div>
        <br>
        <a class="btn btn-outline-primary" id="post-creator" target="_blank"
           style="width: 15%;float:top;">
            @yield('modifyButton')
        </a>
    </div>
    <br>
@endsection

@section('title', '| Thêm bài viết')

@section('script')
    {{--<script src="/ckeditor/ckeditor.js"></script>--}}
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
    <script src="/js/pages/CKEditor/ckeditorInit.js"></script>
    <script src="/js/display/chosen.jquery.js"></script>
    <script src="/js/display/chosen.proto.js"></script>

    <link rel="stylesheet" href="/css/pages/chosen.css">
    @yield('modifyScript')
@endsection
@include('partials.modals.warningModal')