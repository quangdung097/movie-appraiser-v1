@extends('pages.createPages.modifyPost')
@section('modifyTitle', 'Chỉnh sửa bài viết')
@section('modifyButton', 'Cập nhật')

@section('modifyScript')
    <script src="/js/pages/post/loadUpdatePost.js"></script>
    <script src="/js/request/patch/updatePost.js"></script>
@endsection