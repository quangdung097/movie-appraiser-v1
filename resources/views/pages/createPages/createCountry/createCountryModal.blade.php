@extends('pages.createPages.createModal')

@section('createModalTitle', 'Thêm mới Nước')
@section('createModalCreateButtonName', 'Thêm mới')

@section('createModalBody')
    @include('pages.createPages.createCountry.createCountryForm')
@endsection