@extends('pages.createPages.updateModal')

@section('updateModalTitle', 'Cập nhật Nước')
@section('updateModalCreateButtonName', 'Cập nhật')

@section('updateModalBody')
    @include('pages.createPages.createCountry.updateCountryForm')
@endsection