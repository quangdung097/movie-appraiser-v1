@extends('pages.createPages.modifyPost')
@section('modifyTitle', 'Tạo mới bài viết')
@section('modifyButton', 'Tạo bài viết mới')

@section('modifyScript')
    <script src="/js/request/post/createPost.js"></script>
@endsection