@extends('pages.createPages.updateModal')

@section('updateModalTitle', 'Cập nhật Diễn viên')
@section('updateModalCreateButtonName', 'Cập nhật')

@section('updateModalBody')
    @include('pages.createPages.createActor.updateActorForm')
@endsection

@section('modalScript')
    {{--<script src="/js/pages/admin/actors/loadUpdateActor"></script>--}}
@endsection