@extends('pages.createPages.createModal')

@section('createModalTitle', 'Thêm mới Diễn viên')
@section('createModalCreateButtonName', 'Thêm mới')

@section('createModalBody')
    @include('pages.createPages.createActor.createActorForm')
@overwrite