@extends('pages.createPages.updateModal')

@section('updateModalTitle', 'Cập nhật Thể loại')
@section('updateModalCreateButtonName', 'Cập nhật')
@section('updateModalBody')
    @include('pages.createPages.createGenre.updateGenreForm')
@endsection