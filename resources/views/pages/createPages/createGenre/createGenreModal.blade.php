@extends('pages.createPages.createModal')

@section('createModalTitle', 'Thêm Thể loại')
@section('createModalCreateButtonName', 'Thêm mới')

@section('createModalBody')
    @include('pages.createPages.createGenre.createGenreForm')
@endsection