@extends('layouts.mains.accountMain')
@section('content')
    <h2>
        Thêm phim mới
    </h2>
    <hr>
    @include('pages.createPages.createMovie.createMovieForm')
    <button type="button" class="btn btn-primary modal-hide" id="movie-creator" style="float:top">Thêm mới</button>
@endsection
@section('title', '| Thêm phim mới')

@section('script')
    <script src="/js/request/post/createMovie.js"></script>
@endsection