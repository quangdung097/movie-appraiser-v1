<div style="min-height: 400px">

    <form>
        <div class="form-group">
            <label for="input-movie-title">Tên phim</label>
            <input type="text" class="form-control" id="input-movie-title" placeholder="Nhập tên phim"
            @if(isset($extends['movie']))
                value="{{$extends['movie']->title}}"
            @endif
            >
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="input-movie-directors">Đạo diễn</label>

                <select multiple class="form-control" id="input-movie-directors" data-placeholder="Thêm đạo diễn ...">
                    @if(isset($extends['movie']))
                        @foreach($extends['movie']->directors as $defaultDirector)
                            <option selected
                                    id="director-option-{{$defaultDirector->id}}">{{$defaultDirector->name}}
                            </option>
                        @endforeach
                    @endif
                    @foreach($extends['directors'] as $director)
                        <option id="director-option-{{$director->id}}">{{$director->name}}</option>
                    @endforeach
                </select>
                <datalist id="director-suggestions">
                </datalist>
            </div>
            <div class="form-group col-md-6">
                <label for="input-movie-actors">Diễn viên</label>
                <select multiple class="form-control" id="input-movie-actors" data-placeholder="Thêm diễn viên...">
                    @if(isset($extends['movie']))
                        @foreach($extends['movie']->actors as $defaultActor)
                            <option selected id="actor-option-{{$defaultActor->id}}">{{$defaultActor->name}}</option>
                        @endforeach
                    @endif
                    @foreach($extends['actors'] as $actor)
                        <option id="actor-option-{{$actor->id}}">{{$actor->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="input-movie-image">Poster ảnh</label>
            <input type="text" class="form-control" id="input-movie-image" placeholder="Nhập url"
            @if(isset($extends['movie']))
               value="{{$extends['movie']->poster_image_url}}"
            @endif
            >
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="input-movie-genres">Thể loại</label>
                <select multiple class="form-control" id="input-movie-genres" data-placeholder="Thêm thể loại...">\
                    @if(isset($extends['movie']))
                        @foreach($extends['movie']->genres as $defaultGenre)
                            <option selected
                                    id="genre-option-{{$defaultGenre->id}}">{{$defaultGenre->genre_name}}</option>
                        @endforeach
                    @endif
                    @foreach($extends['genres'] as $genre)
                        <option id="genre-option-{{$genre->id}}">{{$genre->genre_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="input-movie-country">Quốc gia</label>
                <select multiple class="form-control" id="input-movie-country" data-placeholder="Chọn quốc gia...">
                    @if(isset($extends['movie']))
                        <option selected id="country-option-{{$extends['movie']->country->id}}">
                            {{$extends['movie']->country->country_name}}
                        </option>
                    @endif
                    @foreach($extends['countries'] as $country)
                        <option id="country-option-{{$country->id}}">{{$country->country_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="input-movie-budget">Đầu tư</label>
                <input type="text" class="form-control" id="input-movie-budget"
                   @if(isset($extends['movie']))
                   value="{{$extends['movie']->budget}}"
                    @endif
                >
            </div>
        </div>
        <div class="form-group">
            <label for="input-movie-trailer">Trailer ID</label>
            <input type="text" class="form-control" id="input-movie-trailer" placeholder="Nhập id video"
            @if(isset($extends['movie']))
               value="{{$extends['movie']->trailer_url}}"
            @endif
            >
        </div>
        <hr>
        <div class="form-group">
            <label for="input-movie-description">Mô tả</label>
            <textarea class="form-control" id="input-movie-description"
                      rows="3">@if(isset($extends['movie'])){!! $extends['movie']->description!!}@endif</textarea>
        </div>
        <br>

    </form>


    <script src="/js/display/chosen.jquery.js"></script>
    <script src="/js/display/chosen.proto.js"></script>

    <link rel="stylesheet" href="/css/pages/chosen.css">
    @include('partials.modals.warningModal')
</div>



