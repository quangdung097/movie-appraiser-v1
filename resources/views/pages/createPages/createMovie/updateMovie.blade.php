@extends('layouts.mains.accountMain')
@section('content')
    <h2>
        Cập nhật thông tin phim {{$extends['movie']->title}}.
    </h2>
    <hr>
    @include('pages.createPages.createMovie.createMovieForm')
    <button type="button" class="btn btn-primary modal-hide" id="movie-editor" style="float:top">Cập nhật</button>
@endsection
@section('title', '| Thêm phim mới')

@section('script')
    <script src="/js/pages/admin/movies/loadUpdateMovie.js"></script>
@endsection