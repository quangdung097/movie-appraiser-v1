@extends('layouts.mains.accountMain')
@section('content')
    <?php
        $users = $extends['paginatedAccounts'];
    ?>
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách Tài khoản.
    </h2>

    <a class="btn btn-outline-primary" href="#" data-toggle="modal" data-target="#create-modal"
       style="width: 17%;float: right;">
        Thêm mới Người dùng
    </a>

    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr id="actor-header">
                    <th>#</th>
                    <th>Tên</th>
                    <th>Ngày tham gia</th>
                    <th>Số comment</th>
                    <th>Số đánh giá</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $account)
                    <tr id="user-member-{{$account->id}}" class="user-member">
                        <td>{{$account->id}}</td>
                        <td>{{$account->name}}</td>
                        <td>{{date('d-m-Y', strtotime($account->created_at))}}</td>
                        <td>{{count($account->comments)}}</td>
                        <td>{{count($account->userRates)}}</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$account->id}}" href="#">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$account->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{$users->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')
    @include('pages.createPages.createUser.updateUserModal')
    @include('pages.createPages.createUser.createUserModal')
@endsection()

@section('title', '| User')

@section('userActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteUser.js"></script>
    <script src="/js/request/post/createUser.js"></script>
    <script src="/js/pages/admin/users/loadUpdateUser.js"></script>
@endsection()