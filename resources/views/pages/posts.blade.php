@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách bài viết
    </h2>
    <a class="btn btn-outline-primary" href="/admin/manager/posts/create" target="_blank"
       style="width: 17%;float: right;">
        Thêm mới bài viết
    </a>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Tiêu đề</th>
                    <th>Phim</th>
                    <th>Điểm số</th>
                    <th>Số bình luận</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr id="post-member-{{$post->id}}">
                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->movie->title}}</td>
                        <td>dolor</td>
                        <td>sit</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$post->id}}"
                               href="/admin/manager/posts/update?id={{$post->id}}" target="_blank">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$post->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')

@endsection()

@section('title', '| Post')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deletePost.js"></script>
@endsection()