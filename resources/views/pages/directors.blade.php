@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách Đạo diễn
    </h2>
    <a class="btn btn-outline-primary" href="#" data-toggle="modal" data-target="#create-modal"
       style="width: 17%;float: right;">
        Thêm mới Đạo diễn
    </a>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Tên</th>
                    <th>DOB</th>
                    <th>Đất nước</th>
                    <th>Phim</th>
                </tr>
                </thead>
                <tbody>
                @foreach($directors as $director)
                    <tr id="director-member-{{$director->id}}" class="director-member">
                        <td>{{$director->id}}</td>
                        <td>{{$director->name}}</td>
                        <td>{{$director->DOB}}</td>
                        <td>{{$director->country->country_name}}</td>
                        <td>sit</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$director->id}}" href="#">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$director->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $directors->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')
    @include('pages.createPages.createDirector.createDirectorModal')
    @include('pages.createPages.createDirector.updateDirectorModal')

@endsection()

@section('title', '| Quản trị đạo diễn')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteDirector.js"></script>
    <script src="/js/request/post/createDirector.js"></script>
    <script src="/js/pages/admin/directors/loadUpdateDirector.js"></script>
@endsection()