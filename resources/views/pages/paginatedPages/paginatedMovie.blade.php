@section('content')
    <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
            @foreach($extends['genres'] as $genre)
                <a class="p-2 text-muted" href="/genres/{{$genre->id}}">{{$genre->genre_name}}</a>
            @endforeach
        </nav>
    </div>

    @include('partials._carousel')

    <hr>

    <div class="row">
        <div class="col-md-8" id="post_column">
            @include('partials.elements.tag.templates._tagPost')

            @foreach ($movies as $movie)
                @include('partials.elements.paginatedElements._paginatedMoviePost')
            @endforeach
        </div>

        <div class="col-md-3 offset-md-1">
            @include('partials.elements._news')

        </div>
        <div class="col-md-8">
            {{ $movies->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    {{--end of .row--}}

@endsection

@section('title', '| Home')

@section('homeActive', 'active')

@section('stylesheet')
    <link rel="stylesheet" href="/css/elements/postElements/postElement.css">
    <link rel="stylesheet" href="/css/elements/postElements/postTag.css">
@endsection
