@section('content')
    <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
            @foreach($extends['genres'] as $genre)
                <a class="p-2 text-muted" href="/genres/{{$genre->id}}">{{$genre->genre_name}}</a>
            @endforeach
        </nav>
    </div>

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--begin of jumbotron--}}
            {{--<div class="jumbotron">--}}
                {{--<h1 class="display-4">Hello, world!</h1>--}}
                {{--<p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra--}}
                    {{--attention to featured content or information.</p>--}}
                {{--<hr class="my-4">--}}
                {{--<p>It uses utility classes for typography and spacing to space content out within the larger--}}
                    {{--container.</p>--}}
                {{--<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>--}}
            {{--</div>--}}
            {{--end of jumbotron--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--end of .row--}}

    @include('partials._carousel')

    <hr>

    <div class="row">
        <div class="col-md-8" id="post_column">
            @include('partials.elements.tag.templates._tagPost')

            @foreach ($posts as $post)
                @include('partials.elements.paginatedElements._paginatedPostPost')
            @endforeach
        </div>

        <div class="col-md-3 offset-md-1">
            @include('partials.elements._news')
        </div>
        <div class="col-md-8">
            {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    {{--end of .row--}}

@endsection

@section('title', '| Home')

@section('homeActive', 'active')

@section('stylesheet')
    <link rel="stylesheet" href="/css/elements/postElements/postElement.css">
    <link rel="stylesheet" href="/css/elements/postElements/postTag.css">
@endsection
