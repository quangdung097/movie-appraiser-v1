@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách diễn viên
    </h2>

    <a class="btn btn-outline-primary" href="#" data-toggle="modal" data-target="#create-modal"
       style="width: 17%;float: right;">
        Thêm mới Diễn viên
    </a>

    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr id="actor-header">
                    <th>#</th>
                    <th>Tên</th>
                    <th>DOB</th>
                    <th>Đất nước</th>
                    <th>Phim</th>
                </tr>
                </thead>
                <tbody>
                @foreach($actors as $actor)
                    <tr id="actor-member-{{$actor->id}}" class="actor-member">
                        <td>{{$actor->id}}</td>
                        <td>{{$actor->name}}</td>
                        <td>{{$actor->DOB}}</td>
                        <td>{{$actor->country->country_name}}</td>
                        <td>sit</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$actor->id}}" href="#">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$actor->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $actors->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')
    @include('pages.createPages.createActor.updateActorModal')
    @include('pages.createPages.createActor.createActorModal')


@endsection()

@section('title', '| Quản trị diễn viên')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteActor.js"></script>
    <script src="/js/request/post/createActor.js"></script>
    <script src="/js/pages/admin/actors/loadUpdateActor.js"></script>
@endsection()