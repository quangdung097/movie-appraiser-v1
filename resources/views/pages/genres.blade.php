@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách Thể loại
    </h2>
    <a class="btn btn-outline-primary" href="#" data-toggle="modal" data-target="#create-modal"
       style="width: 17%;float: right;">
        Thêm mới Thể loại
    </a>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thể loại</th>
                    <th>Số phim</th>
                </tr>
                </thead>
                <tbody>
                @foreach($genres as $genre)
                    <tr id="genre-member-{{$genre->id}}" class="genre-member">
                        <td>{{$genre->id}}</td>
                        <td>{{$genre->genre_name}}</td>
                        <td>{{count($genre->movies)}}</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$genre->id}}" href="#">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$genre->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $genres->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')
    @include('pages.createPages.createGenre.createGenreModal')
    @include('pages.createPages.createGenre.updateGenreModal')

@endsection()

@section('title', '| Quản trị Thể loại')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteGenre.js"></script>
    <script src="/js/request/post/createGenre.js"></script>
    <script src="/js/pages/admin/genres/loadUpdateGenre.js"></script>
@endsection()