@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách Đất nước
    </h2>
    <a class="btn btn-outline-primary" href="#" data-toggle="modal" data-target="#create-modal"
       style="width: 17%;float: right;">
        Thêm mới nước
    </a>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Tên nước</th>
                    <th>Số phim</th>
                    <th>Số diễn viên</th>
                    <th>Số đạo diễn</th>
                </tr>
                </thead>
                <tbody>
                @foreach($countries as $country)
                    <tr id="country-member-{{$country->id}}" class="country-member">
                        <td>{{$country->id}}</td>
                        <td>{{$country->country_name}}</td>
                        <td>{{count($country->movies)}}</td>
                        <td>{{count($country->actors)}}</td>
                        <td>{{count($country->directors)}}</td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$country->id}}" href="#">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$country->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $countries->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')
    @include('pages.createPages.createCountry.createCountryModal')
    @include('pages.createPages.createCountry.updateCountryModal')

@endsection()

@section('title', '| Quản trị Đất nước')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteCountry.js"></script>
    <script src="/js/request/post/createCountry.js"></script>
    <script src="/js/pages/admin/countries/loadUpdateCountry.js"></script>
@endsection()