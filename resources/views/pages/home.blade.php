@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            {{--begin of jumbotron--}}
            <div class="jumbotron">
                <h1 class="display-4">Hello, world!</h1>
                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra
                    attention to featured content or information.</p>
                <hr class="my-4">
                <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
            </div>
            {{--end of jumbotron--}}
        </div>
    </div>
    {{--end of .row--}}

    <div class="row">
        <div class="col-md-8" id="post_column">
            @include('partials.elements.tag.templates._tagPost')
            @include('partials.elements._post')
        </div>

        <div class="col-md-3 offset-md-1" >
            @for($i = 0; $i < 5; $i ++)
                @include('partials.elements._news')
            @endfor
        </div>
        @include('partials._pagination')
    </div>

    {{--end of .row--}}

@endsection

@section('title', '| Home')

@section('homeActive', 'active')

@section('script')
    <script src="/js/request/pages/home/homeLoader.js"></script>
@endsection
