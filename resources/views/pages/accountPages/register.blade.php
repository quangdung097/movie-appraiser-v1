@extends('layouts.mains.initialMain')

@section('content')
    <div class="container login-container" style="max-height: 450px;">
        <div class="row">
            <div class="col-md-6 offset-md-3 login-form-1">
                <h3>Đăng ký</h3>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nhập tên" id="register_name"/>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nhập Email" id="register_email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Nhập mật khẩu" id="register_password"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Xác nhận mật khẩu" id="register_c_password"/>
                    </div>
                    <div class="form-group">
                        <input type="button" class="btnSubmit mx-auto d-block" value="Đăng ký" id="btn_login"/>
                    </div>
                    <div class="form-group" style="text-align: center">
                        <a href="/login">Đã có tài khoản?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link rel="stylesheet"  href="/css/pages/login.css">
@endsection

@section('script')
    <script src="/js/request/post/register/register.js"></script>
@endsection

@section('title', '| Register')