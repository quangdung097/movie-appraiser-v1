@extends('layouts.mains.accountMain')
@section('content')
    <div class="row no-gutters">
        <div class="col-md-3">
            <img src="{{$user->image->image_url}}"
                 class="card-img" id="big-avatar" alt="hello">
        </div>
        <div class="col-md-8 card mb-3 ">
            <div class="card-body">
                <h5 class="card-title">{{$user->name}}</h5>
                <p class="card-text">
                    Vai trò: <b>{{$user->role->role_type}}</b> - Email: <b>{{$user->username}}</b>
                </p>
                <p class="card-text">
                    <small class="text-muted">
                        Số comment: {{count($user->comments)}} - Số lượt đánh giá phim: {{count($user->userRates)}}
                    </small>
                </p>

                <div>
                    <form>
                        <button type="button" class="btn btn-primary" id="change-name-btn"
                                href="#" data-toggle="modal" data-target="#update-name-modal"
                                style="float:left; margin-right: 5%">
                            Cập nhật
                        </button>
                        @include('partials.modals.updateName')
                        <button type="button" class="btn btn-primary"
                                href="#" data-toggle="modal" data-target="#password-modal"
                                style="float:left">
                            Đổi mật khẩu
                        </button>
                        @include('partials.modals.changePasswordModal')
                        @include('partials.modals.warningModal')

                    </form>
                </div>
            </div>
        </div>

        @if(strcasecmp($user->role->role_type, 'admin') == 0)
            <div class="col-md-8 card mb-3 offset-3">
                <div class="card-body">
                    <h5 class="card-title">Quản trị hệ thống</h5>
                    @include('pages.accountPages._adminNavBar')
                </div>
            </div>
        @endif
    </div>

    <div class="col-md-8 mb-3 offset-3 card" style="min-height:320px">
        <div class="card-body">
            @if(count($extends['recentComments']) == 0 && count($extends['recentVotes']) == 0)
                <p class="text-muted" style="text-align: center">
                    Chưa có đánh giá hay bình luận nào
                </p>
            @else
                <div>
                    <h4>Bình luận gần đây</h4>
                    <hr>
                    @if(count($extends['recentComments']) == 0 )
                        <p class="text-muted" style="text-align: center">
                            Chưa có bình luận nào
                        </p>
                    @endif
                    @foreach($extends['recentComments'] as $comment)
                        <div class="card comment-id{{$comment->post->id}}">
                            <div class="card-body">
                                <p>
                                    Bình luận ở bài viết:
                                    <a href="/posts/{{$comment->post->id}}#comment-member-{{$comment->id}}"
                                       class="user-link">{{$comment->post->title}}</a>
                                </p>
                                <hr>
                                Nội dung: <i>"{{$comment->content}}"</i>
                                <br>
                                <small class="text-muted">Vào
                                    lúc {{date('h:i a d-m-Y',strtotime($comment->created_at))}}</small>
                            </div>
                        </div>

                    @endforeach
                </div>

                <hr style="border-top: 1px solid #83a0ab">

                <div>
                    <h4>Đánh giá gần đây</h4>
                    <hr>
                    @if(count($extends['recentVotes']) == 0 )
                        <p class="text-muted" style="text-align: center">
                            Chưa có đánh giá nào
                        </p>
                    @endif
                    <div class="card-group">
                        @foreach($extends['recentVotes'] as $vote)
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="card-title">{{$vote->rate}} ★</h2>
                                    <p class="card-text">
                                        <small class="text-muted">
                                            ở phim <a href="/movies/{{$vote->movie->id}}#rate-title-{{$vote->id}}"
                                                      class="user-link">
                                                {{$vote->movie->title}}
                                            </a>
                                        </small>
                                    </p>
                                    <p class="card-text">
                                        <small class="text-muted">Vào
                                            lúc {{date('h:i a d-m-Y',strtotime($vote->created_at))}}</small>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>

    </div>
@endsection



@section('script')
    <script src="/js/request/patch/changePassword.js"></script>
    <script src="/js/pages/user/loadUpdateUserName.js"></script>
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="/css/pages/profile.css">
@endsection

@section('title', '| Thông tin người dùng')