<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #ffffff">
    <div class="collapse navbar-collapse" id="navbar-admin-content">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active admin-nav-item">
                <a class="nav-link" href="/admin/manager/movies">Phim</a>
            </li>
            <li class="nav-item active admin-nav-item">
                <a class="nav-link" href="/admin/manager/posts">Bài viết</a>
            </li>

            <li class="nav-item active admin-nav-item">
                <a class="nav-link" href="/admin/manager/users">Người dùng</a>
            </li>

            <li class="nav-item dropdown active admin-nav-item">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Khác
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/admin/manager/genres" target="_blank">Thể loại</a>
                    <a class="dropdown-item" href="/admin/manager/actors" target="_blank">Diễn viên</a>
                    <a class="dropdown-item" href="/admin/manager/directors" target="_blank">Đạo diễn</a>
                    <a class="dropdown-item" href="/admin/manager/countries" target="_blank">Quốc gia</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<link rel="stylesheet" href="/css/elements/navBar/adminNavBar.css">

