@extends('layouts.mains.initialMain')

@section('content')
    <div class="container login-container" style="max-height: 465px;">
        <div class="row">
            <div class="col-md-6 offset-md-3 login-form-1">
                <h3>Đăng nhập</h3>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email của bạn" value="" id="login_email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mật khẩu" value="" id="login_password"/>
                    </div>
                    <div class="form-group">
                        <input type="button" class="btnSubmit mx-auto d-block"  value="Đăng nhập" id="btn_login"
                               data-toggle="modal"/>
                    </div>
                    <div class="form-group" style="text-align: center">
                        <a href="/register">Tạo tài khoản mới?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@include('partials.modals.loginModal')

@section('stylesheet')
    <link rel="stylesheet"  href="/css/pages/login.css">
@endsection

@section('script')
    <script src="/js/request/post/login/login.js"></script>
@endsection

@section('title', '| Login')