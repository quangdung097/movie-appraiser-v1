@section('content')
    <div class="search-body">
        <h4>Kết quả tìm kiếm: Đao diễn</h4>
        <div class="row">
            <div class="col-md-6">
                <small class="text-muted">
                    Tìm thấy {{count($directors)}} kết quả.
                </small>
            </div>
            <div class="col-md-3 offset-3">
                <small>
                    <a href="/home/search/movies" class="user-link text-muted search-addition">Tìm kiếm Phim?</a>&nbsp
                    <a href="/home/search/actors" class="user-link text-muted search-addition">Tìm kiếm Diễn viên?</a>
                </small>
            </div>
        </div>

        <hr>
        @if(count($directors) != 0)
            @foreach($directors as $director)
                <div class="card-body round-corner">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">
                                <a href="/directors/{{$director->id}}" class="user-link">
                                    {{$director->name}}
                                </a>
                            </h5>
                        </div>
                    </div>
                    <p class="card-text">
                        <small class="text-muted">
                            {{$director->country->country_name}}. <br>
                            <b>
                                Sửa lần cuối: {{$director->updated_at}}
                            </b>
                        </small>
                    </p>
                </div>
                <br>
            @endforeach

        @else
            <h5 class="search-message">
                Không tìm thấy kết quả nào !!!
            </h5>
        @endif
    </div>

    <link rel="stylesheet" href="/css/pages/searchResult.css">
@endsection