@section('content')
    <div class="search-body">
        <h4>Kết quả tìm kiếm: Phim</h4>
        <div class="row">
            <div class="col-md-6">
                <small class="text-muted">
                    Tìm thấy {{count($movies)}} kết quả.
                </small>
            </div>
            <div class="col-md-3 offset-3">
                <small>
                    <a href="/home/search/actors" class="user-link text-muted search-addition">Tìm kiếm Diễn viên?</a>&nbsp
                    <a href="/home/search/directors" class="user-link text-muted search-addition">Tìm kiếm Đạo diễn?</a>
                </small>
            </div>
        </div>

        <hr>
        @if(count($movies) != 0)
            @foreach($movies as $movie)
                <div class="card-body round-corner">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">
                                <a href="/movies/{{$movie->id}}" class="user-link">
                                    {{$movie->title}}
                                </a>
                            </h5>
                        </div>
                    </div>
                    <p class="card-text">
                        <small class="text-muted">
                            {{$movie->description}}. <br>
                            <b>
                                Sửa lần cuối: {{$movie->updated_at}}
                            </b>
                        </small>
                    </p>
                </div>
                <br>
            @endforeach

        @else
            <h5 class="search-message">
                Không tìm thấy kết quả nào !!!
            </h5>
        @endif
    </div>

    <link rel="stylesheet" href="/css/pages/searchResult.css">
@endsection