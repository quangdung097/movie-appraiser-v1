@section('content')
    <div class="search-body">
        <h4>Kết quả tìm kiếm: <b>{{$extends['query']}}</b></h4>
        <div class="row">
            <div class="col-md-6">
                <small class="text-muted">
                    Tìm thấy <b>
                        {{count($articles)}}
                    </b>
                    kết quả.
                </small>
            </div>
            <div class="col-md-3 offset-3">
                <small>
                    @if($extends['checkpoint'])
                        <a href="/home/search/articles/headline" class="user-link text-muted search-addition">Tin tức mới</a>&nbsp;
                        <a href="/home/search/articles/general" class="user-link text-muted search-addition disabled">Tìm kiếm mở rộng</a>
                    @else
                        <a href="/home/search/articles/headline" class="user-link text-muted search-addition disabled">Tin tức mới</a>&nbsp;
                        <a href="/home/search/articles/general" class="user-link text-muted search-addition">Tìm kiếm mở rộng</a>
                    @endif
                </small>
            </div>
        </div>

        <hr>
        @if(count($articles) != 0)
            @foreach($articles as $article)
                <div class="card-body round-corner">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">
                                <a href="{{$article['url']}}" class="user-link" target="_blank">
                                    {{$article['title']}}
                                </a>
                            </h5>
                        </div>
                    </div>
                    <p class="card-text">
                        <small class="text-muted">
                            {{$article['description']}}. <br>
                            <b>
                                Ngày phát hành: {{date('d/m/Y',strtotime($article['publishedAt']))}}
                            </b>
                        </small>
                    </p>
                </div>
                <br>
            @endforeach

        @else
            <h5 class="search-message">
                Không tìm thấy kết quả nào !!!
            </h5>
        @endif
    </div>

    <link rel="stylesheet" href="/css/pages/searchResult.css">
    <link rel="stylesheet" href="/css/pages/articleSearchResult.css">
@endsection