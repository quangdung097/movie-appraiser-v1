@extends('layouts.mains.accountMain')
@section('content')
    <br>
    @include('partials.elements._adminInformation')

    <h2 style="width: 80%; float: left;">
        Danh sách diễn viên
    </h2>

    <a class="btn btn-outline-primary" href="/admin/manager/movies/create"
       style="width: 17%;float: right;" target="_blank">
        Thêm mới Phim
    </a>

    <div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr id="actor-header">
                    <th>#</th>
                    <th>Tên</th>
                    <th>Thể loại</th>
                    <th>Đạo diễn</th>
                    <th>Diễn viên</th>
                    <th>Đất nước</th>
                </tr>
                </thead>
                <tbody>
                @foreach($movies as $movie)
                    <tr id="movie-member-{{$movie->id}}" class="movie-member">
                        <td>{{$movie->id}}</td>
                        <td>{{$movie->title}}</td>
                        <td>
                            @foreach($movie->genres as $genre)
                                <a href="/genres/{{$genre->id}}" class="user-tag" target="_blank">{{$genre->genre_name}}</a>
                                <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach($movie->directors as $director)
                                <a href="/directors/{{$director->id}}" class="user-tag" target="_blank">{{$director->name}}</a>
                                <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach($movie->actors as $actor)
                                <a href="/movies/{{$actor->id}}" class="user-tag" target="_blank">{{$actor->name}}</a>
                                <br>
                            @endforeach
                        </td>
                        <td>
                            <a href="/countries/{{$movie->country->id}}" class="user-tag" target="_blank">
                                {{$movie->country->country_name}}
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary edit-btn" id="edit-btn-{{$movie->id}}"
                               href="/admin/manager/movies/update?id={{$movie->id}}" target="_blank">
                                Sửa
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary delete-btn" id="delete-btn-{{$movie->id}}" href="#">
                                Xóa
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            {{ $movies->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    @include('partials.modals.confirmModal')


@endsection()

@section('title', '| Quản trị diễn viên')

@section('postActive', 'active')

@section('script')
    <script src="/js/request/delete/deleteMovie.js"></script>
@endsection()