<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8 post-content" id="post-content-{{$post->id}}">

            <!-- Title -->
            <h1 class="mt-4">{{$post->title}}</h1>

            <!-- Author -->
            <p class="lead">
                by {{$post->user->name}}
            </p>

            <hr>

            <!-- Date/Time -->
            {{--<p>Posted on January 1, 2019 at 12:00 PM</p>--}}
            <p class="text-muted">Đăng lúc: {{date('F d - h:i:s a', strtotime($post->created_at))}}</p>

            <hr>
            <iframe width=100%" height="400px" src="https://www.youtube.com/embed/{{$post->movie->trailer_url}}"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen>
            </iframe>
            <hr>
            <!-- Post Content -->

            <img src="{{$post->images[0]->image_url}}" class="img-responsive" style="width: 100%" alt="poster">
            <hr>
            {!!$post->content !!}
            <hr>

            <!-- Comments Form -->
            <div class="card my-4">
                <h5 class="card-header">Bình luận:</h5>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <textarea class="form-control" id="create-comment-area" rows="3"></textarea>
                        </div>
                        <button type="button" class="btn btn-primary" id="create-comment-btn">Submit</button>
                    </form>
                </div>
            </div>

            <div class="comment-section">
                @if(count($post->comments) == 0)
                    <div class="media mb-4">
                        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Blog Movie Admin</h5>
                            Hãy là người đầu tiên để lại bình luận !!!
                        </div>
                    </div>
                @endif

            <!-- Single Comment -->
                @foreach($post->comments as $comment)
                    <div class="media mb-4 inset" id="comment-member-{{$comment->id}}">
                        <img class="d-flex mr-3 rounded-circle" src="{{$comment->user->image->image_url}}" alt="">
                        <div class="media-body">
                            <h5 class="mt-0" style="width: 600px">{{$comment->user->name}}</h5>
                            <p style="overflow: hidden">
                                {{$comment->content}}
                            </p>
                        </div>
                    </div>
                    <br>
                @endforeach
                <div class="col-md-8">
                    {{$post->comments->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control article-search-input" placeholder="Search for...">
                        <span class="input-group-btn">
                <button class="btn btn-secondary article-search-btn" type="button">Go!</button>
              </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Liên quan</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @foreach($post->movie->genres as $genre)
                                    <li>
                                        <a href="/api/v1/genres/get/{{$genre->id}}">{{$genre->genre_name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="/api/v1/countries/get/{{$post->movie->country->id}}">{{$post->movie->country->country_name}}</a>
                                </li>
                                <li>
                                    <a href="/api/v1/movies/get/{{$post->movie->id}}">{{$post->movie->title}}</a>
                                </li>
                                @foreach($post->movie->directors as $director)
                                    <li>
                                        <a href="/api/v1/directors/get/{{$director->id}}">{{$director->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Tình thiết</h5>
                <div class="card-body">
                    {{$post->movie->description}}
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->
    @include('partials.modals.commentWaringModal')
</div>
<!-- /.container -->
<link href="/css/pages/post.css" rel="stylesheet">
<script src="/js/request/post/createComment.js"></script>
<script src="/js/pages/post/movieSearch.js"></script>

@section('title', '| Bài viết')