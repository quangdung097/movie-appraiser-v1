<div class="card mb-3">
    {{--<img src="..." class="card-img-top" alt="...">--}}
    <div class="card-body">
        <h5 class="card-title">{{$actor->name}}</h5>
        <p class="card-text">
            Diễn viên - Ngày sinh: <b>{{$actor->DOB}}</b> - Quốc gia: <b>{{$actor->country->country_name}}</b>
        </p>
        <p class="card-text"><small class="text-muted">Sửa lần cuối: {{$actor->updated_at}}</small></p>
    </div>
</div>

<hr>
<div style="min-height: 300px;">
    <h3>
        Danh sách các phim của {{$actor->name}}.
    </h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr id="actor-header">
                <th>#</th>
                <th>Phim</th>
                <th>Quốc gia</th>
                <th>Đầu tư</th>
                <th>Đạo diễn</th>
            </tr>
            </thead>
            <tbody>
            @foreach($actor->movies as $movie)
                <tr id="movie-member-{{$movie->id}}" class="movie-member">
                    <td>{{$movie->id}}</td>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->country->country_name}}</td>
                    <td>{{$movie->budget}}</td>
                    <td>
                        @foreach($movie->directors as $director)
                            <a href="/directors/{{$director->id}}" class="user-tag">{{$director->name}}</a>
                            <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        {{$actor->movies->links('vendor.pagination.bootstrap-4') }}
    </div>
</div>

@section('title', '| Diễn viên')