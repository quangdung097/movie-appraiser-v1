<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">{{$director->name}}</h5>
        <p class="card-text">
            Đạo diễn - Ngày sinh: <b>{{$director->DOB}}</b> - Quốc gia: <b>{{$director->country->country_name}}</b>
        </p>
        <p class="card-text"><small class="text-muted">Sửa lần cuối: {{$director->updated_at}}</small></p>
    </div>
</div>
<hr>
<div style="min-height: 300px;">
    <h3>
        Danh sách các phim của {{$director->name}}.
    </h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr id="actor-header">
                <th>#</th>
                <th>Phim</th>
                <th>Quốc gia</th>
                <th>Đầu tư</th>
                <th>Diễn viên</th>
            </tr>
            </thead>
            <tbody>
            @foreach($director->movies as $movie)
                <tr id="movie-member-{{$movie->id}}" class="movie-member">
                    <td>{{$movie->id}}</td>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->country->country_name}}</td>
                    <td>{{$movie->budget}}</td>
                    <td>
                        @foreach($movie->actors as $actor)
                            <a href="/actors/{{$actor->id}}" class="user-tag">{{$actor->name}}</a>
                            <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        {{ $director->movies->links('vendor.pagination.bootstrap-4') }}
    </div>
</div>

@section('title', '| Đạo diễn')