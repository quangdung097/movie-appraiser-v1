<div class="card mb-3">
    {{--<img src="..." class="card-img-top" alt="...">--}}
    <div class="card-body">
        <h5 class="card-title">{{$genre->genre_name}}</h5>
        <p class="card-text">
        Thể loại.
        </p>
        <p class="card-text"><small class="text-muted">Sửa lần cuối: {{$genre->updated_at}}</small></p>
    </div>
</div>

<hr>
<div style="min-height: 300px;">
    <h3>
        Danh sách các phim Thể loại {{$genre->genre_name}}.
    </h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr id="actor-header">
                <th>#</th>
                <th>Phim</th>
                <th>Đất nước</th>
                <th>Đầu tư</th>
                <th>Đạo diễn</th>
            </tr>
            </thead>
            <tbody>
            @foreach($genre->movies as $movie)
                <tr id="movie-member-{{$movie->id}}" class="movie-member">
                    <td>{{$movie->id}}</td>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->country->country_name}}</td>
                    <td>{{$movie->budget}}</td>
                    <td>
                        @foreach($movie->directors as $director)
                            <a href="/directors/{{$director->id}}" class="user-tag">{{$director->name}}</a>
                            <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        {{$genre->movies->links('vendor.pagination.bootstrap-4') }}
    </div>
</div>

@section('title', '| Thể loại')