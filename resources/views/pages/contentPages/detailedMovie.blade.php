<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">
            <div class="card mb-3">
                {{--<img src="..." class="card-img-top" alt="...">--}}
                <div class="row">
                    <div class="col-md-8 float-left">
                        <div class="card-body">
                            <h3 class="card-title movie-title" id="movie-title-{{$movie->id}}">{{$movie->title}}</h3>
                            <p class="card-text">
                                @foreach($movie->genres as $genre)
                                    <a href="/genres/{{$genre->id}}" class="user-tag">{{$genre->genre_name}}</a>
                                @endforeach
                                - Quốc gia:
                                <a href="/countries/{{$movie->country->id}}" class="user-tag">{{$movie->country->country_name}}</a>
                            </p>
                            <p class="card-text">
                                <small class="text-muted">Sửa lần cuối: {{$movie->updated_at}}</small>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 offset-1 float-right" style="text-align: center">
                        <br>
                        @if ($movie['countRate'] != 0)
                            <h1>{{$movie['averageRate']}}/5</h1>
                            <small>Dựa trên {{$movie['countRate']}} đánh giá.</small>
                        @endif

                    </div>
                </div>
            </div>

            <hr>

            <!-- Preview Image -->
            <div class="row media-content">
                <div class="col-lg-4 column">
                    <img class="img-fluid rounded center media-height" src="{{$movie->poster_image_url}}"
                         alt="poster-{{$movie->title}}">
                </div>
                <div class="col-lg-8 column circle">
                    <iframe src="https://www.youtube.com/embed/{{$movie->trailer_url}}"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen class="center media-height">
                    </iframe>
                </div>
            </div>


            <hr>

            <!-- Post Content -->
            <div class="center ">
                <p class="lead">
                    <small>
                        <i>"{{$movie->description}}"</i>
                    </small>
                </p>
                <br>
                <span>
                    <b>Đạo diễn: </b>
                </span>
                    @foreach($movie->directors as $director)
                        <a href="/directors/{{$director->id}}" target="_blank" class="user-tag">
                            {{$director->name}}
                        </a>
                    @endforeach
                    <br>
                    <span>
                    <b>Diễn viên: </b>
                </span>
                @foreach($movie->actors as $actor)
                    <a href="/actors/{{$actor->id}}" target="_blank" class="user-tag">
                        {{$actor->name}}
                    </a>
                @endforeach
                <p>
                    <b>Đầu tư: </b> {{$movie->budget}}
                </p>
                <br>
            </div>

            @include('partials.elements.rateBar._rateBar')

            <hr>

            <h3>Các phim liên quan</h3>
            <div class="card-group">
                @include('partials.elements.cards._relatedMovie')
            </div>
        </div>


        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control article-search-input" placeholder="Search for...">
                        <span class="input-group-btn">
                <button class="btn btn-secondary article-search-btn" type="button">Go!</button>
              </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @foreach($movie->genres as $genre)
                                    <li>
                                        <a href="/genres/{{$genre->id}}" class="user-tag">{{$genre->genre_name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="/countries/{{$movie->country->id}}" class="user-tag">{{$movie->country->country_name}}</a>
                                </li>
                                @foreach($movie->directors as $director)
                                    <li>
                                        <a href="/directors/{{$director->id}}" target="_blank" class="user-tag">
                                            {{$director->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            @if(count($movie->posts))
                <div class="card my-4">
                    <h5 class="card-header">Các bài viết liên quan</h5>
                    <div class="card-body">
                        @foreach($movie->posts as $post)
                            <a href="/posts/{{$post->id}}" class="user-link">{{$post->title}}</a>
                        @endforeach
                    </div>
                </div>
            @endif
            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Tin tức liên quan</h5>
                <div class="card-body">
                    @include('partials.elements._news')
                </div>
            </div>


        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<link href="/css/pages/moviePost.css" rel="stylesheet">
<script src="/js/pages/post/movieSearch.js"></script>

@section('title', '| Phim')