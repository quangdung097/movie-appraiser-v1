<div class="card mb-3">
    {{--<img src="..." class="card-img-top" alt="...">--}}
    <div class="card-body">
        <h5 class="card-title">{{$country->country_name}}</h5>
        <p class="card-text">
            Quốc gia
        </p>
        <p class="card-text"><small class="text-muted">Sửa lần cuối: {{$country->updated_at}}</small></p>
    </div>
</div>

<hr>
<div style="min-height: 300px;">
    <h3>
        Danh sách các phim thuộc nước {{$country->country_name}}.
    </h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr id="actor-header">
                <th>#</th>
                <th>Phim</th>
                <th>Đầu tư</th>
                <th>Thể loại</th>
                <th>Đạo diễn</th>
            </tr>
            </thead>
            <tbody>
            @foreach($country->movies as $movie)
                <tr id="movie-member-{{$movie->id}}" class="movie-member">
                    <td>{{$movie->id}}</td>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->budget}}</td>
                    <td>
                        @foreach($movie->genres as $genre)
                            <a href="/genres/{{$genre->id}}" class="user-tag">{{$genre->genre_name}}</a>
                        @endforeach
                    </td>
                    <td>
                        @foreach($movie->directors as $director)
                            <a href="/directors/{{$director->id}}" class="user-tag">{{$director->name}}</a>
                            <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        {{ $country->movies->links('vendor.pagination.bootstrap-4') }}
    </div>
</div>

@section('title', '| Quốc gia')